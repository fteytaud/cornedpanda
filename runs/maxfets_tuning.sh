#!/bin/sh

#OAR -l /nodes=1/core=2,walltime=12:00:00
#OAR --notify mail:dehos@lisic.univ-littoral.fr

GUI=../ucigui/ucigui.out
ARBITER=../cornedpanda/bin/cornedpandarbitre.out
ENGINE=../cornedpanda/bin/cornedpanda.out
ENGINESK=stockfish
OPTIONSSK="Skill Level=2"
OPTIONS1="Engine_player=PlayerMAXFETS;PlayerMAXFETS_kuct=400;PlayerMAXFETS_nbSimulations=100000"
OPTIONS2="Engine_player=PlayerMAXFETS;PlayerMAXFETS_kuct=800;PlayerMAXFETS_nbSimulations=100000"
OPTIONS3="Engine_player=PlayerMAXFETS;PlayerMAXFETS_kuct=1600;PlayerMAXFETS_nbSimulations=100000"
OPTIONS4="Engine_player=PlayerMAXFETS;PlayerMAXFETS_kuct=3200;PlayerMAXFETS_nbSimulations=100000"
OPTIONS5="Engine_player=PlayerMAXFETS;PlayerMAXFETS_kuct=6400;PlayerMAXFETS_nbSimulations=100000"
OPTIONS6="Engine_player=PlayerMAXFETS;PlayerMAXFETS_kuct=12800;PlayerMAXFETS_nbSimulations=100000"
OPTIONS7="Engine_player=PlayerMAXFETS;PlayerMAXFETS_kuct=25600;PlayerMAXFETS_nbSimulations=100000"
OPTIONS8="Engine_player=PlayerMAXFETS;PlayerMAXFETS_kuct=51200;PlayerMAXFETS_nbSimulations=100000"
NBGAMES=100

filename=`basename $0 ".sh"`

$GUI $ARBITER $ENGINE "$OPTIONS1" $ENGINESK "$OPTIONSSK" $NBGAMES > ${filename}_1W_`hostname`_`date +%Y%m%d`_`date +%H%M%S`.dat &
$GUI $ARBITER $ENGINESK "$OPTIONSSK" $ENGINE "$OPTIONS1" $NBGAMES > ${filename}_1B_`hostname`_`date +%Y%m%d`_`date +%H%M%S`.dat &

$GUI $ARBITER $ENGINE "$OPTIONS2" $ENGINESK "$OPTIONSSK" $NBGAMES > ${filename}_2W_`hostname`_`date +%Y%m%d`_`date +%H%M%S`.dat &
$GUI $ARBITER $ENGINESK "$OPTIONSSK" $ENGINE "$OPTIONS2" $NBGAMES > ${filename}_2B_`hostname`_`date +%Y%m%d`_`date +%H%M%S`.dat &

$GUI $ARBITER $ENGINE "$OPTIONS3" $ENGINESK "$OPTIONSSK" $NBGAMES > ${filename}_3W_`hostname`_`date +%Y%m%d`_`date +%H%M%S`.dat &
$GUI $ARBITER $ENGINESK "$OPTIONSSK" $ENGINE "$OPTIONS3" $NBGAMES > ${filename}_3B_`hostname`_`date +%Y%m%d`_`date +%H%M%S`.dat &

$GUI $ARBITER $ENGINE "$OPTIONS4" $ENGINESK "$OPTIONSSK" $NBGAMES > ${filename}_4W_`hostname`_`date +%Y%m%d`_`date +%H%M%S`.dat &
$GUI $ARBITER $ENGINESK "$OPTIONSSK" $ENGINE "$OPTIONS4" $NBGAMES > ${filename}_4B_`hostname`_`date +%Y%m%d`_`date +%H%M%S`.dat &

$GUI $ARBITER $ENGINE "$OPTIONS5" $ENGINESK "$OPTIONSSK" $NBGAMES > ${filename}_5W_`hostname`_`date +%Y%m%d`_`date +%H%M%S`.dat &
$GUI $ARBITER $ENGINESK "$OPTIONSSK" $ENGINE "$OPTIONS5" $NBGAMES > ${filename}_5B_`hostname`_`date +%Y%m%d`_`date +%H%M%S`.dat &

$GUI $ARBITER $ENGINE "$OPTIONS6" $ENGINESK "$OPTIONSSK" $NBGAMES > ${filename}_6W_`hostname`_`date +%Y%m%d`_`date +%H%M%S`.dat &
$GUI $ARBITER $ENGINESK "$OPTIONSSK" $ENGINE "$OPTIONS6" $NBGAMES > ${filename}_6B_`hostname`_`date +%Y%m%d`_`date +%H%M%S`.dat &

$GUI $ARBITER $ENGINE "$OPTIONS7" $ENGINESK "$OPTIONSSK" $NBGAMES > ${filename}_7W_`hostname`_`date +%Y%m%d`_`date +%H%M%S`.dat &
$GUI $ARBITER $ENGINESK "$OPTIONSSK" $ENGINE "$OPTIONS7" $NBGAMES > ${filename}_7B_`hostname`_`date +%Y%m%d`_`date +%H%M%S`.dat &

$GUI $ARBITER $ENGINE "$OPTIONS8" $ENGINESK "$OPTIONSSK" $NBGAMES > ${filename}_8W_`hostname`_`date +%Y%m%d`_`date +%H%M%S`.dat &
$GUI $ARBITER $ENGINESK "$OPTIONSSK" $ENGINE "$OPTIONS8" $NBGAMES > ${filename}_8B_`hostname`_`date +%Y%m%d`_`date +%H%M%S`.dat &

wait;
wait;

wait;
wait;

wait;
wait;

wait;
wait;

wait;
wait;

wait;
wait;

wait;
wait;

wait;
wait;

