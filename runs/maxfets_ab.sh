#!/bin/sh

#OAR -l /nodes=1/core=2,walltime=12:00:00
#OAR --notify mail:dehos@lisic.univ-littoral.fr

GUI=../ucigui/ucigui.out
ARBITER=../cornedpanda/bin/cornedpandarbitre.out
ENGINE1=../cornedpanda/bin/cornedpanda.out
OPTIONS1="Engine_player=PlayerMAXFETS;PlayerMAXFETS_kuct=20000;PlayerMAXFETS_nbSimulations=100000"
ENGINE2=../cornedpanda/bin/cornedpanda.out
OPTIONS2="Engine_player=PlayerABQDLimited;PlayerAB_maxDepth=4"
NBGAMES=1

filename=`basename $0 ".sh"`

$GUI $ARBITER $ENGINE1 "$OPTIONS1" $ENGINE2 "$OPTIONS2" $NBGAMES > ${filename}_1_`hostname`_`date +%Y%m%d`_`date +%H%M%S`.dat &
$GUI $ARBITER $ENGINE2 "$OPTIONS2" $ENGINE1 "$OPTIONS1" $NBGAMES > ${filename}_2_`hostname`_`date +%Y%m%d`_`date +%H%M%S`.dat &

wait;
wait;

