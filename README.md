# cornedpanda #

UCI chess engine

## players ##

- PlayerUCT : MCTS avec formule bandit + politique Monte-Carlo (le truc le plus classique possible).
- PlayerFETS : MCTS avec formule bandit + fonction d'évaluation
- PlayerKFETS : comme FETS mais avant d'appeler la fonction d'évaluation on fait K coups aléatoires.
- PlayerUCBB : MCTS avec MAX (on ne calcule plus le x_i moyen mais on prend le max directement) + fonction d'évaluation
- PlayerMAXFETS : comme UCBB mais on développe tous les fils d'un coup au lieu de le faire noeud par noeud.


## Licence

cornedpanda:
source files: cornedpanda.cpp Board* Player* Engine.*
COPYING.WTFPL

Senjo:
Universal Chess Interface (UCI) adapter by Shawn Chidester zd3nik@gmail.com
COPYING.SENJO
https://github.com/zd3nik/SenjoUCIAdapter

## TODO
à implémenter mais rien de nouveau :
- affichages intéressants en release
- fonction d'évaluation (avec opening/mid-game/end-game)
- alpha-beta :
  - quiescence search 
  - iterative deepning + transposition table

apprentissage :
- opening/mid-game/end-game
- fonction d'évaluation

algo :
- selection :
  - mean/max
  - utilisation de la fonction d'évaluation (uct/playouts)
- simulation/expansion :
  - alpha-beta/solver
  - MCTS local
  - quiescence search
  - expansion en largeur

