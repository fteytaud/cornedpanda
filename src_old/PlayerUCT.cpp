/*
// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "PlayerUCT.hpp"
#include "Log.hpp"
#include "Fastmath.hpp"
#include <cassert>
#include <cmath>

cornedpanda::PlayerUCT::Node::Node(const cornedpanda::board_t &board) :
    _nbWins(0), _nbSims(0), _board(board), _parentNode(nullptr)
{
    _possibleMoves = _board.getMoves();
    //_move;
}

cornedpanda::PlayerUCT::Node::Node(const cornedpanda::board_t & board, const Move &m, Node *parentNode) :
    _nbWins(0), _nbSims(0), _board(board), _parentNode(parentNode)
{
    _board.makeMove(m);
    _possibleMoves = _board.getMoves();
    _move = m;
}

cornedpanda::PlayerUCT::PlayerUCT()
{
    _parameters["PlayerUCT_kuct"] = 0.25;
    _parameters["PlayerUCT_nbSimulations"] = 100;
    _parameters["PlayerUCT_stepSimulations"] = 10;
}

void cornedpanda::PlayerUCT::initBestMove()
{
    _iSims = 0;
    _sSims = _parameters["PlayerUCT_stepSimulations"];
    _nSims = _parameters["PlayerUCT_nbSimulations"];
    _isBestMoveOk = false;

    // reset search tree
    _nodes.clear();
    // warning: _nodes must be fully reserved !!!
    _nodes.reserve(_nSims + 1);
    _nodes.emplace_back(_board);

    // choose move
    assert(_nodes.front()._possibleMoves.size() > 0);
}

void cornedpanda::PlayerUCT::iterBestMove()
{
    int n = std::min(_nSims, _iSims + _sSims);
    while (_iSims < n)
    {
        computeOneSimulation();
        _iSims++;
    }

    _isBestMoveOk = _iSims == _nSims;
    _iBest = getFinalChoice();
    _bestMove =  (_nodes.front())._childNodes[_iBest]->_move;
}

int cornedpanda::PlayerUCT::getFinalChoice()
{
    Node *root = &(_nodes.front());
    assert(root->_childNodes.size() > 0);
    assert(root->_childNodes[0]->_nbSims > 0);
    int ibest = 0;
    int best = root->_childNodes[0]->_nbSims;

    for (unsigned ichild = 1; ichild < root->_childNodes.size(); ichild++)
    {
        if (root->_childNodes[ichild]->_nbSims > best)
        {
            ibest = ichild;
            best = root->_childNodes[ichild]->_nbSims;
        }
    }
    return ibest;
}


cornedpanda::PlayerUCT::Node * cornedpanda::PlayerUCT::computeSelection(Node *node)
{
    assert(not node->_childNodes.empty());

    Node *bestNode = nullptr;
    float bestScore = -1.;
    for (Node *childNode : node->_childNodes)
    {
        assert(childNode->_nbSims > 0);
        float exploitation = childNode->_nbWins / (float)childNode->_nbSims;
        assert(exploitation >= 0.);
        float exploration = cornedpanda::fastsqrt(cornedpanda::fastlog(node->_nbSims) / (float)childNode->_nbSims);
        assert(exploration >= 0.);
        float score = exploitation + _parameters.at("PlayerUCT_kuct") * exploration;
        assert(score >= 0.);
        if (score > bestScore)
        {
            bestNode = childNode;
            bestScore = score;
        }
    }
    assert(bestScore > -1.);
    assert(bestNode);
    return bestNode;
}

cornedpanda::Move cornedpanda::PlayerUCT::computeExpansion(Node *node)
{
    assert(node);
    assert(node->_possibleMoves.size());
    Move m = node->_possibleMoves.back();
    node->_possibleMoves.pop_back();
    return m;
}

cornedpanda::GameState cornedpanda::PlayerUCT::computeSimulation(Node *node)
{
    assert(node);
    board_t copy = node->_board;
    GameState winner = copy.getGameState();
    int iteration = 0;
    while (winner == GAME_STATE_RUNNING)
    {
        const std::vector<Move> & allMoves = copy.getMoves();
        copy.makeMove(allMoves[random(allMoves.size())]);
        winner = copy.getGameState();

        if (iteration > 20)
        {
            int cutEnding = copy.evaluate();
            if (cutEnding < -4)
                return GAME_STATE_BLACK_WON;
            if (cutEnding > 4)
                return GAME_STATE_WHITE_WON;
            if (iteration > 50)
                return GAME_STATE_DRAW;
        }
        iteration++;
    }

    assert(winner != GAME_STATE_RUNNING);
    return winner;
}

void cornedpanda::PlayerUCT::computeBackPropagation(Node *node, GameState endState)
{
    assert(node);
    while (node)
    {
        bool winnerIsOpponent = false;
        if ((node->_board.isWhiteToMove() && (endState == GAME_STATE_WHITE_WON)) || (!(node->_board.isWhiteToMove()) && (endState == GAME_STATE_BLACK_WON)))
            winnerIsOpponent = true;

        if (!winnerIsOpponent)
            node->_nbWins++;
        node->_nbSims++;
        node = node->_parentNode;
    }
}

void cornedpanda::PlayerUCT::computeOneSimulation()
{
    Node *currentNode = &(_nodes.front());

    // descent (selection step)
    while (not currentNode->_childNodes.empty() and currentNode->_possibleMoves.empty())
    {
        currentNode = computeSelection(currentNode);
        assert(currentNode);
    }

    // expansion (create a new node) unless the current node ends the game
    if (currentNode->_board.getGameState() == GAME_STATE_RUNNING)
    {
        Move m = computeExpansion(currentNode);
        const board_t &b = currentNode->_board;
        _nodes.emplace_back(b, m, currentNode);
        Node* newNode = &(_nodes.back());
        currentNode->_childNodes.push_back(newNode);
        currentNode = newNode;
    }

    // simulation (random playout)
    GameState endState = computeSimulation(currentNode);

    // backpropagation 
    computeBackPropagation(currentNode, endState);
    assert(currentNode->_nbSims > 0);
}

void cornedpanda::PlayerUCT::statsBestMove()
{
    Player::statsBestMove();

    cornedpanda::Output() << "cornedpanda::PlayerUCT::statsBestMove : PlayerUCT_nbSims = " << _parameters["PlayerUCT_nbSimulations"] << '\n';
    cornedpanda::Output() << "cornedpanda::PlayerUCT::statsBestMove : Evaluation de la position courante = " << _board.evaluate() << '\n';
#ifdef DEBUG
    cornedpanda::logStream << "cornedpanda::PlayerUCT::statsBestMove : PlayerUCT_nbSims = " << _parameters["PlayerUCT_nbSimulations"] << '\n';
    cornedpanda::logStream << "cornedpanda::PlayerUCT::statsBestMove : Evaluation de la position courante = " << _board.evaluate() << '\n';
#endif

    Node *root = &(_nodes.front());
    cornedpanda::Output() << "cornedpanda::PlayerUCT::statsBestMove : coup best = " << _board.moveToString(root->_childNodes[_iBest]->_move) << '\n';
#ifdef DEBUG
    cornedpanda::logStream << "cornedpanda::PlayerUCT::statsBestMove : coup best = " << _board.moveToString(root->_childNodes[_iBest]->_move) << '\n';
#endif
}

*/

