/*
// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef _PLAYERUCBB_HPP_
#define _PLAYERUCBB_HPP_

#include "Player.hpp"
#include <cassert>

namespace cornedpanda
{
    class PlayerUCBB : public Player
    {
        private:
            struct Node {
                int _nbSims;
                int _minmaxEval;
                float _avgEval;

                int _coefWhiteNode; // 1 is white, -1 is black ; used for backpropagation

                board_t _board;
                Move _move;

                std::vector<Node*> _childNodes;
                std::vector<Move> _possibleMoves;

                Node * _parentNode;
                Node * _bestSon;

                Node(const board_t &, int);
                Node(const Node &); 
                Node(const board_t &, const Move &, Node *, int);
            };

        public:
            PlayerUCBB();

            void initBestMove();
            void iterBestMove();
            void statsBestMove();

        private:

            int _iSims;
            int _sSims;
            int _nSims;
            int _iBest;

            Node * computeSelection(Node *node);
            Move computeExpansion(Node *node);
            int getFinalChoice();
            int computeSimulation(Node *node);
            void computeBackPropagation(Node *node, int);
            void computeOneSimulation();

        private:
            std::vector<Node> _nodes;
    };
}

#endif
*/

