/*
// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef _PLAYERFETS_HPP_
#define _PLAYERFETS_HPP_

#include "Player.hpp"
#include <cassert>

namespace cornedpanda
{
    class PlayerFETS : public Player
    {
        private:
            struct Node {
                float _nodeScore;
                int _nbSims;

                board_t _board;
                Move _move;

                std::vector<Node*> _childNodes;
                std::vector<Move> _possibleMoves;

                Node * _parentNode;

                Node(const board_t &);
                Node(const Node &) { assert(0); }
                Node(const board_t &, const Move &, Node *);
            };

        public:
            PlayerFETS();

            void initBestMove();
            void iterBestMove();
            void statsBestMove();

        private:

            int _iSims;
            int _sSims;
            int _nSims;
            int _nInterestingSims;
            int _iBest;

            Node * computeSelection(Node *node);
            Move computeExpansion(Node *node);
            int getFinalChoice();
            int computeSimulation(Node *node);
            void computeBackPropagation(Node *node, int);
            Node * computeOneSimulation(Node * fromNode);

        private:
            std::vector<Node> _nodes;
    };
}

#endif
*/

