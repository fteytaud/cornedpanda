/*
// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "PlayerUCBB.hpp"
#include "Log.hpp"
#include "Fastmath.hpp"
#include <cassert>
#include <cmath>
#include <sstream>

cornedpanda::PlayerUCBB::Node::Node(const cornedpanda::board_t &board, int k):_nbSims(0), _minmaxEval(-200000), _avgEval(0), _coefWhiteNode(k), _board(board), _parentNode(nullptr), _bestSon(nullptr)
{
    _possibleMoves = _board.getMoves();
    //_move;
}

cornedpanda::PlayerUCBB::Node::Node(const cornedpanda::board_t & board, const Move &m, Node *parentNode, int k):_nbSims(0), _minmaxEval(-200000), _avgEval(0), _coefWhiteNode(k), _board(board), _parentNode(parentNode), _bestSon(nullptr)
{
    _board.makeMove(m);
    _possibleMoves = _board.getMoves();
    _move = m;
}

cornedpanda::PlayerUCBB::Node::Node(const Node &) 
{ 
    assert(0); 
}

cornedpanda::PlayerUCBB::PlayerUCBB()
{
    _parameters["PlayerUCBB_kuct"] = 1;
    _parameters["PlayerUCBB_nbSimulations"] = 100000;
    _parameters["PlayerUCBB_stepSimulations"] = 10;
}

void cornedpanda::PlayerUCBB::initBestMove()
{
    _iSims = 0;
    _sSims = _parameters["PlayerUCBB_stepSimulations"];
    _nSims = _parameters["PlayerUCBB_nbSimulations"];
    _isBestMoveOk = false;

    // reset search tree
    _nodes.clear();
    // warning: _nodes must be fully reserved !!!
    _nodes.reserve(_nSims + 1);
    _nodes.emplace_back(_board, _board.isWhiteToMove()*-2+1);

    // choose move
    assert(_nodes.front()._possibleMoves.size() > 0);
}

void cornedpanda::PlayerUCBB::iterBestMove()
{
    int n = std::min(_nSims, _iSims + _sSims);
    while (_iSims < n)
    {
#ifdef DEBUG
      cornedpanda::Output()<<"\n<+++ Simulation "<<_iSims<<"/"<<n<<" +++>>>\n";
#endif
        computeOneSimulation();
        _iSims++;
    }

    _isBestMoveOk = _iSims == _nSims;
    _iBest = getFinalChoice();
    _bestMove =  (_nodes.front())._childNodes[_iBest]->_move;
}

int cornedpanda::PlayerUCBB::getFinalChoice()
{
    Node *root = &(_nodes.front());
    assert(root->_childNodes.size() > 0);
    assert(root->_childNodes[0]->_nbSims > 0);
    int ibest = 0;
    int best = root->_childNodes[0]->_nbSims;

    for (unsigned ichild = 1; ichild < root->_childNodes.size(); ichild++)
    {
#ifdef DEBUG
        cornedpanda::logStream << "cornedpanda::PlayerUCBB::getFinalChoice :\n";
        cornedpanda::logStream << "Coup: " << _board.moveToString(root->_childNodes[ichild]->_move);
        cornedpanda::logStream << "Score moyen: " << root->_childNodes[ichild]->_avgEval << " sur " << root->_childNodes[ichild]->_nbSims << std::endl;
        cornedpanda::logStream << "Score minmax: " << root->_childNodes[ichild]->_minmaxEval << " sur " << root->_childNodes[ichild]->_nbSims << std::endl;
        cornedpanda::logStream << std::endl;
#endif
        if (root->_childNodes[ichild]->_nbSims > best)
        {
            ibest = ichild;
            best = root->_childNodes[ichild]->_nbSims;
        }
    }
#ifdef DEBUG
    cornedpanda::logStream << "cornedpanda::PlayerUCBB::getFinalChoice :\n";
    cornedpanda::logStream << "Coup: " << _board.moveToString(root->_childNodes[ibest]->_move);
    cornedpanda::logStream << "Score moyen: " << root->_childNodes[ibest]->_avgEval << " sur " << root->_childNodes[ibest]->_nbSims << std::endl;
    cornedpanda::logStream << "Score minmax: " << root->_childNodes[ibest]->_minmaxEval << " sur " << root->_childNodes[ibest]->_nbSims << std::endl;
    cornedpanda::logStream << std::endl;
#endif
    return ibest;
}


cornedpanda::PlayerUCBB::Node * cornedpanda::PlayerUCBB::computeSelection(Node *node)
{
    assert(not node->_childNodes.empty());

    float kuct = (float)_parameters.at("PlayerUCBB_kuct");
    Node *bestNode = nullptr;
    float bestScore = -1000000.f;
    
    for (Node *childNode : node->_childNodes)
    {
        assert(childNode->_nbSims > 0);
        float exploitation = childNode->_minmaxEval;
        float exploration = fastsqrt(fastlog(node->_nbSims) / (float)childNode->_nbSims);
        assert(exploration >= 0.f);
        float score = exploitation + kuct * exploration;
        if (score > bestScore)
        {
            bestNode = childNode;
            bestScore = score;
        }
    }
    assert(bestScore > -1000000.f);
    assert(bestNode);
    return bestNode;
}

cornedpanda::Move cornedpanda::PlayerUCBB::computeExpansion(Node *node)
{
    assert(node);
    assert(node->_possibleMoves.size());
    Move m = node->_possibleMoves.back();
    node->_possibleMoves.pop_back();
    return m;
}

int cornedpanda::PlayerUCBB::computeSimulation(Node *node)
{
    return  node->_board.evaluate();
}

void cornedpanda::PlayerUCBB::computeBackPropagation(Node *node, int s)
{
 //   Node *root = &(_nodes.front()); // noeud racine
    assert(node);
    int ns; // evaluation according to the player to play
    Node *sonNode = NULL; // used to update the best son
    bool updateMinMax = true; // used to know whether we should continue to backpropagate

    while (node->_parentNode) // not needed to update the root  except nbSims++ see end of function
    {
#ifdef DEBUG
       int old_value = node->_minmaxEval;
#endif

        node->_nbSims++; 

        if (updateMinMax)
        {
            ns = s * node->_coefWhiteNode; 
                // node->_coefWhiteNode equals 1 if white -1 if black

            if (node->_nbSims==1)
            {
                node->_avgEval = ns;
                node->_minmaxEval = ns;
                node->_bestSon = NULL;
            }
            else
            {
                if (ns < node->_minmaxEval)
                {
                    node->_minmaxEval = ns;
                    node->_bestSon = sonNode;
                }
                else
                {
                    if (node->_bestSon == NULL)
                    {
                        node->_minmaxEval = ns;
                        node->_bestSon = sonNode;
                    }
                    else
                    {
                        if (node->_bestSon->_move == sonNode->_move)
                        {// the best is not as good as we thought
                            Node *best = node->_childNodes[0];
                            int vbest = node->_childNodes[0]->_minmaxEval;
                            for (unsigned i = 1; i < node->_childNodes.size(); i++)
                            {
                                if (node->_childNodes[i]->_minmaxEval > vbest)
                                {
                                    best = node->_childNodes[i];
                                    vbest = node->_childNodes[i]->_minmaxEval;
                                }
                            }
                            node->_minmaxEval = -1 * vbest;
                            node->_bestSon = best;
                            s = vbest * -1 * node->_coefWhiteNode;
                        }
                        else
                        {
                            updateMinMax = false;
                        }
                    }
                }

                // moyenne glissante : x_n = (x_{n-1} * (n-1) + x_n) / n
                node->_avgEval = (node->_avgEval * (float) (node->_nbSims - 1.))/(float)node->_nbSims + (float)ns / (float)node->_nbSims;
            }
            sonNode = node;
        }

#ifdef DEBUG
	cornedpanda::Output()<<"<<< "<<old_value<<" >>>\n";

	for (unsigned j=0; j < node->_childNodes.size(); j++)
	  {
	    cornedpanda::Output() << "\t\t" << _board.moveToString(node->_childNodes[j]->_move) << " ; " << node->_childNodes[j]->_nbSims << " ; " << node->_childNodes[j]->_minmaxEval << "\n";
	  }
#endif

        node = node->_parentNode;
    }
    //  adding one simulation to the root
    node->_nbSims++;
}

void cornedpanda::PlayerUCBB::computeOneSimulation()
{
    Node *currentNode = &(_nodes.front());

#ifdef DEBUG
    cornedpanda::Output()<<"\n<--- Simulation "<<currentNode->_nbSims<<" --->>>\n";
#endif


    // descent (selection step)
    while (not currentNode->_childNodes.empty() and currentNode->_possibleMoves.empty())
    {
        currentNode = computeSelection(currentNode);
        assert(currentNode);
    }

    // expansion (create a new node) unless the current node ends the game
    if (currentNode->_board.getGameState() == GAME_STATE_RUNNING)
    {
        Move m = computeExpansion(currentNode);

        const board_t &b = currentNode->_board;
        //int k = (_nodes.front()._board.isWhiteToMove() == _board.isWhiteToMove()) * 2 - 1;
        _nodes.emplace_back(b, m, currentNode, -1 * currentNode->_coefWhiteNode);
        Node* newNode = &(_nodes.back());
        currentNode->_childNodes.push_back(newNode);

        currentNode = newNode;
    }


    // simulation (random playout)
    int s = computeSimulation(currentNode);

    // backpropagation 
    computeBackPropagation(currentNode, s);
    assert(currentNode->_nbSims > 0);
}

void cornedpanda::PlayerUCBB::statsBestMove()
{
    Player::statsBestMove();

    cornedpanda::Output() << "cornedpanda::PlayerUCBB::statsBestMove : PlayerUCBB_nbSims = " << _parameters["PlayerUCBB_nbSimulations"] << '\n';
    cornedpanda::Output() << "cornedpanda::PlayerUCBB::statsBestMove : Evaluation de la position courante =" << _board.evaluate() << '\n';
#ifdef DEBUG
    cornedpanda::logStream << "cornedpanda::PlayerUCBB::statsBestMove : PlayerUCBB_nbSims = " << _parameters["PlayerUCBB_nbSimulations"] << '\n';
    cornedpanda::logStream << "cornedpanda::PlayerUCBB::statsBestMove : Evaluation de la position courante =" << _board.evaluate() << '\n';
#endif

    // TODO 
    std::stringstream ss;
    cornedpanda::Output() << "cornedpanda::PlayerUCBB::getMove -> PlayerUCBB_nbSims =" << _parameters["PlayerUCBB_nbSimulations"] << '\n';
    cornedpanda::Output() << "cornedpanda::PlayerUCBB::getMove -> Evaluation de la position courante =" << _board.evaluate() << '\n';
    cornedpanda::Output() << "cornedpanda::PlayerUCBB::getMove -> Nombre de fils ="<< _nodes.front()._childNodes.size() << '\n';

    for (unsigned i=0; i < _nodes.front()._childNodes.size(); i++)
    {
        float tmpkuct = fabs(_nodes.front()._childNodes[i]->_minmaxEval - _nodes.front()._childNodes[i]->_avgEval); 
        ss << _board.moveToString(_nodes.front()._childNodes[i]->_move) << " ; " << _nodes.front()._childNodes[i]->_avgEval << " ;" << _nodes.front()._childNodes[i]->_nbSims << "; eventuellement tmpkuct: "<< tmpkuct << " ; " << _nodes.front()._childNodes[i]->_minmaxEval << "\n";
    }
    cornedpanda::Output() << "cornedpanda::PlayerUCBB::getMove -> allScores = \n" << ss.str() << '\n';




    Node *root = &(_nodes.front());
    cornedpanda::Output() << "cornedpanda::PlayerUCBB::statsBestMove : coup best = " << _board.moveToString(root->_childNodes[_iBest]->_move) << '\n';
#ifdef DEBUG
    cornedpanda::logStream << "cornedpanda::PlayerUCBB::statsBestMove : coup best = " << _board.moveToString(root->_childNodes[_iBest]->_move) << '\n';
#endif
}

*/

