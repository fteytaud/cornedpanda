/*
// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "PlayerFETS.hpp"
#include "Log.hpp"
#include "Fastmath.hpp"
#include <cassert>
#include <cmath>
#include <sstream>

cornedpanda::PlayerFETS::Node::Node(const cornedpanda::board_t &board) :
    _nodeScore(0), _nbSims(0), _board(board), _parentNode(nullptr)
{
    _possibleMoves = _board.getMoves();
}

cornedpanda::PlayerFETS::Node::Node(const cornedpanda::board_t & board, const Move &m, Node *parentNode) :
    _nodeScore(0), _nbSims(0), _board(board), _parentNode(parentNode)
{
    _board.makeMove(m);
    _possibleMoves = _board.getMoves();
    _move = m;
}

cornedpanda::PlayerFETS::PlayerFETS()
{
    _parameters["PlayerFETS_kuct"] = 10000;
    _parameters["PlayerFETS_nbSimulations"] = 100000;
    _parameters["PlayerFETS_stepSimulations"] = 10;
    _parameters["PlayerFETS_interestingSims"] = 1000;
}

void cornedpanda::PlayerFETS::initBestMove()
{
    _iSims = 0;
    _sSims = _parameters["PlayerFETS_stepSimulations"];
    _nSims = _parameters["PlayerFETS_nbSimulations"];
    _nInterestingSims = _parameters["PlayerFETS_interestingSims"];
    _isBestMoveOk = false;

    // reset search tree
    _nodes.clear();
    // warning: _nodes must be fully reserved !!!
    _nodes.reserve(_nSims + 1);
    _nodes.emplace_back(_board);

    // choose move
    assert(_nodes.front()._possibleMoves.size() > 0);
}

void cornedpanda::PlayerFETS::iterBestMove()
{
    int n = std::min(_nSims, _iSims + _sSims);
    while (_iSims < n)
    {

        Node * rootNode = &(_nodes.front());
        Node * interestingNode = computeOneSimulation(rootNode);
        _iSims++;

        // extend with local fets if interesting node
        if (interestingNode != nullptr)
        {
            // TODO si l'algo fonctionne, refactorer 
            int nInteresting = std::min(_nSims, _iSims + _nInterestingSims);
            while (_iSims < nInteresting)
            {
                computeOneSimulation(interestingNode);
                _iSims++;
            }            
        }

        // TODO algo qui developpe recursivement les fils interessants
        
    }

    _isBestMoveOk = _iSims == _nSims;
    _iBest = getFinalChoice();
    _bestMove =  (_nodes.front())._childNodes[_iBest]->_move;
}

int cornedpanda::PlayerFETS::getFinalChoice()
{
    Node *root = &(_nodes.front());
    assert(root->_childNodes.size() > 0);
    assert(root->_childNodes[0]->_nbSims > 0);
    int ibest = 0;
    int best = root->_childNodes[0]->_nbSims;

    for (unsigned ichild = 1; ichild < root->_childNodes.size(); ichild++)
    {
        if (root->_childNodes[ichild]->_nbSims > best)
        {
            ibest = ichild;
            best = root->_childNodes[ichild]->_nbSims;
        }
    }
    return ibest;
}


cornedpanda::PlayerFETS::Node * cornedpanda::PlayerFETS::computeSelection(Node *node)
{
    assert(not node->_childNodes.empty());

#ifndef NDEBUG
    Node *bestNode = nullptr;
#endif
    float bestScore = -1000000.;
    std::vector<Node*> bestIndices;
    for (Node *childNode : node->_childNodes)
    {
        assert(childNode->_nbSims > 0);
        float exploitation = childNode->_nodeScore;
        float exploration = cornedpanda::fastsqrt(cornedpanda::fastlog(node->_nbSims) / (float)childNode->_nbSims);
        assert(exploration >= 0.);
        float score = exploitation + _parameters.at("PlayerFETS_kuct") * exploration;
        if (score > bestScore)
        {
#ifndef NDEBUG
            bestNode = childNode;
#endif
            bestScore = score;
            bestIndices.clear();
            bestIndices.push_back(childNode);
        }
        else
        {
            if (score == bestScore)
            {
                bestIndices.push_back(childNode);
            }
        }
    }
    assert(bestScore > -1000000.);
    assert(bestNode);
    assert(bestIndices.size() > 0);
    unsigned r = random(bestIndices.size());
    assert((int)r >= 0);
    assert(r < bestIndices.size());
    return bestIndices[r];
    //return bestNode;
}

cornedpanda::Move cornedpanda::PlayerFETS::computeExpansion(Node *node)
{
    assert(node);
    assert(node->_possibleMoves.size());
    Move m = node->_possibleMoves.back();
    node->_possibleMoves.pop_back();
    return m;
}

int cornedpanda::PlayerFETS::computeSimulation(Node *node)
{
   return  node->_board.evaluate();
}

void cornedpanda::PlayerFETS::computeBackPropagation(Node *node, int s)
{
    assert(node);
    while (node)
    {
        node->_nbSims++;

        // moyenne glissante : x_n = (x_{n-1} * (n-1) + x_n) / n
        if (node->_board.isWhiteToMove())
            if (node->_nbSims==1)
                node->_nodeScore = -s;
            else
                node->_nodeScore = (node->_nodeScore * (float) (node->_nbSims - 1.))/(float)node->_nbSims - (float)s / (float)node->_nbSims;
        else
            if (node->_nbSims==1)
                node->_nodeScore = s;
            else
                node->_nodeScore = (node->_nodeScore * (float) (node->_nbSims - 1.))/(float)node->_nbSims + (float)s / (float)node->_nbSims;

        node = node->_parentNode;
    }
}

cornedpanda::PlayerFETS::Node * cornedpanda::PlayerFETS::computeOneSimulation(Node * fromNode)
{
    Node *currentNode = fromNode;

    // descent (selection step)
    while (not currentNode->_childNodes.empty() and currentNode->_possibleMoves.empty())
    {
        currentNode = computeSelection(currentNode);
        assert(currentNode);
    }

    // expansion (create a new node) unless the current node ends the game
    if (currentNode->_board.getGameState() == GAME_STATE_RUNNING)
    {
        Move m = computeExpansion(currentNode);

        const board_t &b = currentNode->_board; // TODO ca fait une copie de trop ?
        _nodes.emplace_back(b, m, currentNode);
        Node* newNode = &(_nodes.back());
        currentNode->_childNodes.push_back(newNode);

        currentNode = newNode;
    }

    // simulation (random playout)
    int s = computeSimulation(currentNode);

    // backpropagation 
    computeBackPropagation(currentNode, s);
    assert(currentNode->_nbSims > 0);

    // check if the current node in interesting to consider for the next simulation
    bool isInteresting = currentNode->_move._capture != PIECE_0 or currentNode->_move._check;
    return isInteresting ? currentNode : nullptr;

}

void cornedpanda::PlayerFETS::statsBestMove()
{
    Player::statsBestMove();

    std::stringstream ss;
    cornedpanda::Output() << "cornedpanda::PlayerFETS::statsBestMove : PlayerFETS_nbSims = " << _parameters["PlayerFETS_nbSimulations"] << '\n';
    cornedpanda::Output() << "cornedpanda::PlayerFETS::statsBestMove : Evaluation de la position courante =" << _board.evaluate() << '\n';
    cornedpanda::Output() << "cornedpanda::PlayerFETS::statsBestMove : Nombre de fils = "<< _nodes.front()._childNodes.size() << '\n';
    for (unsigned i=0; i < _nodes.front()._childNodes.size(); i++)
    {
        ss << _board.moveToString(_nodes.front()._childNodes[i]->_move) << " : " << _nodes.front()._childNodes[i]->_nodeScore << " (" << _nodes.front()._childNodes[i]->_nbSims << ")\n";
    }
    cornedpanda::Output() << "cornedpanda::PlayerFETS::getMove : allScores = \n" << ss.str() << '\n';
    // TODO logstream



    Node *root = &(_nodes.front());

    for (unsigned ichild = 0; ichild < root->_childNodes.size(); ichild++)
    {
        cornedpanda::Output() << "cornedpanda::PlayerFETS::statsBestMove :" 
            << "\ti=" << ichild 
            << "\tmove=" << _board.moveToString(root->_childNodes[ichild]->_move) 
            << "\tscore=" << root->_childNodes[ichild]->_nodeScore 
            << "\tsims=" << root->_childNodes[ichild]->_nbSims
            << "\n";
#ifdef DEBUG
        cornedpanda::logStream << "cornedpanda::PlayerFETS::statsBestMove :" 
            << "\ti=" << ichild 
            << "\tmove=" << _board.moveToString(root->_childNodes[ichild]->_move) 
            << "\tscore=" << root->_childNodes[ichild]->_nodeScore 
            << "\tsims=" << root->_childNodes[ichild]->_nbSims
            << "\n";
#endif
    }
}

*/

