/*
// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "PlayerKFEMCTS.hpp"
#include "Log.hpp"
#include "Fastmath.hpp"
#include <cassert>
#include <cmath>
#include <sstream>

//Idée : Pas de pur monte-carlo pour l'évaluation, mais pas non plus d'éval directe : on fait des chemins randoms sur quelques étapes, puis on fait l'évaluation. Ensuite, on en prend la valeur (min ? max ? moyenne ?)

cornedpanda::PlayerKFEMCTS::Node::Node(const cornedpanda::board_t &board) :
    _nodeScore(0), _nbSims(0), _board(board), _parentNode(nullptr)
{
    _possibleMoves = _board.getMoves();
}

cornedpanda::PlayerKFEMCTS::Node::Node(const cornedpanda::board_t & board, const Move &m, Node *parentNode) :
    _nodeScore(0), _nbSims(0), _board(board), _parentNode(parentNode)
{
    _board.makeMove(m);
    _possibleMoves = _board.getMoves();
    _move = m;
}

cornedpanda::PlayerKFEMCTS::PlayerKFEMCTS()
{
    _parameters["PlayerKFEMCTS_kuct"] = 1;
    _parameters["PlayerKFEMCTS_nbSimulations"] = 100;
    _parameters["PlayerKFEMCTS_stepSimulations"] = 10;
    _parameters["PlayerKFEMCTS_nbRandomMoves"] = 5;
}

void cornedpanda::PlayerKFEMCTS::initBestMove()
{
    _iSims = 0;
    _sSims = _parameters["PlayerKFEMCTS_stepSimulations"];
    _nSims = _parameters["PlayerKFEMCTS_nbSimulations"];
    _isBestMoveOk = false;
    _nbRandomMoves = _parameters["PlayerKFEMCTS_nbRandomMoves"];

    // reset search tree
    _nodes.clear();
    // warning: _nodes must be fully reserved !!!
    _nodes.reserve(_nSims + 1);
    _nodes.emplace_back(_board);

    // choose move
    assert(_nodes.front()._possibleMoves.size() > 0);
}

void cornedpanda::PlayerKFEMCTS::iterBestMove()
{
    int n = std::min(_nSims, _iSims + _sSims);
    while (_iSims < n)
    {
        computeOneSimulation();
        _iSims++;
    }

    _isBestMoveOk = _iSims == _nSims;
    _iBest = getFinalChoice();
    _bestMove =  (_nodes.front())._childNodes[_iBest]->_move;
}

int cornedpanda::PlayerKFEMCTS::getFinalChoice()
{
    Node *root = &(_nodes.front());
    assert(root->_childNodes.size() > 0);
    assert(root->_childNodes[0]->_nbSims > 0);
    int ibest = 0;
    int best = root->_childNodes[0]->_nbSims;

    for (unsigned ichild = 1; ichild < root->_childNodes.size(); ichild++)
    {
        if (root->_childNodes[ichild]->_nbSims > best)
        {
            ibest = ichild;
            best = root->_childNodes[ichild]->_nbSims;
        }
    }
    return ibest;
}


cornedpanda::PlayerKFEMCTS::Node * cornedpanda::PlayerKFEMCTS::computeSelection(Node *node)
{
    assert(not node->_childNodes.empty());

    Node *bestNode = nullptr;
    float bestScore = -1000000.;
    for (Node *childNode : node->_childNodes)
    {
        assert(childNode->_nbSims > 0);
        float exploitation = childNode->_nodeScore;
        float exploration = cornedpanda::fastsqrt(cornedpanda::fastlog(node->_nbSims) / (float)childNode->_nbSims);
        assert(exploration >= 0.);
        float score = exploitation + _parameters.at("PlayerKFEMCTS_kuct") * exploration;
        if (score > bestScore)
        {
            bestNode = childNode;
            bestScore = score;
        }
    }
    assert(bestScore > -1000000.);
    assert(bestNode);
    return bestNode;
}

cornedpanda::Move cornedpanda::PlayerKFEMCTS::computeExpansion(Node *node)
{
    assert(node);
    assert(node->_possibleMoves.size());
    Move m = node->_possibleMoves.back();
    node->_possibleMoves.pop_back();
    return m;
}

int cornedpanda::PlayerKFEMCTS::computeSimulation(Node *node)
{
    assert(node);
    board_t copy = node->_board;
    for (int move = 0; move < _nbRandomMoves; move++)
    {
        const std::vector<Move> & allMoves = copy.getMoves();
        copy.makeMove(allMoves[random(allMoves.size())]);

        if (copy.getGameState() != GAME_STATE_RUNNING)
            return copy.evaluate();
    }

    return  copy.evaluate();
}

void cornedpanda::PlayerKFEMCTS::computeBackPropagation(Node *node,int s)
{
    assert(node);
    while (node)
    {
        node->_nbSims++;

        // moyenne glissante : x_n = (x_{n-1} * (n-1) + x_n) / n
        if (node->_board.isWhiteToMove())
            if (node->_nbSims==1)
                node->_nodeScore = -s;
            else
                // pourquoi "-" et pas "+" ?  Probablemnt pour déterminer le joueur
                node->_nodeScore = (node->_nodeScore * (float) (node->_nbSims - 1.))/(float)node->_nbSims - (float)s / (float)node->_nbSims;
        else
            if (node->_nbSims==1)
                node->_nodeScore = s;
            else
                node->_nodeScore = (node->_nodeScore * (float) (node->_nbSims - 1.))/(float)node->_nbSims + (float)s / (float)node->_nbSims;

        node = node->_parentNode;
    }
}

void cornedpanda::PlayerKFEMCTS::computeOneSimulation()
{
    Node *currentNode = &(_nodes.front());

    // descent (selection step)
    while (not currentNode->_childNodes.empty() and currentNode->_possibleMoves.empty())
    {
        currentNode = computeSelection(currentNode);
        assert(currentNode);
    }

    // expansion (create a new node) unless the current node ends the game
    if (currentNode->_board.getGameState() == GAME_STATE_RUNNING)
    {
        Move m = computeExpansion(currentNode);

        const board_t &b = currentNode->_board; // TODO ca fait une copie de trop ?
        _nodes.emplace_back(b, m, currentNode);
        Node* newNode = &(_nodes.back());
        currentNode->_childNodes.push_back(newNode);

        currentNode = newNode;
    }


    // simulation (random playout)
    int s = computeSimulation(currentNode);

    // backpropagation 
    computeBackPropagation(currentNode, s);
    assert(currentNode->_nbSims > 0);
}

void cornedpanda::PlayerKFEMCTS::statsBestMove()
{
    Player::statsBestMove();

    std::stringstream ss;
    cornedpanda::Output() << "cornedpanda::PlayerKFEMCTS::statsBestMove : PlayerKFEMCTS_nbSims = " << _parameters["PlayerKFEMCTS_nbSimulations"] << '\n';
    cornedpanda::Output() << "cornedpanda::PlayerKFEMCTS::statsBestMove : Evaluation de la position courante =" << _board.evaluate() << '\n';
    cornedpanda::Output() << "cornedpanda::PlayerKFEMCTS::statsBestMove : Nombre de fils = "<< _nodes.front()._childNodes.size() << '\n';
    for (unsigned i=0; i < _nodes.front()._childNodes.size(); i++)
    {
        ss << _board.moveToString(_nodes.front()._childNodes[i]->_move) << " : " << _nodes.front()._childNodes[i]->_nodeScore << " (" << _nodes.front()._childNodes[i]->_nbSims << ")\n";
    }
    cornedpanda::Output() << "cornedpanda::PlayerKFEMCTS::getMove : allScores = \n" << ss.str() << '\n';
    // TODO logstream


    Node *root = &(_nodes.front());

    for (unsigned ichild = 0; ichild < root->_childNodes.size(); ichild++)
    {
        cornedpanda::Output() << "cornedpanda::PlayerKFEMCTS::statsBestMove : coup[" << ichild << "] = " << _board.moveToString(root->_childNodes[ichild]->_move) << '\n';
        cornedpanda::Output() << "cornedpanda::PlayerKFEMCTS::statsBestMove : score[" << ichild << "] = " << root->_childNodes[ichild]->_nodeScore << " sur " << root->_childNodes[ichild]->_nbSims << '\n';
#ifdef DEBUG
        cornedpanda::logStream << "cornedpanda::PlayerKFEMCTS::statsBestMove : coup[" << ichild << "] = " << _board.moveToString(root->_childNodes[ichild]->_move) << '\n';
        cornedpanda::logStream << "cornedpanda::PlayerKFEMCTS::statsBestMove : score[" << ichild << "] = " << root->_childNodes[ichild]->_nodeScore << " sur " << root->_childNodes[ichild]->_nbSims << '\n';
#endif
    }

    cornedpanda::Output() << "cornedpanda::PlayerKFEMCTS::statsBestMove : coup best = " << _board.moveToString(root->_childNodes[_iBest]->_move) << '\n';
    cornedpanda::Output() << "cornedpanda::PlayerKFEMCTS::statsBestMove : score best = " << root->_childNodes[_iBest]->_nodeScore << '\n';
#ifdef DEBUG
    cornedpanda::logStream << "cornedpanda::PlayerKFEMCTS::statsBestMove : coup best = " << _board.moveToString(root->_childNodes[_iBest]->_move) << '\n';
    cornedpanda::logStream << "cornedpanda::PlayerKFEMCTS::statsBestMove : score best = " << root->_childNodes[_iBest]->_nodeScore << '\n';
#endif
}

*/


