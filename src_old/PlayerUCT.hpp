/*
// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef _PLAYERUCT_HPP_
#define _PLAYERUCT_HPP_

#include "Player.hpp"
#include <cassert>

namespace cornedpanda
{
    class PlayerUCT : public Player
    {
        private:
            struct Node {
                int _nbWins;
                int _nbSims;

                board_t _board;
                Move _move;

                std::vector<Node*> _childNodes;
                std::vector<Move> _possibleMoves;

                Node * _parentNode;

                Node(const board_t &);
                Node(const Node &) { assert(0); }
                Node(const board_t &, const Move &n, Node *);
            };

        public:
            PlayerUCT();

            void initBestMove();
            void iterBestMove();
            void statsBestMove();

        private:

            int _iSims;
            int _sSims;
            int _nSims;
            int _iBest;

            Node * computeSelection(Node *node);
            Move computeExpansion(Node *node);
            int getFinalChoice();
            GameState computeSimulation(Node *node);
            void computeBackPropagation(Node *node, GameState endState);
            void computeOneSimulation();

        private:
            std::vector<Node> _nodes;
    };
}

#endif
*/

