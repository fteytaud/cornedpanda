# Copyright © 2014 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

MAINSRC = ./src/cornedpanda.cpp ./src/cornedpanda_test_magicmoves.cpp ./src/cornedpanda_test_players.cpp ./src/cornedpandarbitre.cpp
INSTALLPATH = /usr/local/bin/
override LIBS += -pthread
override CXXFLAGS += -std=c++11 -Wall -Wextra -I/usr/local/include
ifeq ($(DEBUG), 1)
	override CXXFLAGS += -O0 -g -DDEBUG
else
	override CXXFLAGS += -Ofast -DNDEBUG
endif

MESSAGE = "make options: DEBUG=1" 

BINDIR = ./bin
OBJDIR = ./obj
SRCDIR = ./src
SRC = $(filter-out $(MAINSRC), $(shell find $(SRCDIR) -name "*.cpp"))
OBJ = $(subst $(SRCDIR)/, $(OBJDIR)/, $(SRC:.cpp=.o))
BIN = $(subst $(SRCDIR)/, $(BINDIR)/, $(MAINSRC:.cpp=.out))

.PHONY : all clean 
.SECONDARY:

ALL: tags $(BINDIR)/test.out $(BIN)
	@echo $(MESSAGE)
$(BINDIR)/%.out: $(OBJ) $(OBJDIR)/%.o
	mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $@ $^ $(LIBS)
$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) -c $< -o $@
clean:
	@echo $(MESSAGE)
	find $(OBJDIR) -name "*.o" | xargs rm $(BIN) tags $(BINDIR)/test.out test.cpp
install: $(BIN) 
		cp $(BIN) $(INSTALLPATH)

# build ctags data (code navigation)
tags: $(SRC) $(MAINSRC)
	ctags -R

# build unit tests (using the cxxtest framework)
# unit tests should be written in $(SRCDIR)/*_test.hpp files
# execute ./test.out to run tests
CXXTESTBIN = cxxtestgen
CXXTESTOPT = --error-printer
TESTSRC = $(shell find $(SRCDIR) -name "*_test.hpp")
$(BINDIR)/test.out: $(TESTSRC) $(OBJ)
	mkdir -p $(@D)
	$(CXXTESTBIN) $(CXXTESTOPT) -o test.cpp $(TESTSRC)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $@ test.cpp $(OBJ) $(LIBS)

