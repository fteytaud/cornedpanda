#include <iostream>
#include <list>
#include <algorithm>

using namespace std;

const int Taille = 7;

class Coup {
 public:
  char couleur;
  int x, y;
};

class Virus {
 public:
  char damier [Taille] [Taille];

  Virus () {
    init ();
  }

  void init () {
    for (int i = 0; i < Taille; i++)
      for (int j = 0; j < Taille; j++)
	damier [i] [j] = '+';
    damier [0] [0] = '@';
    damier [Taille - 1] [Taille - 1] = '@';
    damier [Taille - 1] [0] = 'O';
    damier [0] [Taille - 1] = 'O';
  }

  char adversaire (char couleur) const {
    if (couleur == 'O')
      return '@';
    return 'O';
  }

  void joue (const Coup & m) {
    damier [m.x] [m.y] = m.couleur;
    char autre = adversaire (m.couleur);
    int debutx = m.x - 1, finx = m.x + 1;
    int debuty = m.y - 1, finy = m.y + 1;
    if (debutx < 0) debutx = 0;
    if (debuty < 0) debuty = 0;
    if (finx > Taille - 1) finx = Taille - 1;
    if (finy > Taille - 1) finy = Taille - 1;
    for (int i = debutx; i <= finx; i++)
      for (int j = debuty; j <= finy; j++)
	if (damier [i] [j] == autre)
	  damier [i] [j] = m.couleur;
  }

  int evaluation (char couleur) const {
    int eval = 0;
    char autre = adversaire (couleur);
    for (int i = 0; i < Taille; i++)
      for (int j = 0; j < Taille; j++)
	if (damier [i] [j] == couleur)
	  eval++;
	else if (damier [i] [j] == autre)
	  eval--;
    return eval;
  }

  bool coupLegal (Coup & coup) {
    if (damier [coup.x] [coup.y] != '+') 
      return false;
    for (int x = max (coup.x - 1, 0); 
	 x <= min (coup.x + 1, Taille - 1); x++)
      for (int y = max (coup.y - 1, 0); 
	   y <= min (coup.y + 1, Taille - 1); y++)
	if (damier [x] [y] == coup.couleur)
	  return true;
    return false;
  }

  list<Coup> coupsLegaux (char couleur) {
    list<Coup> liste;
    Coup coup;
    coup.couleur = couleur;
    for (int i = 0; i < Taille; i++)
      for (int j = 0; j < Taille; j++) {
	coup.x = i;
	coup.y = j;
	if (coupLegal (coup))
	  liste.push_back (coup);
      }
    return liste;
  }
  
  friend ostream & operator << (ostream & sortie, 
				const Virus & v);
};

ostream & operator << (ostream & sortie, 
		       const Virus & v) {
  sortie << "  ";
  for (int i = 0; i < Taille; i++)
    sortie << i << " ";
  sortie << endl;

  for (int i = 0; i < Taille; i++) {
    sortie << i << " ";
    for (int j = 0; j < Taille; j++)
      sortie << v.damier  [j] [i] << " ";
    sortie << endl;
  }
  sortie << "evaluation pour @ = " << 
    v.evaluation ('@') << endl;
  return sortie;
}

Virus virus;

int main () {
  list<Coup> listeCoups;
  while (true) {
    cout << virus;
    listeCoups = virus.coupsLegaux ('@');
    if (listeCoups.empty ())
      break;
    cout << "Donnez votre coup : ";
    Coup coup;
    coup.couleur = '@';
    do {
      cin >> coup.x >> coup.y;
    } while (!virus.coupLegal (coup));
    virus.joue (coup);
    cout << virus;
    listeCoups = virus.coupsLegaux ('O');
    if (listeCoups.empty ())
      break;
    int meilleureEvaluation = - Taille * Taille - 1;
    for (list<Coup>::iterator it = listeCoups.begin ();
	 it != listeCoups.end (); ++it) {
      Virus v = virus;
      v.joue (*it);
      int eval = v.evaluation ('O');
      if (eval > meilleureEvaluation) {
	meilleureEvaluation = eval;
	coup = *it;
      }
    }
    cout << "Je joue en " << coup.x << " " << 
      coup.y << endl;
    virus.joue (coup);
  }
  return 0;
}
