#include <iostream>

using namespace std;

int tas [4] = {7, 5, 3, 1};

int main () {
  int t, nombre;
  while (true) {
    cout << tas [0] << " " << tas [1] << " " << 
            tas [2] << " " << tas [3] << endl;
    if (tas [0] + tas [1] + tas [2] + tas [3] == 0) {
      cout << "J'ai gagne !" << endl;
      break;
    }
    do {
      cout << "Donnez le tas (entre 0 et 3): ";
      cin >> t;
      cout << "Donnez le nombre : ";
      cin >> nombre;
    } while ((t < 0) || (t > 3)  || 
	     (tas [t] < nombre) || (nombre < 0));
    tas [t] -= nombre;
    cout << tas [0] << " " << tas [1] << " " << 
            tas [2] << " " << tas [3] << endl;
    for (int i = 0; i < 4; i++)
      for (int j = 1; j <= tas [i]; j++) {
	tas [i] -= j;
	if ((tas [0] ^ tas [1] ^ tas [2] ^ tas [3]) == 0) {
	  t = i;
	  nombre = j;
	}
	tas [i] += j;
      }
    cout << "Je prend " << nombre << 
            " dans le tas " << t << endl;
    tas [t] -= nombre;
  }
  return 0;
}
