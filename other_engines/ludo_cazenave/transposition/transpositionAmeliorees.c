#include <iostream>
#include <list>
#include <algorithm>
#include <time.h>

using namespace std;

const int Taille = 7;

const int MaxNombre = 2 * Taille * Taille;

unsigned long long scoreHistorique [MaxNombre];
  
void initHistorique () {
  for (int j = 0; j < MaxNombre; j++)
    scoreHistorique [j] = 0;
}

class Coup {
 public:
  char couleur;
  int x, y;

  bool operator != (const Coup & c) {
    if ((couleur != c.couleur) || 
	(x != c.x) ||
	(y != c.y))
      return true;
    return false;
  }

  bool operator < (Coup c) {
    return scoreHistorique [nombre ()] >
           scoreHistorique [c.nombre ()];
  }

  int nombre () {
    if (couleur == '@')
      return x + y * Taille;
    return Taille * Taille + x + y * Taille;
  }

  friend ostream & operator << (ostream & sortie, 
                                const Coup & c);
};

ostream & operator << (ostream & sortie, 
		       const Coup & c) {
  sortie << "(" << c.couleur << "," << c.x << "," << c.y << ")";
  return sortie;
}

unsigned long long HashArray [Taille] [Taille] [2];

/* initialisation des nombres aléatoires */
void initHash () {
  for (int i = 0; i < Taille; i++)
    for (int j = 0; j < Taille; j++)
      for (int k = 0; k < 2; k++) {
        HashArray [i] [j] [k] = 0;
        for (int b = 0; b < 64; b++)
          if ((rand () / (RAND_MAX + 1.0)) > 0.5)
            HashArray [i] [j] [k] |= (1ULL << b);
      }
}

class Virus {
 public:
  char damier [Taille] [Taille];
  int nbNoirs, nbBlancs;
  int nbModifications;
  int pileModifications [20 * Taille * Taille];
  unsigned long long hashcode;


  Virus () {
    init ();
  }

  unsigned long long hash () { return hashcode; }

  void init () {
    for (int i = 0; i < Taille; i++)
      for (int j = 0; j < Taille; j++)
        damier [i] [j] = '+';
    hashcode = 0;
    damier [0] [0] = '@';
    hashcode ^= HashArray [0] [0] [0];
    damier [Taille - 1] [Taille - 1] = '@';
    hashcode ^= HashArray [Taille - 1] [Taille - 1] [0];
    damier [Taille - 1] [0] = 'O';
    hashcode ^= HashArray [Taille - 1] [0] [1];
    damier [0] [Taille - 1] = 'O';
    hashcode ^= HashArray [0] [Taille - 1] [1];
    nbNoirs = 2;
    nbBlancs = 2;
    nbModifications = 0;
  }

  char adversaire (char couleur) const {
    if (couleur == 'O')
      return '@';
    return 'O';
  }

  void joue (const Coup & m) {
    damier [m.x] [m.y] = m.couleur;
    if (m.couleur == '@') 
      hashcode ^= HashArray [m.x] [m.y] [0];
    else 
      hashcode ^= HashArray [m.x] [m.y] [1];
    pileModifications [nbModifications] = m.x;
    nbModifications++;
    pileModifications [nbModifications] = m.y;
    nbModifications++;
    int nbSwaps = 0;
    if (m.couleur == '@') 
      nbNoirs++;
    else
      nbBlancs++;

    char autre = adversaire (m.couleur);
    int debutx = m.x - 1, finx = m.x + 1;
    int debuty = m.y - 1, finy = m.y + 1;
    if (debutx < 0) debutx = 0;
    if (debuty < 0) debuty = 0;
    if (finx > Taille - 1) finx = Taille - 1;
    if (finy > Taille - 1) finy = Taille - 1;
    for (int i = debutx; i <= finx; i++)
      for (int j = debuty; j <= finy; j++)
        if (damier [i] [j] == autre) {
          damier [i] [j] = m.couleur;
	  hashcode ^= HashArray [m.x] [m.y] [0];
	  hashcode ^= HashArray [m.x] [m.y] [1];
	  pileModifications [nbModifications] = i;
	  nbModifications++;
	  pileModifications [nbModifications] = j;
	  nbModifications++;
	  nbSwaps++;
          if (m.couleur == '@') {
	    nbNoirs++;
	    nbBlancs--;
	  }
          else {
	    nbNoirs--;
	    nbBlancs++;
	  }
        }
    pileModifications [nbModifications] = nbSwaps;
    nbModifications++;
  }

  void dejoue (const Coup & m) {
    int x, y, nbSwaps;
    char autre = adversaire (m.couleur);
    nbModifications--;
    nbSwaps = pileModifications [nbModifications];
    for (int i = 0; i < nbSwaps; i++) {
      nbModifications--;
      y = pileModifications [nbModifications];
      nbModifications--;
      x = pileModifications [nbModifications];
      damier [x] [y] = autre;
      hashcode ^= HashArray [x] [y] [0];
      hashcode ^= HashArray [x] [y] [1];
      if (m.couleur == '@') {
	nbNoirs--;
	nbBlancs++;
      }
      else {
	nbNoirs++;
	nbBlancs--;
      }
    }
    nbModifications--;
    y = pileModifications [nbModifications];
    nbModifications--;
    x = pileModifications [nbModifications];
    damier [x] [y] = '+';
    if (m.couleur == '@') {
      hashcode ^= HashArray [x] [y] [0];
      nbNoirs--;
    }
    else {
      hashcode ^= HashArray [x] [y] [1];
      nbBlancs--;
    }
   }

  int evaluation (char couleur) const {
    if (couleur == '@')
      return nbNoirs - nbBlancs;
    return nbBlancs - nbNoirs;
  }

  int evaluationSiPlusDeCoupsPossibles (char couleur) const {
    if (couleur == '@')
      return nbNoirs - (Taille * Taille - nbNoirs);
    else
      return nbBlancs - (Taille * Taille - nbBlancs);
  }

  bool coupLegal (Coup & coup) {
    if (damier [coup.x] [coup.y] != '+') 
      return false;
    for (int x = max (coup.x - 1, 0); 
         x <= min (coup.x + 1, Taille - 1); x++)
      for (int y = max (coup.y - 1, 0); 
           y <= min (coup.y + 1, Taille - 1); y++)
        if (damier [x] [y] == coup.couleur)
          return true;
    return false;
  }

  list<Coup> coupsLegaux (char couleur) {
    list<Coup> liste;
    Coup coup;
    coup.couleur = couleur;
    for (int i = 0; i < Taille; i++)
      for (int j = 0; j < Taille; j++) {
        coup.x = i;
        coup.y = j;
        if (coupLegal (coup))
          liste.push_back (coup);
      }
    return liste;
  }
  
  int nbCasesModifiees (Coup & m) {
    int nb = 1;
    char autre = adversaire (m.couleur);
    int debutx = m.x - 1, finx = m.x + 1;
    int debuty = m.y - 1, finy = m.y + 1;
    if (debutx < 0) debutx = 0;
    if (debuty < 0) debuty = 0;
    if (finx > Taille - 1) finx = Taille - 1;
    if (finy > Taille - 1) finy = Taille - 1;
    for (int i = debutx; i <= finx; i++)
      for (int j = debuty; j <= finy; j++)
        if (damier [i] [j] == autre) 
	  nb++;
    return nb;
  }

  list<Coup> coupsQuiescence (char couleur) {
    list<Coup> liste, l = coupsLegaux (couleur);
    for (list<Coup>::iterator it = l.begin (); it != l.end (); ++it)
      if (nbCasesModifiees (*it) > 4)
	liste.push_back (*it);
    return liste;
  }
  
  friend ostream & operator << (ostream & sortie, 
                                const Virus & v);
};

ostream & operator << (ostream & sortie, 
		       const Virus & v) {
  sortie << "  ";
  for (int i = 0; i < Taille; i++)
    sortie << i << " ";
  sortie << endl;

  for (int i = 0; i < Taille; i++) {
    sortie << i << " ";
    for (int j = 0; j < Taille; j++)
      sortie << v.damier  [j] [i] << " ";
    sortie << endl;
  }
  sortie << "evaluation pour @ = " << 
    v.evaluation ('@') << endl;
  return sortie;
}

int SizeTable = 65535; // une puissance de 2 moins 1

template <class Move>
class GenericTranspo {
 public:
  bool scoreExact;
  short int score;
  unsigned char depth;
  Move best;
  unsigned long long hash;
};

template <class Move, class Board, class Transpo>
class Table {
  Transpo * table;
  
 public:
  Table () {
    table = new Transpo [SizeTable];
  }
  
  ~Table() {delete [] table;}
  
  Transpo * look (Board * b) {
    Transpo * trans = & table [b->hash () & SizeTable];
    if (trans->hash == b->hash ())
      return trans;
    return NULL;
  }
  
  bool add (Board * b, unsigned char depth, 
            short int score, 
            const Move & best, bool scoreExact) {
    Transpo * trans = & table [b->hash () & SizeTable];
        
    if (trans->depth >= depth)
      return false;
    trans->hash = b->hash ();
    trans->score = score;
    trans->scoreExact = scoreExact;
    trans->depth = depth;
    trans->best = best;
    return true;
  }
};

Table<Coup, Virus, GenericTranspo<Coup> > TT;

Virus virus;

clock_t maxClock = 2 * CLOCKS_PER_SEC; // 2 secondes
clock_t clockStart;
const int ProfondeurMax = 64;

int quiescence (int alpha, int beta, char joueur) {
  if (clock () - clockStart > maxClock)
    return 0;

  list<Coup> coups = virus.coupsQuiescence (joueur);

  if (coups.empty ())
    return virus.evaluation (joueur);

  char autre = virus.adversaire (joueur);
  for (list<Coup>::iterator it = coups.begin(); 
       it != coups.end(); ++it) {
    virus.joue (*it);
    int eval = -quiescence (-beta, -alpha, autre);
    virus.dejoue (*it);
    if (eval > alpha)
      alpha = eval;
    if (alpha >= beta)
      return beta;
  }
  return alpha;
}

int alphabeta (int depth, int alpha, int beta, char joueur, list<Coup> & vp);

void joueAlphaBeta (Coup & coup, int depth, int & alpha, int & beta, char joueur, list<Coup> &vp) {
  char autre = virus.adversaire (joueur);
  virus.joue (coup);
  list<Coup> vptemp;
  int eval = -alphabeta (depth - 1, -beta, -alpha, autre, vptemp);
  if (eval > alpha) {
    alpha = eval;
    vp = vptemp;
    vp.push_front (coup);
  }
  virus.dejoue (coup);
  if (alpha >= beta) {
    scoreHistorique [coup.nombre ()] += 4 << (depth * 2);
  }
}

int alphabeta (int depth, int alpha, int beta, char joueur, list<Coup> & vp) {
  if (clock () - clockStart > maxClock)
    return 0;

  if (depth == 0)
    return quiescence (alpha, beta, joueur);

  char autre = virus.adversaire (joueur);
  /* Coupes selectives avec coup nul */
  int R = 2;
  if (depth > R + 1) {
    list<Coup> vptemp;
    int eval = -alphabeta (depth - 1 - R, -beta, -beta + 1, autre, vptemp);
    if (eval >= beta)
      return beta;
  }
  /* fin des coupes selectives avec coup nul */

  Coup transpoMove;
  GenericTranspo<Coup> *t = TT.look (&virus);
  if (t != NULL) {
    if (t->depth >= depth) {
      if (t->scoreExact) return t->score;
      else alpha = max (alpha, (int)t->score);
      if (alpha >= beta) return beta;
    }
    transpoMove = t->best;
  }

  list<Coup> listeCoups = virus.coupsLegaux (joueur);
  if (listeCoups.empty ())
    return virus.evaluationSiPlusDeCoupsPossibles (joueur);
    
  /* coupes de transposition améliorées */
  if (depth > 2)
    for (list<Coup>::iterator it = listeCoups.begin();
	 it != listeCoups.end() && alpha < beta;
	 ++it) {
      virus.joue (*it);
      GenericTranspo<Coup> * t1 = TT.look (&virus);
      if (t1 != NULL)
	if (t1->depth >= depth - 1)
	  if (t1->scoreExact)
	    alpha = max (alpha, -t1->score);
      virus.dejoue (*it);
      if (alpha >= beta) return beta;
    }

  /* on teste le coup de transposition en premier */
  if (t != NULL && alpha < beta)
    if (virus.coupLegal (transpoMove))
      joueAlphaBeta (transpoMove, depth, alpha, beta, joueur, vp);


  if (alpha < beta) {
    listeCoups.sort ();
    
    for (list<Coup>::iterator it = listeCoups.begin ();
	 (it != listeCoups.end ()) && (alpha < beta); ++it) 
      if ((t == NULL) || (*it != transpoMove))
	joueAlphaBeta (*it, depth, alpha, beta, joueur, vp);
  }
  
  if (alpha >= beta) 
    TT.add (&virus, depth, alpha, *vp.begin (), false);
  else if (!vp.empty ())
    TT.add (&virus, depth, alpha, *vp.begin (), true);
  else if (!listeCoups.empty ())
    TT.add (&virus, depth, alpha, *listeCoups.begin (), true);
  else 
    cerr << "bug\n";
  
  return alpha;
}

int approfondissementIteratif (list<Coup> & vp) {
  int eval = virus.evaluation ('O');
  list<Coup> vpTemp;
  clockStart = clock ();
  initHistorique ();
  for (int d = 1; 
       (clock () - clockStart < maxClock) && (d < ProfondeurMax); 
       d++) {
    int evalTemp = alphabeta (d, -Taille * Taille, Taille * Taille, 'O', vpTemp);
    if (clock () - clockStart < maxClock) {
      eval = evalTemp;
      vp = vpTemp;
    }
  }
  return eval;
}

int main () {
  initHash ();
  list<Coup> listeCoups;
  while (true) {
    cout << virus;
    listeCoups = virus.coupsLegaux ('@');
    if (listeCoups.empty ())
      break;
    cout << "Donnez votre coup : ";
    Coup coup;
    coup.couleur = '@';
    do {
      cin >> coup.x >> coup.y;
    } while (!virus.coupLegal (coup));
    virus.joue (coup);
    cout << virus;
    listeCoups = virus.coupsLegaux ('O');
    if (listeCoups.empty ())
      break;
    list<Coup> vp;
    int eval = approfondissementIteratif (vp);
    coup = *vp.begin ();
    cout << "eval = " << eval << endl;
    cout << "Variation : ";
    for (list<Coup>::iterator it = vp.begin (); it != vp.end (); ++it)
      cout << *it  << " ";
    cout << endl;
    cout << "Je joue en " << coup.x << " " << 
      coup.y << endl;
    virus.joue (coup);
  }
  return 0;
}
