#include <iostream>
#include <list>
#include <algorithm>

using namespace std;

const int Taille = 11;

const int JoueurGauche = 0;
const int JoueurDroit = 1;

class Zone {
 public:
  unsigned long long z [3];

  Zone () { init (); }

  void init () {
    z [0] = 0;
    z [1] = 0;
    z [2] = 0;
  }

  Zone operator + (Zone zone) {
    Zone res;
    res.z [0] = z [0] | zone.z[0];
    res.z [1] = z [1] | zone.z[1];
    res.z [2] = z [2] | zone.z[2];
    return res;
  }

  bool marquee (int x, int y) {
    int inter = x + y * Taille;
    return (z [inter / 64] & (1ULL << (inter % 64))) > 0;
  }
 
  void marque (int x, int y) {
    int inter = x + y * Taille;
    z [inter / 64] |= (1ULL << (inter % 64));
  }

  friend ostream & operator << (ostream & sortie, 
			        Zone & z);
};

ostream & operator << (ostream & sortie, 
		       Zone & z) {
  sortie << "  ";
  for (int i = 0; i < Taille; i++)
    sortie << i << " ";
  sortie << endl;

  for (int i = 0; i < Taille; i++) {
    sortie << i << " ";
    for (int j = 0; j < Taille; j++)
      if (z.marquee (j, i))
	sortie << "1 ";
      else
	sortie << "0 ";
    sortie << endl;
  }
  return sortie;
}

Zone zone;

class Intersection {
 public:
  int x, y;

  bool legale () {
    return ((x >= 0) && (x < Taille) &&
	    (y >= 0) && (y < Taille));
  }

  bool operator == (Intersection & inter) {
    if ((x != inter.x) || (y != inter.y))
      return false;
    return true;
  }

};

class Coup {
 public:
  list<Intersection> l;

  bool operator == (Coup & coup) {
    if (l.size () != coup.l.size ())
      return false;

    list<Intersection>::iterator it = l.begin ();
    list<Intersection>::iterator it1 = coup.l.begin ();
    while (true) {
      if ((it->x != it1->x) || (it->y != it1->y))
	return false;
      it++;
      it1++;
      if (it == l.end ())
	return true;
    }
  }

  friend istream & operator >> (istream & entree, 
			        Coup & c);

  friend ostream & operator << (ostream & sortie, 
			        Coup & c);
};

istream & operator >> (istream & entree, 
		       Coup & c) {
  Intersection inter;
  entree >> inter.x >> inter.y;
  while (true) {
    c.l.push_back (inter);
    entree >> inter.x;
    if (inter.x == -1)
      break;
    entree >> inter.y;
  }
  return entree;
}

ostream & operator << (ostream & sortie, 
		       Coup & c) {
  for (list<Intersection>::iterator it = c.l.begin ();
       it != c.l.end (); ++it)
    sortie << "(" << it->x << "," << it->y << ")";
  return sortie;
}


class Phutball {
 public:
  char damier [Taille] [Taille];
  Intersection balle;

  Phutball () {
    init ();
  }

  void init () {
    for (int i = 0; i < Taille; i++)
      for (int j = 0; j < Taille; j++)
	damier [i] [j] = '+';
    damier [Taille / 2] [Taille / 2] = '@';
    balle.x = Taille / 2;
    balle.y = Taille / 2;
  }

  void joue (Coup & c) {
    list<Intersection>::iterator it = c.l.begin ();
    if (c.l.size () == 1)
      damier [it->x] [it->y] = 'O';
    else {
      list<Intersection>::iterator precedent;
      while (true) {
	precedent = it;
	it++;
	if (it == c.l.end ())
	  break;
	damier [precedent->x] [precedent->y] = '+';
      }
      if (precedent->legale ()) {
	damier [precedent->x] [precedent->y] = '@';
	balle = *precedent;
      }
    }
    //cerr << *this;
  }

  void dejoue (Coup & c) {
    list<Intersection>::iterator it = c.l.begin ();
    if (c.l.size () == 1)
      damier [it->x] [it->y] = '+';
    else {
      damier [balle.x] [balle.y] = '+';
      balle = *it;
      it++;
      list<Intersection>::iterator precedent;
      while (true) {
	precedent = it;
	it++;
	if (it == c.l.end ())
	  break;
	damier [precedent->x] [precedent->y] = 'O';
      }
      damier [balle.x] [balle.y] = '@';
    }
  }

  bool gagne (int joueur, Intersection inter) {
    if (((joueur == JoueurDroit) && (inter.x < 0)) ||
	((joueur == JoueurDroit) && inter.legale () && (inter.x == 0)) ||
	((joueur == JoueurGauche) && (inter.x > Taille - 1)) ||
	((joueur == JoueurGauche) && inter.legale () && (inter.x == Taille - 1)))
      return true;
    return false;
  }

  bool coupsLegaux (int joueur, list<Coup> & liste) {
    liste.clear ();
    for (int i = 0; i < Taille; i++)
      for (int j = 0; j < Taille; j++) 
	if (damier [i] [j] == '+') {
	  Intersection inter;
	  inter.x = i;
	  inter.y = j;
	  Coup coup;
	  coup.l.push_back (inter);
	  liste.push_back (coup);
	}
    Coup coup;
    coup.l.push_back (balle);
    damier [balle.x] [balle.y] = '+';
    bool gagne = coupsBalle (joueur, coup, liste, zone);
    damier [balle.x] [balle.y] = '@';
    return gagne;
  }
  
  bool coupsLegaux (int joueur, list<Coup> & liste, Zone z) {
    liste.clear ();
    for (int i = 0; i < Taille; i++)
      for (int j = 0; j < Taille; j++) 
	if ((damier [i] [j] == '+') && z.marquee (i, j)) {
	  Intersection inter;
	  inter.x = i;
	  inter.y = j;
	  Coup coup;
	  coup.l.push_back (inter);
	  liste.push_back (coup);
	}
    Coup coup;
    coup.l.push_back (balle);
    damier [balle.x] [balle.y] = '+';
    bool gagne = coupsBalle (joueur, coup, liste, zone);
    damier [balle.x] [balle.y] = '@';
    return gagne;
  }
  
  bool coupsLegaux (int ordre, int joueur, list<Coup> & liste, Zone & z) {
    bool gagne = false;
    liste.clear ();
    if (ordre > 0)
      coupsVoisinsBalle (joueur, liste, z);
    if (ordre != 1) {
      Coup coup;
      coup.l.push_back (balle);
      damier [balle.x] [balle.y] = '+';
      gagne = coupsBalle (joueur, coup, liste, z);
      damier [balle.x] [balle.y] = '@';
    }
    return gagne;
  }
  
  bool coupsBalle (int joueur, Coup & coup, list<Coup> & liste, Zone & z) {
    bool coupGagnant = false;
    for (int i = -1; i <= 1; i++)
      for (int j = -1; j <= 1; j++) {
	Intersection inter = balle;
	inter.x += i;
	inter.y += j;
	z.marque (inter.x, inter.y);
	if (inter.legale ())
	  if (damier [inter.x] [inter.y] == 'O') {
	    Coup coup1 = coup;
	    list<Intersection> pierresEnlevees;
	    while (true) {
	      coup1.l.push_back (inter);
	      pierresEnlevees.push_back (inter);
	      inter.x += i;
	      inter.y += j;
	      z.marque (inter.x, inter.y);
	      if (!inter.legale () || 
		  (damier [inter.x] [inter.y] != 'O')) 
		break;
	    }
	    if (gagne (joueur, inter)) {
	      coup1.l.push_back (inter);
	      liste.push_back (coup1);
	      coupGagnant = true;
	    }
	    else if (inter.legale ()) {
	      if (damier [inter.x] [inter.y] == '+') {
		// on memorise la place de la balle
		Intersection ancienneBalle = balle;
		// on deplace la balle
		balle = inter;
		// on enleve les pierres sautees
		for (list<Intersection>::iterator it = pierresEnlevees.begin ();
		     it != pierresEnlevees.end (); ++it) 
		  damier [it->x] [it->y] = '+';
		// on ajoute le nouveau coup
		Coup coup2 = coup1;
		coup2.l.push_back (inter);
		liste.push_back (coup2);
		// on regarde les coups suivants
		if (coupsBalle (joueur, coup1, liste, z))
		  coupGagnant = true;
		// on remet les pierres enlevees
		for (list<Intersection>::iterator it = pierresEnlevees.begin ();
		     it != pierresEnlevees.end (); ++it) 
		  damier [it->x] [it->y] = 'O';
		// on remet la balle
		balle = ancienneBalle;
	      }
	      else
		cerr << "bug coupsBalle \n";
	    }
	  }
	}
    return coupGagnant;
  }

  bool elementCoup (Coup & coup, list<Coup> & liste) {
    for (list<Coup>::iterator it = liste.begin ();
	 it != liste.end (); ++it) 
      if (coup == *it) 
	return true;
    return false;
  }
  
  bool ajouteCoup (Coup & coup, list<Coup> & liste) {
    if (!elementCoup (coup, liste)) {
      liste.push_back (coup);
      return true;
    }
    return false;
  }
  
  bool elementIntersection (Intersection & inter, list<Intersection> & liste) {
    for (list<Intersection>::iterator it = liste.begin ();
	 it != liste.end (); ++it) 
      if (inter == *it) 
	return true;
    return false;
  }
  
  bool ajouteIntersection (Intersection & inter, list<Intersection> & liste) {
    if (!elementIntersection (inter, liste)) {
      liste.push_back (inter);
      return true;
    }
    return false;
  }
  
  void intersectionsBalle (Intersection interBalle, list<Intersection> & liste) {
    if (!elementIntersection (interBalle, liste)) {
      ajouteIntersection (interBalle, liste);
      for (int i = -1; i <= 1; i++)
	for (int j = -1; j <= 1; j++) {
	  Intersection inter = interBalle;
	  inter.x += i;
	  inter.y += j;
	  if (inter.legale ()) {
	    if (damier [inter.x] [inter.y] == 'O') {
	      while (true) {
		inter.x += i;
		inter.y += j;
		if (!inter.legale () || 
		    (damier [inter.x] [inter.y] != 'O')) 
		  break;
	      }
	      if (inter.legale () && (damier [inter.x] [inter.y] == '+'))
		intersectionsBalle (inter, liste);
	    }
	  }
	}
    }
  }
  
  void coupsVoisinsBalle (int joueur, list<Coup> & listeCoups, Zone & z) {
    list<Intersection> liste;
    intersectionsBalle (balle, liste);
    int meilleur;
    if (joueur == JoueurDroit)
      meilleur = Taille;
    else
      meilleur = -1;
    for (list<Intersection>::iterator it = liste.begin ();
	 it != liste.end (); ++it) {
      for (int i = -1; i <= 1; i++)
	for (int j = -1; j <= 1; j++) {
	  Intersection inter = *it;
	  inter.x += i;
	  inter.y += j;
	  if (inter.legale ())
	    if (damier [inter.x] [inter.y] == '+') {
	      if (((joueur == JoueurDroit) && (inter.x < meilleur)) ||
		  ((joueur == JoueurGauche) && (inter.x > meilleur)))
		meilleur = inter.x;
	    }
	}
    }
    for (list<Intersection>::iterator it = liste.begin ();
	 it != liste.end (); ++it) {
      for (int i = -1; i <= 1; i++)
	for (int j = -1; j <= 1; j++) {
	  Intersection inter = *it;
	  inter.x += i;
	  inter.y += j;
	  if (inter.legale ())
	    if (damier [inter.x] [inter.y] == '+') 
/* 	      if (((joueur == JoueurDroit) && (inter.x <= meilleur + 1)) || */
/* 		  ((joueur == JoueurGauche) && (inter.x >= meilleur - 1))) { */
	      if (inter.x == meilleur) {
		Coup coup;
		coup.l.push_back (inter);
		ajouteCoup (coup, listeCoups);
		z.marque (inter.x, inter.y);
	      }
	}
    }
  }

  friend ostream & operator << (ostream & sortie, 
			        Phutball & v);
};

ostream & operator << (ostream & sortie, 
		       Phutball & v) {
  sortie << "  ";
  for (int i = 0; i < Taille; i++)
    sortie << i << " ";
  sortie << endl;

  for (int i = 0; i < Taille; i++) {
    sortie << i << " ";
    for (int j = 0; j < Taille; j++)
      sortie << v.damier  [j] [i] << " ";
    sortie << endl;
  }
  return sortie;
}

Phutball phutball;

int adversaire (int joueur) {
  if (joueur == JoueurGauche)
    return JoueurDroit;
  return JoueurGauche;
}

class Menace {
 public:
  int nbOrdre [10];

  Menace (int n0 = 0, int n1 = 0, int n2 = 0, int n3 = 0) {
    for (int i = 0; i < 10; i++) 
      nbOrdre [i] = 0;
    nbOrdre [0] = n0;
    nbOrdre [1] = n1;
    nbOrdre [2] = n2;
    nbOrdre [3] = n3;
  }

  void ecrete (int n) {
    for (int i = n; i < 10; i++) 
      nbOrdre [i] = 0;
  }
  
  int ordre () {
    for (int i = 0; i < 9; i++) 
      if (nbOrdre [i + 1] == 0)
	return i;
    return 9;
  }

  Menace moitie () {
    Menace m;
    for (int i = 0; i < 10; i++) 
      m.nbOrdre [i] = nbOrdre [i] / 2;
    return m;
  }

  friend ostream & operator << (ostream & sortie, 
			        Menace & m);
};

ostream & operator << (ostream & sortie, 
		       Menace & m) {
  sortie << "(" << m.nbOrdre [0] << "," <<
    m.nbOrdre [1] <<  "," << m.nbOrdre [2] <<  "," << 
    m.nbOrdre [3] << ")";
  return sortie;
}

bool lambda (int joueur, Menace m, Zone & z) {
  Zone z2;
  if (phutball.gagne (joueur, phutball.balle))
    return true;
  int autre = adversaire (joueur);
  if (phutball.gagne (autre, phutball.balle))
    return false;
  list<Coup> listeCoups, listeCoupsAdverses;
  if (phutball.coupsLegaux (0, joueur, listeCoups, z2))
    return true;
  int n = m.ordre ();
  if (phutball.coupsLegaux (n, joueur, listeCoups, z2))
    return true;
  if (n == 0)
    return false;
    Menace moitie = m.moitie ();
    if (lambda (joueur, moitie, z)) 
    return true;
/*   if (n >= 2) */
/*     cerr << phutball; */
  Zone z3;
  for (list<Coup>::iterator it = listeCoups.begin ();
       it != listeCoups.end (); ++it) {
    z3.init ();
    phutball.joue (*it);
    m.nbOrdre [n]--;
/*     cerr << phutball; */
    bool menaceVerifiee = false;
/*     if (lambda (joueur, m, depth - 1)) */
/*       menaceVerifiee = true; */
    Menace moitie = m.moitie ();
    Zone z1;
    for (int ordre = 0; (ordre < n) && !menaceVerifiee; ordre++) {
      Menace m1 = moitie;
      m1.ecrete (ordre + 1);
      z1.init ();
      if (lambda (joueur, m1, z1))
	menaceVerifiee = true;
    }
/*     if (menaceVerifiee && (n >= 2)) { */
/*       cerr << phutball << "\nz1\n" << z1; */
/*     } */
    z3 = z3 + z1;
    bool gagneVerifie = false;
    if (menaceVerifiee)
      if (!phutball.coupsLegaux (autre, listeCoupsAdverses, z1))
	for (int ordre = 0; ordre <= n; ordre++) {
	  gagneVerifie = true;
	  Menace m1 = m;
	  m1.ecrete (ordre + 1);
	  for (list<Coup>::iterator it1 = listeCoupsAdverses.begin ();
	       (it1 != listeCoupsAdverses.end ()) && gagneVerifie; ++it1) {
	    phutball.joue (*it1);
	    if (!lambda (joueur, m1, z3))
	      gagneVerifie = false;
/* 	    if (n >= 2) */
/* 	      cerr << z3 << *it1 << "\n"; */
	    phutball.dejoue (*it1);
	  }
	}
    phutball.dejoue (*it);
    m.nbOrdre [n]++;
    if (gagneVerifie) {
      z = z + z2;
      z = z + z3;
/*       if (n >= 2) { */
/* 	cerr << phutball << "\ngagne\n" << z2; */
/*       } */
      return true;
    }
  }
  return false;
}

int main () {
  list<Coup> listeCoups;
  while (true) {
    cout << phutball;
    if (phutball.coupsLegaux (JoueurGauche, listeCoups)) {
      cout << "vous avez gagne !" << endl;
      break;
    }
    Menace m (30, 15, 6, 1);
    //Menace m (8, 4, 2);
    //Menace m (6, 3, 2);
    //Menace m (4, 4, 1);
    cout << "test lambda " << m << " = " << lambda (JoueurGauche, m, zone) << endl;
    Coup coup;
    cin >> coup;
    phutball.joue (coup);
    cout << phutball;
    if (phutball.coupsLegaux (JoueurDroit, listeCoups)) {
      cout << "j'ai gagne !" << endl;
      break;
    }
    int i = 0, indice = rand () % listeCoups.size ();
    for (list<Coup>::iterator it = listeCoups.begin ();
	 it != listeCoups.end (); ++it) {
      if (i == indice) {
	coup = *it;
	break;
      }
      i++;
    }
    cout << "Je joue en " << coup << endl;
    phutball.joue (coup);
  }
  return 0;
}
