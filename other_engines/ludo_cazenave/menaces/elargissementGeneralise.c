#include <iostream>
#include <list>
#include <algorithm>

using namespace std;

const int Taille = 9;

const int JoueurGauche = 0;
const int JoueurDroit = 1;

class Intersection {
 public:
  int x, y;

  bool legale () {
    return ((x >= 0) && (x < Taille) &&
	    (y >= 0) && (y < Taille));
  }
};

class Coup {
 public:
  list<Intersection> l;

  friend istream & operator >> (istream & entree, 
			        Coup & c);

  friend ostream & operator << (ostream & sortie, 
			        Coup & c);
};

istream & operator >> (istream & entree, 
		       Coup & c) {
  Intersection inter;
  entree >> inter.x >> inter.y;
  while (true) {
    c.l.push_back (inter);
    entree >> inter.x;
    if (inter.x == -1)
      break;
    entree >> inter.y;
  }
  return entree;
}

ostream & operator << (ostream & sortie, 
		       Coup & c) {
  for (list<Intersection>::iterator it = c.l.begin ();
       it != c.l.end (); ++it)
    sortie << "(" << it->x << "," << it->y << ")";
  return sortie;
}


class Phutball {
 public:
  char damier [Taille] [Taille];
  Intersection balle;

  Phutball () {
    init ();
  }

  void init () {
    for (int i = 0; i < Taille; i++)
      for (int j = 0; j < Taille; j++)
	damier [i] [j] = '+';
    damier [Taille / 2] [Taille / 2] = '@';
    balle.x = Taille / 2;
    balle.y = Taille / 2;
  }

  void joue (Coup & c) {
    list<Intersection>::iterator it = c.l.begin ();
    if (c.l.size () == 1)
      damier [it->x] [it->y] = 'O';
    else {
      list<Intersection>::iterator precedent;
      while (true) {
	precedent = it;
	it++;
	if (it == c.l.end ())
	  break;
	damier [precedent->x] [precedent->y] = '+';
      }
      if (precedent->legale ()) {
	damier [precedent->x] [precedent->y] = '@';
	balle = *precedent;
      }
    }
    //cerr << *this;
  }

  void dejoue (Coup & c) {
    list<Intersection>::iterator it = c.l.begin ();
    if (c.l.size () == 1)
      damier [it->x] [it->y] = '+';
    else {
      damier [balle.x] [balle.y] = '+';
      balle = *it;
      it++;
      list<Intersection>::iterator precedent;
      while (true) {
	precedent = it;
	it++;
	if (it == c.l.end ())
	  break;
	damier [precedent->x] [precedent->y] = 'O';
      }
      damier [balle.x] [balle.y] = '@';
    }
  }

  bool gagne (int joueur) {
    if (((joueur == JoueurDroit) && (balle.x <= 0)) ||
	((joueur == JoueurGauche) && (balle.x >= Taille - 1))) 
      return true;
    return false;
  }

  bool coupsLegaux (int joueur, list<Coup> & liste) {
    liste.clear ();
    for (int i = 0; i < Taille; i++)
      for (int j = 0; j < Taille; j++) 
	if (damier [i] [j] == '+') {
	  Intersection inter;
	  inter.x = i;
	  inter.y = j;
	  Coup coup;
	  coup.l.push_back (inter);
	  liste.push_back (coup);
	}
    Coup coup;
    coup.l.push_back (balle);
    damier [balle.x] [balle.y] = '+';
    bool gagne = coupsBalle (joueur, coup, liste);
    damier [balle.x] [balle.y] = '@';
    return gagne;
}
  
  bool coupsBalle (int joueur, Coup & coup, list<Coup> & liste) {
    bool gagne = false;
    for (int i = -1; i <= 1; i++)
      for (int j = -1; j <= 1; j++) {
	Intersection inter = balle;
	inter.x += i;
	inter.y += j;
	if (inter.legale ())
	  if (damier [inter.x] [inter.y] == 'O') {
	    Coup coup1 = coup;
	    list<Intersection> pierresEnlevees;
	    while (true) {
	      coup1.l.push_back (inter);
	      pierresEnlevees.push_back (inter);
	      inter.x += i;
	      inter.y += j;
	      if ((inter.x < 0) || (inter.x >= Taille) || 
		  (inter.y < 0) || (inter.y >= Taille) || 
		  (damier [inter.x] [inter.y] != 'O')) 
		break;
	    }
	    if (((joueur == JoueurDroit) && (inter.x <= 0)) ||
		((joueur == JoueurGauche) && (inter.x >= Taille - 1))) {
	      coup1.l.push_back (inter);
	      liste.push_back (coup1);
	      gagne = true;
	    }
	    else if ((inter.x >= 0) && (inter.x < Taille) && (inter.y >= 0) && (inter.y < Taille)) {
	      if (damier [inter.x] [inter.y] == '+') {
		// on memorise la place de la balle
		Intersection ancienneBalle = balle;
		// on deplace la balle
		balle = inter;
		// on enleve les pierres sautees
		for (list<Intersection>::iterator it = pierresEnlevees.begin ();
		     it != pierresEnlevees.end (); ++it) 
		  damier [it->x] [it->y] = '+';
		// on ajoute le nouveau coup
		Coup coup2 = coup1;
		coup2.l.push_back (inter);
		liste.push_back (coup2);
		// on regarde les coups suivants
		if (coupsBalle (joueur, coup1, liste))
		  gagne = true;
		// on remet les pierres enlevees
		for (list<Intersection>::iterator it = pierresEnlevees.begin ();
		     it != pierresEnlevees.end (); ++it) 
		  damier [it->x] [it->y] = 'O';
		// on remet la balle
		balle = ancienneBalle;
	      }
	      else
		cerr << "bug coupsBalle \n";
	    }
	  }
	}
    return gagne;
  }

  friend ostream & operator << (ostream & sortie, 
			        Phutball & v);
};

ostream & operator << (ostream & sortie, 
		       Phutball & v) {
  sortie << "  ";
  for (int i = 0; i < Taille; i++)
    sortie << i << " ";
  sortie << endl;

  for (int i = 0; i < Taille; i++) {
    sortie << i << " ";
    for (int j = 0; j < Taille; j++)
      sortie << v.damier  [j] [i] << " ";
    sortie << endl;
  }
  return sortie;
}

Phutball phutball;

int adversaire (int joueur) {
  if (joueur == JoueurGauche)
    return JoueurDroit;
  return JoueurGauche;
}

bool lambda (int joueur, int n, int depth) {
  if (phutball.gagne (joueur))
    return true;
  int autre = adversaire (joueur);
  if (phutball.gagne (autre))
    return false;
  list<Coup> listeCoups, listeCoupsAdverses;
  if (phutball.coupsLegaux (joueur, listeCoups))
    return true;
  if ((n == 0) || (depth <= 0))
    return false;
  if (lambda (joueur, n - 1, depth)) 
    return true;
  //if (n == 2)
  // cerr << phutball;
  for (list<Coup>::iterator it = listeCoups.begin ();
       it != listeCoups.end (); ++it) {
    bool menaceVerifiee = false;
    phutball.joue (*it);
    if (lambda (joueur, n - 1, depth - 1)) {
      if (!phutball.coupsLegaux (autre, listeCoupsAdverses))
	for (int ordre = 0; ordre <= n; ordre++) {
	  for (int d = 2; d <= depth - 2; d += 2) {
	    menaceVerifiee = true;
	    for (list<Coup>::iterator it1 = listeCoupsAdverses.begin ();
		 (it1 != listeCoupsAdverses.end ()) && menaceVerifiee; ++it1) {
	      phutball.joue (*it1);
	      if (!lambda (joueur, ordre, d))
		menaceVerifiee = false;
	      phutball.dejoue (*it1);
	    }
	    if (menaceVerifiee)
	      break;
	  }
	  if (menaceVerifiee)
	    break;
	}
    }
    phutball.dejoue (*it);
    if (menaceVerifiee)
      return true;
  }
  return false;
}

int main () {
  list<Coup> listeCoups;
  while (true) {
    cout << phutball;
    if (phutball.coupsLegaux (JoueurGauche, listeCoups)) {
      cout << "vous avez gagne !" << endl;
      break;
    }
    for (int l = 0; l < 3; l++)
      cout << "test lambda " << l << " = " << lambda (JoueurGauche, l, 12) << endl;
    Coup coup;
    cin >> coup;
    phutball.joue (coup);
    cout << phutball;
    if (phutball.coupsLegaux (JoueurDroit, listeCoups)) {
      cout << "j'ai gagne !" << endl;
      break;
    }
    int i = 0, indice = rand () % listeCoups.size ();
    for (list<Coup>::iterator it = listeCoups.begin ();
	 it != listeCoups.end (); ++it) {
      if (i == indice) {
	coup = *it;
	break;
      }
      i++;
    }
    cout << "Je joue en " << coup << endl;
    phutball.joue (coup);
  }
  return 0;
}
