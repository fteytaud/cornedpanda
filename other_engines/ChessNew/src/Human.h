/*
 * =====================================================================================
 *
 *       Filename:  Human.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  29/09/2014 18:26:42
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Fabien Teytaud (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef __HUMAN__
#define __HUMAN__

#include"Player.h"

class Human:public Player
{
	public:
		Human(bool c, Board *b, Game *g):Player(c,b,g) {}
		Move* getMove();
};

#endif
