/*
 * =====================================================================================
 *
 *       Filename:  Game.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  29/09/2014 18:37:05
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Fabien Teytaud (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef __GAME__
#define __GAME__

#include"Board.h"
#include"Player.h"

using namespace std;

class Player;
class Board;

class Game
{
	public:
		Board *board;
		short winner;	// 0 white; 1 black; -1 nobody
		short draw;
		// 0: pat;
		// 1: 3 repeated positions
		// 2: insufficient material
		// 3: 50 moves;

		Player *players[2];

		//	Game(Player *, Player *, bool tp=WHITE);
		//	Game(Board *b);
		Game(Board *);
		bool isFinished(); // 0: not finished, 1: finished
		void playAGame();
};

#endif
