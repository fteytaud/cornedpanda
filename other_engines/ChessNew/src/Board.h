/*
 * =====================================================================================
 *
 *       Filename:  Board.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  26/09/2014 18:27:10
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Fabien Teytaud (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef __BOARD__
#define __BOARD__

#include"defines.h"
#include"Move.h"

using namespace std;

class Board
{
	public:
		bool toPlay;
		bool castlingOO[2];
		bool castlingOOO[2];
		bool playerInCheck[2];
		short nbOfMovesWithoutCaptureOrPawnMove;
		U64 pieces[6][2];
		// pawns  : 0
		// rooks  : 1
		// knights: 2
		// bishops: 3
		// queens : 4
		// kings  : 5


		Board();
		void clear();
		void print();
		short countNbPieces(U64, bool);			// count the number of piece (first param) of one color (second param)
		U64 possibleDestinations(short, bool, U64);	// possible destination for short from int
		U64 getRookMoves(bool, U64);
		U64 getBishopMoves(bool, U64);
		U64 getPawnPushes(bool, U64);
		U64 getPawnAttacks(bool, U64);
		U64 getQueenMoves(bool, U64);
		U64 getKnightMoves(bool, U64);
		U64 getKingMoves(bool, U64);
		bool isAttacked(U64, bool, bool);
		//	bool testLegality(const Move &);

		float evaluate();
		void playMove(const Move &);
		short playFakeMove(const Move &);
		vector<Move> getPossibleMoves();
		vector<Move> getCheckEvasions(U64);
};

#endif
