/*
 * =====================================================================================
 *
 *       Filename:  Move.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  29/09/2014 12:14:20
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Fabien Teytaud (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef __MOVE__
#define __MOVE__

#include"defines.h"

class Move
{
	private:
		bool putsInCheck;
		bool moveIsEnPassant;
		short capture;
		short piece;
		short promotion;
		U64 from;
		U64 to;

	public:
		// constructor
		Move(short, short, U64, U64, short, bool, bool);

		// getters
		bool getPutsInCheck() const { return putsInCheck; }
		bool getMoveIsEnPassant() const { return moveIsEnPassant; }
		short getCapture() const { return capture; }
		short getPiece() const { return piece; }
		short getPromotion() const { return promotion; }
		U64 getFrom() const { return from; }
		U64 getTo() const { return to; }

		// setters 
		void setPutsInCheck(bool c) { putsInCheck = c; }
};

#endif
