/*
 * =====================================================================================
 *
 *       Filename:  Game.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  29/09/2014 18:37:30
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Fabien Teytaud (), 
 *   Organization:  
 *
 * =====================================================================================
 */


#include"Game.h"

using namespace std;

Game::Game(Board *b)
{
	board = b;
	winner = -1;
	draw = -1;
	players[0] = NULL;
	players[1] = NULL;
}

void Game::playAGame()
{
	Move *m;
	bool toPlay = board->toPlay;

	while (true)
	{
		cout << "New turn ! " << endl;
		m = players[toPlay]->getMove();

		assert(m->getPiece()<6);

		if (m == NULL)
		{
			// if check then toPlay is checkmate
			if (players[toPlay]->board->isAttacked(board->pieces[KING][toPlay], !toPlay, 0))
			{
				winner = !toPlay;
				return;
			}

			// otherwise it is a pat
			draw = 3; // pat
			return;
		}
		else
		{
			board->playMove(*m);
			toPlay ^= 1ULL;
			board->print();


			// Insufficient material
			if ((board->pieces[QUEEN][toPlay] == 0) && (board->pieces[QUEEN][!toPlay] == 0) && (board->pieces[ROOK][toPlay] == 0) && (board->pieces[ROOK][!toPlay] == 0))
			{
				// No Queen nor Rook : draw possible
				if ((board->countNbPieces(BISHOP,toPlay)<2)&&(board->countNbPieces(BISHOP,!toPlay)<2)&&(board->countNbPieces(KNIGHT,toPlay)<2)&&(board->countNbPieces(KNIGHT,!toPlay)<2))
				{
					draw = 2;
					return;
				}
			}

			// TODO 1: 3 repeated positions
		}
	}
}
