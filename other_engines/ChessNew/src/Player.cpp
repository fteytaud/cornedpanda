/*
 * =====================================================================================
 *
 *       Filename:  Player.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  29/09/2014 18:23:23
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Fabien Teytaud (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include"Player.h"

Player::Player(bool c, Board *b, Game *g) {
	color = c;
	opponentColor = c ^ 1ULL;
	board = b;
	game = g;
	game->players[color] = this;
}
