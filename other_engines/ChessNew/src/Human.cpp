/*
 * =====================================================================================
 *
 *       Filename:  Human.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  29/09/2014 18:27:08
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Fabien Teytaud (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include"Human.h"

using namespace std;

Move* Human::getMove()
{
	vector<Move> possibles = board->getPossibleMoves();
	int lp;

	cout << "Possible moves (" << possibles.size() << ")" << endl;
	for (unsigned int i = 0; i < possibles.size(); i++)
	{
		cout<< "(" << i << ") -- " ;

		if (possibles[i].getPiece() == ROOK)
			cout << "R";
		if (possibles[i].getPiece() == KING)
			cout << "K";
		if (possibles[i].getPiece() == KNIGHT)
			cout << "N";
		if (possibles[i].getPiece() == QUEEN)
			cout << "Q";
		if (possibles[i].getPiece() == BISHOP)
			cout << "B";
		if ((possibles[i].getPiece() == PAWN) && possibles[i].getCapture() != NONE)
		{
			lp = (int)log2(possibles[i].getFrom());

			if (FILES[lp] == 1)
				cout << "a";
			if (FILES[lp] == 2)
				cout << "b";
			if (FILES[lp] == 3)
				cout << "c";
			if (FILES[lp] == 4)
				cout << "d";
			if (FILES[lp] == 5)
				cout << "e";
			if (FILES[lp] == 6)
				cout << "f";
			if (FILES[lp] == 7)
				cout << "g";
			if (FILES[lp] == 8)
				cout << "h";
		}

		if (possibles[i].getCapture() != NONE)
			cout << "x";

		lp = (int)log2(possibles[i].getTo());

		if (FILES[lp] == 1)
			cout << "a";
		if (FILES[lp] == 2)
			cout << "b";
		if (FILES[lp] == 3)
			cout << "c";
		if (FILES[lp] == 4)
			cout << "d";
		if (FILES[lp] == 5)
			cout << "e";
		if (FILES[lp] == 6)
			cout << "f";
		if (FILES[lp] == 7)
			cout << "g";
		if (FILES[lp] == 8)
			cout << "h";

		cout << RANKS[lp];

		if (possibles[i].getPutsInCheck())
			cout << "+";

		cout << endl;
	}

	int indice;
	cin >> indice;
	Move *m = new Move(possibles[indice]);
	return m;
}
