/*
 * =====================================================================================
 *
 *       Filename:  Board.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  26/09/2014 18:42:06
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Fabien Teytaud (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include"Board.h"

using namespace std;

/* Creating a board with initial setup (white to play and initial position) */
Board::Board() {

	toPlay = WHITE;
	castlingOO[0] = 1;
	castlingOO[1] = 1;
	castlingOOO[0] = 1;
	castlingOOO[1] = 1;

	pieces[PAWN][WHITE] = 0x000000000000FF00;
	pieces[PAWN][BLACK] = 0x00FF000000000000;

	pieces[ROOK][WHITE] = 0x0000000000000081;
	pieces[ROOK][BLACK] = 0x8100000000000000;

	pieces[KNIGHT][WHITE] = 0x0000000000000042;
	pieces[KNIGHT][BLACK] = 0x4200000000000000;

	pieces[BISHOP][WHITE] = 0x0000000000000024;
	pieces[BISHOP][BLACK] = 0x2400000000000000;

	pieces[QUEEN][WHITE] = 0x0000000000000008;
	pieces[QUEEN][BLACK] = 0x0800000000000000;

	pieces[KING][WHITE] = 0x0000000000000010;
	pieces[KING][BLACK] = 0x1000000000000000;

	// used for draw detection
	nbOfMovesWithoutCaptureOrPawnMove = 0;
}

/* Clearing the Board */
void Board::clear() {
	pieces[PAWN][WHITE] = 0;
	pieces[PAWN][BLACK] = 0;
	pieces[ROOK][WHITE] = 0;
	pieces[ROOK][BLACK] = 0;
	pieces[KNIGHT][WHITE] = 0;
	pieces[KNIGHT][BLACK] = 0;
	pieces[BISHOP][WHITE] = 0;
	pieces[BISHOP][BLACK] = 0;
	pieces[QUEEN][WHITE] = 0;
	pieces[QUEEN][BLACK] = 0;
	pieces[KING][WHITE] = 0;
	pieces[KING][BLACK] = 0;

	nbOfMovesWithoutCaptureOrPawnMove = 0;
}

/* Printing the Board in console */
void Board::print() {
	U64 rank = 0xFF00000000000000;
	U64 column = 0x0101010101010101;
	for (short i = 0; i < 8; i++) {
		for (short j = 0; j < 8; j++) {
			if (pieces[PAWN][WHITE] & (rank & column))
				cout << "P ";
			else if (pieces[PAWN][BLACK] & (rank & column))
				cout << "p ";
			else if (pieces[ROOK][WHITE] & (rank & column))
				cout << "R ";
			else if (pieces[ROOK][BLACK] & (rank & column))
				cout << "r ";
			else if (pieces[KNIGHT][WHITE] & (rank & column))
				cout << "N ";
			else if (pieces[KNIGHT][BLACK] & (rank & column))
				cout << "n ";
			else if (pieces[BISHOP][WHITE] & (rank & column))
				cout << "B ";
			else if (pieces[BISHOP][BLACK] & (rank & column))
				cout << "b ";
			else if (pieces[QUEEN][WHITE] & (rank & column))
				cout << "Q ";
			else if (pieces[QUEEN][BLACK] & (rank & column))
				cout << "q ";
			else if (pieces[KING][WHITE] & (rank & column))
				cout << "K ";
			else if (pieces[KING][BLACK] & (rank & column))
				cout << "k ";
			else
				cout << ". ";

			column <<= 1ULL;
		}
		cout << endl;
		rank >>= 8;
		column = 0x0101010101010101;
	}
	cout << endl;
}

/* Return the number of pieces of type p for player c */
short Board::countNbPieces(U64 p, bool c) {
	static const U64 S[] = { 1, 2, 4, 8, 16, 32 };
	static const U64 B[] = { 0x5555555555555555, 0x3333333333333333,
		0x0F0F0F0F0F0F0F0F, 0x00FF00FF00FF00FF, 0x0000FFFF0000FFFF,
		0x00000000FFFFFFFF };
	U64 count;
	assert(p<NB_TYPE_OF_PIECES);
	U64 v = pieces[p][c];

	count = v - ((v >> 1) & B[0]);
	count = ((count >> S[1]) & B[1]) + (count & B[1]);
	count = ((count >> S[2]) + count) & B[2];
	count = ((count >> S[3]) + count) & B[3];
	count = ((count >> S[4]) + count) & B[4];
	count = ((count >> S[5]) + count) & B[5];

	return count;
}

/* Return possible destinations for pieces of type p from f for player c */
U64 Board::possibleDestinations(short p, bool c, U64 f) {
	assert(p >= 0);
	assert(p < 6);
	if (p == PAWN) {
		return getPawnPushes(c, f) | getPawnAttacks(c, f);
	}
	if (p == ROOK) {
		return getRookMoves(c, f);
	}

	if (p == KNIGHT) {
		return getKnightMoves(c, f);
	}
	if (p == BISHOP) {
		return getBishopMoves(c, f);
	}
	if (p == QUEEN) {
		return getQueenMoves(c, f);
	}
	if (p == KING) {
		return getKingMoves(c, f);
	}
	return 0;
}

U64 Board::getKingMoves(bool c, U64 f) {
	U64 destination;
	U64 occupied[] = { pieces[0][0] | pieces[1][0] | pieces[2][0] | pieces[3][0]
		| pieces[4][0] | pieces[5][0], pieces[0][1] | pieces[1][1]
			| pieces[2][1] | pieces[3][1] | pieces[4][1] | pieces[5][1] };
	U64 occupiedBoard = occupied[0] | occupied[1];
	U64 emptySquares = ~occupiedBoard;

	destination = f;

	destination |= (f & notColumn[7]) << 1;
	destination |= (f & notColumn[0]) >> 1;
	destination |= destination >> 8;
	destination |= destination << 8;
	destination ^= f;
 
	if (castlingOO[toPlay]) {
		if (c) {
			if ((0x4000000000000000 & emptySquares) && (0x2000000000000000 & emptySquares)) {
				cout << "Color 1" << endl;
				U64 isa1 = 0x4000000000000000; // knight square -> destination
				U64 isa2 = 0x1000000000000000; // king not in check
				if (!((isAttacked(isa1, c ^ 1ULL, true))
							|| (isAttacked(isa2, c ^ 1ULL, true)))) {
					destination |= 0x4000000000000000;
				}
			}
		} else {
			if ((0x0000000000000040 & emptySquares) && (0x0000000000000020 & emptySquares)) {
				cout << "Color 0" << endl;
				U64 isa1 = 0x0000000000000040; // knight square -> destination
				U64 isa2 = 0x0000000000000010; // king not in check
				if (!((isAttacked(isa1, c ^ 1ULL, true))
							|| (isAttacked(isa2, c ^ 1ULL, true)))) {
					destination |= 0x0000000000000040;
				}
			}
		}
	}
	if (castlingOOO[toPlay]) {
		//       cout << "Checking castling000" << endl;
		if (c) {
			if ((0x0200000000000000 & emptySquares) && (0x0400000000000000 & emptySquares) && (0x0800000000000000 & emptySquares)) {
				//         cout << "Color 1" << endl;
				U64 isa1 = 0x0400000000000000; // bishop square -> destination
				U64 isa2 = 0x0800000000000000; // queen square just not attacked
				U64 isa3 = 0x1000000000000000; // king not in check
				if (!((isAttacked(isa1, c ^ 1ULL, true))
							|| (isAttacked(isa2, c ^ 1ULL, true))
							|| (isAttacked(isa3, c ^ 1ULL, true))))
					destination |= 0x0400000000000000;
			}
		} else {
			if ((0x0000000000000002 & occupiedBoard) && (0x0000000000000004 & occupiedBoard) && (0x0000000000000008 & occupiedBoard)) {
				//          cout << "Color 0" << endl;
				U64 isa1 = 0x0000000000000004; // bishop square -> destination
				U64 isa2 = 0x0000000000000008; // queen square just not attacked
				U64 isa3 = 0x0000000000000010; // king not in check
				if (!((isAttacked(isa1, c ^ 1ULL, true))
							|| (isAttacked(isa2, c ^ 1ULL, true))
							|| (isAttacked(isa3, c ^ 1ULL, true))))
					destination |= 0x0000000000000004;
			}
		}
	}


	destination &= emptySquares;

	return destination;
}

U64 Board::getRookMoves(bool c, U64 f) {
	U64 destination;
	U64 occupied[] = { pieces[0][0] | pieces[1][0] | pieces[2][0] | pieces[3][0]
		| pieces[4][0] | pieces[5][0], pieces[0][1] | pieces[1][1]
			| pieces[2][1] | pieces[3][1] | pieces[4][1] | pieces[5][1] };
	U64 occupiedBoard = occupied[0] | occupied[1];
	U64 emptySquares = ~occupiedBoard;

	U64 up = f;
	up |= up >> 32;
	up |= up >> 16;
	up |= up >> 8;
	up = up ^ f;
	U64 down = f;
	down |= down << 32;
	down |= down << 16;
	down |= down << 8;
	down = down ^ f;

	U64 allH = f;
	allH |= allH >> 4;
	allH |= allH >> 2;
	allH |= allH >> 1;
	allH = allH ^ f;
	allH &= 0x0101010101010101;
	allH |= allH << 4;
	allH |= allH << 2;
	allH |= allH << 1;

	U64 temp = up & occupiedBoard;
	temp = (temp >> 8) | (temp >> 16) | (temp >> 24) | (temp >> 32)
		| (temp >> 40) | (temp >> 48);
	temp &= up;
	temp ^= up;

	U64 v_dest = down & occupiedBoard;
	v_dest = (v_dest << 8) | (v_dest << 16) | (v_dest << 24) | (v_dest << 32)
		| (v_dest << 40) | (v_dest << 48);
	v_dest &= down;
	v_dest ^= down;
	v_dest |= temp;

	temp = (f << 1) | (f << 2) | (f << 3) | (f << 4) | (f << 5) | (f << 6);
	U64 right = allH & temp;
	U64 rightM = right & occupiedBoard;
	rightM = (rightM << 1) | (rightM << 2) | (rightM << 3) | (rightM << 4)
		| (rightM << 5) | (rightM << 6);
	rightM &= right;
	rightM ^= right;

	temp = (f >> 1) | (f >> 2) | (f >> 3) | (f >> 4) | (f >> 5) | (f >> 6);
	U64 left = allH & temp;
	U64 h_dest = left & occupiedBoard;
	h_dest = (h_dest >> 1) | (h_dest >> 2) | (h_dest >> 3) | (h_dest >> 4)
		| (h_dest >> 5) | (h_dest >> 6);
	h_dest &= left;
	h_dest ^= left;

	h_dest |= rightM;
	destination = v_dest | h_dest;
	destination &= (occupied[c ^ 1ULL] | emptySquares);
	return destination;
}

U64 Board::getBishopMoves(bool c, U64 f) {
	U64 destination;
	U64 occupied[] = { pieces[0][0] | pieces[1][0] | pieces[2][0] | pieces[3][0]
		| pieces[4][0] | pieces[5][0], pieces[0][1] | pieces[1][1]
			| pieces[2][1] | pieces[3][1] | pieces[4][1] | pieces[5][1] };
	U64 occupiedBoard = occupied[0] | occupied[1];

	U64 emptySquares = ~occupiedBoard;

	U64 diag = 0;
	for (int i = 0; i < 13; i++) {
		if (f & diagonals[i]) {
			diag = diagonals[i];
			break;
		}
	}

	U64 antidiag = 0;
	for (int i = 0; i < 13; i++) {
		if (f & antidiagonals[i]) {
			antidiag |= antidiagonals[i];
			break;
		}
	}

	diag ^= f;
	antidiag ^= f;

	U64 temp = (f << 9) | (f << 18) | (f << 27) | (f << 36) | (f << 45)
		| (f << 54);
	U64 upright = antidiag & temp;
	U64 uprightM = upright & occupiedBoard;
	uprightM = (uprightM << 9) | (uprightM << 18) | (uprightM << 27)
		| (uprightM << 36) | (uprightM << 45) | (uprightM << 54);

	uprightM &= upright;
	uprightM ^= upright;

	temp = (f >> 9) | (f >> 18) | (f >> 27) | (f >> 36) | (f >> 45) | (f >> 54);
	U64 downleft = antidiag & temp;
	U64 d1 = downleft & occupiedBoard;
	d1 = (d1 >> 9) | (d1 >> 18) | (d1 >> 27) | (d1 >> 36) | (d1 >> 45)
		| (d1 >> 54);
	d1 &= downleft;
	d1 ^= downleft;

	d1 |= uprightM;

	temp = (f << 7) | (f << 14) | (f << 21) | (f << 28) | (f << 35) | (f << 42);
	U64 upleft = diag & temp;
	U64 upleftM = upleft & occupiedBoard;
	upleftM = (upleftM << 7) | (upleftM << 14) | (upleftM << 21)
		| (upleftM << 28) | (upleftM << 35) | (upleftM << 42);
	upleftM &= upleft;
	upleftM ^= upleft;

	temp = (f >> 7) | (f >> 14) | (f >> 21) | (f >> 28) | (f >> 35) | (f >> 42);
	U64 downright = diag & temp;
	U64 d2 = downright & occupiedBoard;
	d2 = (d2 >> 7) | (d2 >> 14) | (d2 >> 21) | (d2 >> 28) | (d2 >> 35)
		| (d2 >> 42);
	d2 &= downright;
	d2 ^= downright;

	d2 |= upleftM;

	destination = d1 | d2;

	destination &= (occupied[c ^ 1ULL] | emptySquares);
	return destination;
}

U64 Board::getPawnPushes(bool c, U64 f) {
	U64 destination;
	U64 occupied[] = { pieces[0][0] | pieces[1][0] | pieces[2][0] | pieces[3][0]
		| pieces[4][0] | pieces[5][0], pieces[0][1] | pieces[1][1]
			| pieces[2][1] | pieces[3][1] | pieces[4][1] | pieces[5][1] };
	U64 occupiedBoard = occupied[0] | occupied[1];
	U64 emptySquares = ~occupiedBoard;

	if (c) // black
	{
		destination = (f >> 8) & emptySquares; // one square above if emptySquare
		destination |= ((destination >> 8) & emptyRanks[3] & emptySquares); // two squares if one square was possible
		return destination;
	} else {
		destination = (f << 8) & emptySquares; // one square above if emptySquare
		destination |= ((destination << 8) & emptyRanks[4] & emptySquares); // two squares if one square was possible
		return destination;
	}
}

U64 Board::getPawnAttacks(bool c, U64 f) {
	U64 destination;
	U64 occupied[] = { pieces[0][0] | pieces[1][0] | pieces[2][0] | pieces[3][0]
		| pieces[4][0] | pieces[5][0], pieces[0][1] | pieces[1][1]
			| pieces[2][1] | pieces[3][1] | pieces[4][1] | pieces[5][1] };
	if (c) // black
	{
		destination = (((f >> 7) & occupied[0] & notColumn[0])
				| ((f >> 9) & occupied[0] & notColumn[7]));
		return destination;
	} else {
		destination = (((f << 9) & occupied[1] & notColumn[0])
				| ((f << 7) & occupied[1] & notColumn[7]));
		return destination;
	}

}

U64 Board::getQueenMoves(bool c, U64 f) {
	return getRookMoves(c, f) | getBishopMoves(c, f);
}

U64 Board::getKnightMoves(bool c, U64 f) {
	U64 destination;
	U64 occupied[] = { pieces[0][0] | pieces[1][0] | pieces[2][0] | pieces[3][0]
		| pieces[4][0] | pieces[5][0], pieces[0][1] | pieces[1][1]
			| pieces[2][1] | pieces[3][1] | pieces[4][1] | pieces[5][1] };
	U64 occupiedBoard = occupied[0] | occupied[1];
	U64 emptySquares = ~occupiedBoard;
	destination = (f & notColumn[7]) << 17;
	destination |= (f & notColumn[6] & notColumn[7]) << 10;
	destination |= (f & notColumn[6] & notColumn[7]) >> 6;
	destination |= (f & notColumn[7]) >> 15;
	destination |= (f & notColumn[0]) << 15;
	destination |= (f & notColumn[0] & notColumn[1]) << 6;
	destination |= (f & notColumn[0] & notColumn[1]) >> 10;
	destination |= (f & notColumn[0]) >> 17;
	destination &= (occupied[c ^ 1ULL] | emptySquares);
	return destination;
}

bool Board::isAttacked(U64 square, bool bySide, bool forCastling) { // forCastling should be true if checking if king is attacked ?
	U64 destinations;

	destinations = getPawnAttacks(bySide, pieces[PAWN][bySide]);
	if (destinations & square)
		return true;

	destinations = getRookMoves(bySide, pieces[ROOK][bySide]);
	if (destinations & square)
		return true;

	destinations = getKnightMoves(bySide, pieces[KNIGHT][bySide]);
	if (destinations & square)
		return true;

	destinations = getBishopMoves(bySide, pieces[BISHOP][bySide]);
	if (destinations & square)
		return true;

	destinations = getQueenMoves(bySide, pieces[QUEEN][bySide]);
	if (destinations & square)
		return true;

	if (!forCastling) {
		destinations = getKingMoves(bySide, pieces[KING][bySide]);
		if (destinations & square)
			return true;
	}

	return false;
}

/* Playing a move for toPlay */
void Board::playMove(const Move &m) {
	bool c, ep;
	short p, cap, prom;
	U64 f, t;

	ep = m.getMoveIsEnPassant();
	c = m.getPutsInCheck();
	p = m.getPiece();
	f = m.getFrom();
	t = m.getTo();
	cap = m.getCapture();
	prom = m.getPromotion();

	assert(p >= 0);
	assert(p < 6);
	//cout << " piece : " << p << " from " << f << " to " << t << " puts in check ? " << c << " en passant ? " << ep << endl;

	// update of number of moves without a capture or pawn move (draw detection)
	if (p == PAWN || cap)
		nbOfMovesWithoutCaptureOrPawnMove = 0;
	else
		nbOfMovesWithoutCaptureOrPawnMove++;

	if (c)
		playerInCheck[!toPlay] = true;

	if (p == KING && castlingOO[toPlay]) {
		if (toPlay) {
			// if black king castle king side
			if ((f == 0x1000000000000000) && (t == 0x4000000000000000)) {
				cout<<"BLACK castle king side"<<endl;
				castlingOO[toPlay] = false;
				castlingOOO[toPlay] = false;
				pieces[ROOK][toPlay] &= 0x8000000000000000 ^ FULLBOARD;
				pieces[ROOK][toPlay] |= 0x2000000000000000;
				pieces[KING][toPlay] &= 0x1000000000000000 ^ FULLBOARD;
				pieces[KING][toPlay] |= 0x4000000000000000;
				// we have played
				castlingOOO[toPlay] = 0;
				castlingOO[toPlay] = 0;
				toPlay = !toPlay;
				return;
			}
		} else {
			// if white king castle king side
			if ((f == 0x0000000000000010) && (t == 0x0000000000000040)) {
				cout<<"WHITE castle king side"<<endl;
				castlingOO[toPlay] = 0;
				castlingOOO[toPlay] = 0;
				intToBoard(pieces[ROOK][toPlay]);
				pieces[ROOK][toPlay] &= 0x0000000000000080 ^ FULLBOARD;
				pieces[ROOK][toPlay] |= 0x0000000000000020;
				pieces[KING][toPlay] &= 0x0000000000000010 ^ FULLBOARD;
				pieces[KING][toPlay] |= 0x0000000000000040;
				// we have played
				castlingOOO[toPlay] = 0;
				castlingOO[toPlay] = 0;
				toPlay = !toPlay;
				return;
			}
		}
	}
	if (p == KING && castlingOOO[toPlay]) {
		if (toPlay) {
			// if back king castle queen side
			if ((f == 0x1000000000000000) && (t == 0x0400000000000000)) {
				cout<<"BLACK castle queen side"<<endl;
				castlingOO[toPlay] = 0;
				castlingOOO[toPlay] = 0;
				pieces[ROOK][toPlay] &= 0x0100000000000000 ^ FULLBOARD;
				pieces[ROOK][toPlay] |= 0x1000000000000000;
				pieces[KING][toPlay] &= 0x1000000000000000 ^ FULLBOARD;
				pieces[KING][toPlay] |= 0x3000000000000000;
				// we have played
				castlingOOO[toPlay] = 0;
				castlingOO[toPlay] = 0;
				toPlay = !toPlay;
				return;
			}
		} else {
			// if white king castle queen side
			if ((f == 0x0000000000000010) && (t == 0x0000000000000004)) {
				cout<<"WHITE castle king side"<<endl;
				castlingOO[toPlay] = 0;
				castlingOOO[toPlay] = 0;
				pieces[ROOK][toPlay] &= 0x0000000000000001 ^ FULLBOARD;
				pieces[ROOK][toPlay] |= 0x0000000000000010;
				pieces[KING][toPlay] &= 0x0000000000000010 ^ FULLBOARD;
				pieces[KING][toPlay] |= 0x0000000000000030;
				// we have played
				castlingOOO[toPlay] = 0;
				castlingOO[toPlay] = 0;
				toPlay = !toPlay;
				return;
			}
		}
	}

	// King or Rook move : castling no more possible
	if (p == KING) {
		castlingOO[toPlay] = 0;
		castlingOOO[toPlay] = 0;
	} else {
		if (p == ROOK) {
			if (toPlay) // black move
			{
				// Queen rook move
				if (f == 0x0100000000000000)
					castlingOOO[BLACK] = 0;
				else if (f == 0x8000000000000000)
					castlingOO[BLACK] = 0;
			}
		} else {
			// Queen rook move
			if (f == 0x0000000000000001)
				castlingOOO[WHITE] = 0;
			else if (f == 0x0000000000000080)
				castlingOO[WHITE] = 0;
		}
	}

	// capture the opponent's rook : no more castle
	if (cap && toPlay) { // black move
		if (t == 0x0000000000000001)
			castlingOOO[WHITE] = 0;
		else if (t == 0x0000000000000080)
			castlingOO[WHITE] = 0;
	} else {
		if (t == 0x0100000000000000)
			castlingOOO[BLACK] = 0;
		else if (t == 0x8000000000000000)
			castlingOO[BLACK] = 0;
	}

	U64 nf = ~f;
	U64 nt = ~t;
	bool ntoPlay = !toPlay;

	// if capture
	if (cap != NONE) {
		pieces[cap][ntoPlay] &= nt;
	}

	// if there is no promotion
	if (prom == NONE) {
		pieces[p][toPlay] &= nf;
		pieces[p][toPlay] |= t;
	}
	// if there is a promotion
	else {
		pieces[p][toPlay] &= nf;
		pieces[prom][toPlay] |= t;
	}
	toPlay = !toPlay;
}

/* Playing a move for toPlay on a copy board and return if the move was legal (if the king is in check after playing */
short Board::playFakeMove(const Move &m) {
	Board b(*this);
	b.playMove(m);
	if (b.isAttacked(pieces[KING][toPlay],!toPlay, true))
		return -1; // move not legal

	if (b.isAttacked(pieces[KING][!toPlay],toPlay, true))
		return 1; // putting opponent in check

	return 0; // move ok
}

/* Generating all possible moves for toPlay */
vector<Move> Board::getPossibleMoves() {

	if (playerInCheck[toPlay]) {
		cout << "In Check " << endl;
		return getCheckEvasions(pieces[KING][toPlay]);
	}

	short nb;
	U64 current, LSB, currentPossible, tmpPoss;
	bool ntoPlay = !toPlay;

	vector<Move> possibles;
	for (int i = 0; i < NB_TYPE_OF_PIECES; i++) {
		current = pieces[i][toPlay];
		nb = countNbPieces(i, toPlay);
		if (nb)
			do {
				LSB = (current & -current);
				currentPossible = possibleDestinations(i, toPlay, LSB);
				if (currentPossible) {
					do {
						tmpPoss = currentPossible & -currentPossible;

						short cap = NONE;
						short prom = NONE;

						// update of capture
						for (int ci = 0; ci < NB_TYPE_OF_PIECES; ci++)
							if (tmpPoss & pieces[ci][ntoPlay]) {
								cap = ci;
								break;
							}

						// check promotion
						if (toPlay == WHITE && (i == PAWN)
								&& (tmpPoss & 0x00000000000000FF))
							prom = QUEEN;
						else if (toPlay == BLACK && (i == PAWN)
								&& (tmpPoss & 0xFF00000000000000))
							prom = QUEEN;

						Move m(i, prom, LSB, tmpPoss, cap, false, false);

						// if legal move then add to possible moves
						short rr = playFakeMove(m);
						if (rr != -1) {
							if (rr == 1)
								m.setPutsInCheck(true);
							possibles.push_back(m);
						}
					} while (currentPossible &= (currentPossible - 1));
				}
			} while (current &= (current - 1));
	}
	return possibles;
}

/* getting all possible ways to escape to a check */
vector<Move> Board::getCheckEvasions(U64 kingSquare) {
	// TODO mettre a jour playerInCheck
	cout << "King under fire ! Defense, defense, defense !" << endl;

	vector<Move> possibles;
	possibles.clear();
	U64 destinations, destination1 = 0ULL, destination2 = 0ULL;
	U64 current, LSB, currentPossible, tmpPoss;

	bool ntoPlay = !toPlay;

	destinations = getPawnAttacks(ntoPlay, pieces[PAWN][ntoPlay]);
	if (destinations & kingSquare)
		destination1 = destinations;

	destinations = getRookMoves(ntoPlay, pieces[ROOK][ntoPlay]);
	if (destinations & kingSquare) {
		if (destination1)
			destination2 = destinations;
		else
			destination1 = destinations;
	}

	destinations = getKnightMoves(ntoPlay, pieces[KNIGHT][ntoPlay]);
	if (destinations & kingSquare) {
		if (destination1)
			destination2 = destinations;
		else
			destination1 = destinations;
	}

	destinations = getBishopMoves(ntoPlay, pieces[BISHOP][ntoPlay]);
	if (destinations & kingSquare) {
		if (destination1)
			destination2 = destinations;
		else
			destination1 = destinations;
	}

	destinations = getQueenMoves(ntoPlay, pieces[QUEEN][ntoPlay]);
	if (destinations & kingSquare) {
		if (destination1)
			destination2 = destinations;
		else
			destination1 = destinations;
	}

	if (destination1 && destination2) {
		cout << "Double check ! Need to run !" << endl;
		destinations = getKingMoves(ntoPlay, pieces[KING][ntoPlay]);

		// we are checkmated
		if (destinations == 0)
			return possibles;

		// otherwise we need to run
		do {
			tmpPoss = destinations & -destinations;

			// update of capture
			short cap = NONE;
			short prom = NONE;
			for (int ci = 0; ci < NB_TYPE_OF_PIECES; ci++)
				if (tmpPoss & pieces[ci][ntoPlay]) {
					intToBoard(tmpPoss);
					cap = ci;
					break;
				}


			Move m(KING, prom, kingSquare, tmpPoss, cap, false, false); // we can't avoid double check AND give check
			possibles.push_back(m);
		} while (currentPossible &= (currentPossible - 1));

		// end of double check
		return possibles;
	}

	for (int i = 0; i < NB_TYPE_OF_PIECES; i++) {
		current = pieces[i][toPlay];
		if (current)
			do {
				LSB = (current & -current);
				currentPossible = possibleDestinations(i, toPlay, LSB);
				if (currentPossible) {
					do {
						tmpPoss = currentPossible & -currentPossible;

						// update of capture
						short cap = NONE;
						short prom = NONE;
						for (int ci = 0; ci < NB_TYPE_OF_PIECES; ci++)
							if (tmpPoss & pieces[ci][ntoPlay]) {
								cap = ci;
								break;
							}

						// check promotion
						if (toPlay == WHITE && (i == PAWN)
								&& (tmpPoss & 0x00000000000000FF))
							prom = QUEEN;
						else if (toPlay == BLACK && (i == PAWN)
								&& (tmpPoss & 0xFF00000000000000))
							prom = QUEEN;

						Move m(i, prom, LSB, tmpPoss, cap, false, false);
						short rr = playFakeMove(m);
						if (rr != -1) {
							if (rr == 1)
								m.setPutsInCheck(true);
							possibles.push_back(m);
						}
					} while (currentPossible &= (currentPossible - 1));
				}
			} while (current &= (current - 1));
	}
	return possibles;
}

float Board::evaluate() {
	float score = 0.;
	bool ntoPlay = !toPlay;

	score +=
		COEF_QUEEN
		* (countNbPieces(QUEEN, toPlay)
				- countNbPieces(QUEEN,
					ntoPlay));
	score +=
		COEF_ROOK
		* (countNbPieces(ROOK, toPlay)
				- countNbPieces(ROOK,
					ntoPlay));
	score += COEF_KNIGHT
		* (countNbPieces(KNIGHT, toPlay)
				- countNbPieces(KNIGHT,
					ntoPlay));
	score += COEF_BISHOP
		* (countNbPieces(BISHOP, toPlay)
				- countNbPieces(BISHOP,
					ntoPlay));
	score +=
		COEF_PAWN
		* (countNbPieces(PAWN, toPlay)
				- countNbPieces(PAWN,
					ntoPlay));

	return score;
}
