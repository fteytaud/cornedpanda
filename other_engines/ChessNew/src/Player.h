/*
 * =====================================================================================
 *
 *       Filename:  Player.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  29/09/2014 17:14:00
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Fabien Teytaud (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef __PLAYER__
#define __PLAYER__

#include"Board.h"
#include"Game.h"

class Board;
class Game;

class Player
{
	public:
		bool color;
		bool opponentColor;

		Board *board;
		Game *game;

		Player(bool, Board *, Game *);
		virtual ~Player() { }

		virtual Move* getMove() { return NULL; }
};

#endif
