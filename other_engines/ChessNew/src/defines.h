/*
 * =====================================================================================
 *
 *       Filename:  defines.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  26/09/2014 18:01:29
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Fabien Teytaud (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef __DEFINES__
#define __DEFINES__

#include<iostream>
#include<assert.h>
#include<vector>
#include<math.h>

typedef unsigned long long U64;
using namespace std;

#define WHITE 0
#define BLACK 1

#define PAWN 0
#define ROOK 1
#define KNIGHT 2
#define BISHOP 3
#define QUEEN 4
#define KING 5
#define NONE 6

#define NB_TYPE_OF_PIECES 6

#define FULLBOARD 0xFFFFFFFFFFFFFFFF
#define EMPTYBOARD 0x0000000000000000;

#define COEF_KNIGHT 3.0
#define COEF_BISHOP 3.0
#define COEF_ROOK 5.0
#define COEF_PAWN 1.0
#define COEF_QUEEN 9.0

static const U64 emptyRanks[] = { 0xFF00000000000000, 0x00FF000000000000,
	0x0000FF0000000000, 0x000000FF00000000, 0x00000000FF000000,
	0x0000000000FF0000, 0x000000000000FF00, 0x00000000000000FF };

static const U64 emptyColumns[] = { 0x0101010101010101, 0x0202020202020202,
	0x0404040404040404, 0x0808080808080808, 0x1010101010101010,
	0x2020202020202020, 0x4040404040404040, 0x8080808080808080 };

static const U64 diagonals[] = { 0x4080000000000000, 0x2040800000000000,
	0x1020408000000000, 0x0810204080000000, 0x0408102040800000,
	0x0204081020408000, 0x0102040810204080, 0x0001020408102040,
	0x0000010204081020, 0x0000000102040810, 0x0000000001020408,
	0x0000000000010204, 0x0000000000000102 };

static const U64 antidiagonals[] = { 0x0201000000000000, 0x0402010000000000,
	0x0804020100000000, 0x1008040201000000, 0x2010080402010000,
	0x4020100804020100, 0x8040201008040201, 0x0080402010080402,
	0x0000804020100804, 0x0000008040201008, 0x0000000080402010,
	0x0000000000804020, 0x0000000000008040 };

static const U64 notRank[] = { ~emptyRanks[0], ~emptyRanks[1], ~emptyRanks[2],
	~emptyRanks[3], ~emptyRanks[4], ~emptyRanks[5], ~emptyRanks[6],
	~emptyRanks[7] };

static const U64 notColumn[] = { ~emptyColumns[0], ~emptyColumns[1],
	~emptyColumns[2], ~emptyColumns[3], ~emptyColumns[4], ~emptyColumns[5],
	~emptyColumns[6], ~emptyColumns[7] };

static void intToBoard(U64 e) {
	U64 rank = 0xFF00000000000000;
	U64 column = 0x0101010101010101;
	for (short i = 0; i < 8; i++) {
		for (short j = 0; j < 8; j++) {
			if (e & (rank & column))
				cout << "1 ";
			else
				cout << "0 ";

			column <<= 1ULL;
		}
		cout << endl;
		rank >>= 8;
		column = 0x0101010101010101;
	}
	cout << endl;
}

static const int FILES[64] = { 1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8,
	1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8,
	1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8 };

static const int RANKS[64] = { 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2,
	3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5,
	6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8 };

#endif
