#include"Player.h"
//#include"Computer.h"
#include"Human.h"
#include"Board.h"
#include"Game.h"

int main()
{
	Board *b = new Board();
	Game *g = new Game(b);
	Human *p1 = new Human(WHITE,b,g);
	Human *p2 = new Human(BLACK,b,g);
	//    Computer *p1 = new Computer(WHITE,b,g);
	//Computer *p2 = new Computer(BLACK,b,g);
	b->print();
	g->playAGame();
	b->print();
}
