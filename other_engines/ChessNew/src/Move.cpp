/*
 * =====================================================================================
 *
 *       Filename:  Move.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  29/09/2014 12:15:46
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Fabien Teytaud (), 
 *   Organization:  
 *
 * =====================================================================================
 */


#include "Move.h"

Move::Move(short p, short pro, U64 f, U64 t, short c, bool isCheck, bool enP)
{
	moveIsEnPassant = enP;
	putsInCheck = isCheck;
	capture = c;
	piece = p;
	promotion = pro;
	from = f;
	to = t;
}
