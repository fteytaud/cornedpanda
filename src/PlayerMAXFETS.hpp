// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef _PLAYERMAXFETS_HPP_
#define _PLAYERMAXFETS_HPP_

#include "Player.hpp"

namespace cornedpanda
{
    // MCTS with AMAF and board evaluation as playout
    class PlayerMAXFETS : public Player
    {
        struct Node 
        {
            BoardEval _board;
            Move _move;
            std::vector<Move> _possibleMoves;
            std::vector<Node*> _childNodes;
            Node * _ptrParentNode;
            Node * _ptrBestChildNodeEval;
            Node * _ptrBestChildNodeSims;
            int _nbSims;
            int _minmaxEval; // negamax score for the root player
            int _depth;
            int _maxDepth;

            // do not use copy constructor (inefficient memory management)
            Node(const Node &); 
            // construct a root node
            Node(const BoardEval & board);
            // construct a child node
            Node(Node * ptrParentNode, const Move & move);
        };

        std::vector<Node> _nodes;

        public:
        PlayerMAXFETS();

        // virtual methods
        // _statsCurrMoveNumber is not updated because it would be expensive and not very relevant
        void initBestMove();
        void iterBestMove();
        std::string getBestMove();
        std::string formatMovesStats() const;
        void printStatsBestMove();

        private:
        int _iSims;
        int _nSims;
        Node * _ptrRootNode;

        // the 4 MCTS steps
        Node * computeSelection(Node * ptrNode);
        void computeExpansionAmaf(Node * ptrSelectedNode, int & nbNewSims, int & newMaxDepth);
        void computeSimulationAmaf(Node * ptrSelectedNode);
        void computeBackpropagationAmaf(Node * ptrSelectedNode, int nbNewSims, int newMaxDepth); 
        void computeBackpropagationEnded(Node * ptrSelectedNode); 

        // compute one MCTS iteration, or more (AMAF)
        void computeOneMctsIteration(); 
    };
}

#endif

