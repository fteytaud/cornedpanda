// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef _PLAYERAB_HPP_
#define _PLAYERAB_HPP_

#include "Player.hpp"
#include "BoardEval.hpp"

namespace cornedpanda
{

    // alpha-beta
    class PlayerAB : public Player
    {
            std::vector<Move> _moves;
            std::vector<int> _scores;
        public:
            PlayerAB();
            void initBestMove();
            void iterBestMove();
            void printStatsBestMove();
        private:
            std::string formatMovesStats() const;
            void alphabeta(int maxDepth);
            int alphabetaRec(const BoardEval & refBoard, int currentDepth, int maxDepth, int alpha, int beta);
            virtual int quiesce(const BoardEval & refBoard, int currentDepth, int alpha, int beta);
    };

    // alpha-beta + quiescence search
    class PlayerABQ : public PlayerAB
    {
            virtual int quiesce(const BoardEval & refBoard, int currentDepth, int alpha, int beta);
    };

    // alpha-beta + quiescence search + delta-pruning
    class PlayerABQD : public PlayerAB
    {
            virtual int quiesce(const BoardEval & refBoard, int currentDepth, int alpha, int beta);
    };

    // alpha-beta + quiescence search with depth max
    class PlayerABQLIMITED : public PlayerAB
    {
            virtual int quiesce(const BoardEval & refBoard, int currentDepth, int alpha, int beta);
    };

    // alpha-beta + quiescence search with depth max + delta-pruning
    class PlayerABQDLIMITED : public PlayerAB
    {
            virtual int quiesce(const BoardEval & refBoard, int currentDepth, int alpha, int beta);
    };

}

#endif

