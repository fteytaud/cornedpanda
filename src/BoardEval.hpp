// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef _BOARDEVAL_HPP_
#define _BOARDEVAL_HPP_

#include "Board.hpp"

namespace cornedpanda
{

    class BoardEval : public Board
    {
        public:
            static const int MIN_EVAL = -1e6;
            static const int MAX_EVAL =  1e6;
            constexpr static const float MIN_EVAL_F = -1e6;
            constexpr static const float MAX_EVAL_F =  1e6;

        public:

            BoardEval();
            // TODO overload setParameter; reset board coef...

            bool isEndGame() const;
            int getColorCoef() const;
            int estimateMaxGain(const Move & m) const;
            int evaluate() const;

        private:

            int evaluateWhiteOpening(int whiteMaterial) const;
            int evaluateBlackOpening(int BlackMaterial) const;
            int evaluateWhiteMiddleGame(int whiteMaterial) const;
            int evaluateBlackMiddleGame(int blackMaterial) const;
            int evaluateEndGame(int whiteMaterial, int blackMaterial) const;
            int evaluateKingPosition() const;
            int evaluatePawnsRanks() const;

            int evaluateWhiteCenter() const;
            int evaluateWhiteDevelopment() const;
            int evaluateWhiteKingSafety() const;
            int evaluateWhitePawnWeaknesses() const;
            int evaluateWhiteMobility() const;
            int evaluateBlackCenter() const;
            int evaluateBlackDevelopment() const;
            int evaluateBlackKingSafety() const;
            int evaluateBlackPawnWeaknesses() const;
            int evaluateBlackMobility() const;
            int evaluateWhiteLightSquares() const;
            int evaluateWhiteDarkSquares() const;
            int evaluateBlackLightSquares() const;
            int evaluateBlackDarkSquares() const;
            int evaluateWhiteBishops() const;
            int evaluateBlackBishops() const;
            int evaluateWhiteRooks() const;
            int evaluateBlackRooks() const;

            // Shannon evaluation function
            int evaluateShannon(int whiteMaterial, int blackMaterial) const;

    };

}

#endif

