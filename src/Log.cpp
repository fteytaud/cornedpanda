
#include "Log.hpp"

#include <csignal>
#include <ctime>

std::ofstream cornedpanda::logStream;

void cornedpanda::sigHandler(int)
{
    std::time_t currentTime = std::time(0);
    logStream << "sigHandler -- " << std::asctime(std::localtime(&currentTime)) << std::endl;
    logStream.close();
}

void cornedpanda::exitHandler()
{
    sigHandler(0);
}

void cornedpanda::intHandler(int i)
{
    sigHandler(i);
    abort();
}

void cornedpanda::initLogStream(const char * filename)
{
    logStream.open(filename);
    signal(SIGINT, intHandler);
    signal(SIGKILL, sigHandler);
    signal(SIGABRT, sigHandler);
    std::atexit(exitHandler);
    std::time_t currentTime = std::time(0);
    logStream << std::endl << "initLogStream -- " << std::asctime(std::localtime(&currentTime)) << std::endl;
}

