// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "Board.hpp"
#include "Log.hpp"

#include "magicmoves/magicmoves.hpp"

#include <array>
#include <cassert>
#include <cmath>
#include <cstring>
#include <sstream>

// Little-Endian Rank-File Mapping
//
// https://chessprogramming.wikispaces.com/Square+Mapping+Considerations
//
// uint64_t:  63 62 ... 1  0  ->  h8 g8 ... b1 a1
//
//      A    B    C    D    E    F    G    H
//    +----+----+----+----+----+----+----+----+
//  8 | 56 | 57 | 58 | 59 | 60 | 61 | 62 | 63 |  8th rank
//    +----+----+----+----+----+----+----+----+
//  7 | 48 | 49 | 50 | 51 | 52 | 53 | 54 | 55 |  7th rank
//    +----+----+----+----+----+----+----+----+
//  6 | 40 | 41 | 42 | 43 | 44 | 45 | 46 | 47 |  6th rank
//    +----+----+----+----+----+----+----+----+
//  5 | 32 | 33 | 34 | 35 | 36 | 37 | 38 | 39 |  5th rank
//    +----+----+----+----+----+----+----+----+
//  4 | 24 | 25 | 26 | 27 | 28 | 29 | 30 | 31 |  4th rank
//    +----+----+----+----+----+----+----+----+
//  3 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 |  3rd rank
//    +----+----+----+----+----+----+----+----+
//  2 |  8 |  9 | 10 | 11 | 12 | 13 | 14 | 15 |  2nd rank
//    +----+----+----+----+----+----+----+----+
//  1 |  0 |  1 |  2 |  3 |  4 |  5 |  6 |  7 |  1st rank
//    +----+----+----+----+----+----+----+----+
//      A    B    C    D    E    F    G    H - file(s)

namespace cornedpanda
{

    static const uint64_t FILE_A_COMP = ~FILE_A;
    static const uint64_t FILE_B_COMP = ~FILE_B;
    static const uint64_t FILE_G_COMP = ~FILE_G;
    static const uint64_t FILE_H_COMP = ~FILE_H;

    static const uint64_t CASTLING_K_from       = 0x0000000000000090;
    static const uint64_t CASTLING_Q_from       = 0x0000000000000011;
    static const uint64_t CASTLING_k_from       = 0x9000000000000000;
    static const uint64_t CASTLING_q_from       = 0x1100000000000000;
    static const uint64_t CASTLING_all_from     = 0x9100000000000091;

    static const int indexUint64ToInt[64] = 
    {
        63,  0, 58,  1, 59, 47, 53,  2,
        60, 39, 48, 27, 54, 33, 42,  3,
        61, 51, 37, 40, 49, 18, 28, 20,
        55, 30, 34, 11, 43, 14, 22,  4,
        62, 57, 46, 52, 38, 26, 32, 41,
        50, 36, 17, 19, 29, 10, 13, 21,
        56, 45, 25, 31, 35, 16,  9, 12,
        44, 24, 15,  8, 23,  7,  6,  5  
    };

    static const uint64_t debruijn64 = 0x07EDD5E59A4E28C2ULL;

    static const std::array<int,13> initialNbMaterial = {{ 8, 2, 2, 2, 1, 1, 8, 2, 2, 2, 1, 1, 32 }};
    static const std::array<int,13> zeroNbMaterial = {{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }};
}

bool cornedpanda::Board::isInit()
{
    return _isInit;
}

bool cornedpanda::Board::_isInit = false;

bool cornedpanda::Move::operator==(const Move &m1) const
{
    return m1._from == _from && m1._to == _to && m1._piece == _piece; 
}

void cornedpanda::Board::init()
{
    if (not _isInit)
    {
        initmagicmoves();
        _isInit = true;
    }
}

cornedpanda::Board::Board() 
{
    _squares[PIECE_p] = 0x00FF000000000000;
    _squares[PIECE_r] = 0x8100000000000000;
    _squares[PIECE_n] = 0x4200000000000000;
    _squares[PIECE_b] = 0x2400000000000000;
    _squares[PIECE_q] = 0x0800000000000000;
    _squares[PIECE_k] = 0x1000000000000000;

    _squares[PIECE_P] = 0x000000000000FF00;
    _squares[PIECE_R] = 0x0000000000000081;
    _squares[PIECE_N] = 0x0000000000000042;
    _squares[PIECE_B] = 0x0000000000000024;
    _squares[PIECE_Q] = 0x0000000000000008;
    _squares[PIECE_K] = 0x0000000000000010;

    _isWhiteToMove = true;
    _castlings = CASTLING_all_from ;
    _enPassantTarget = 0;
    _halfmoveClock = 0;
    _fullmoveCounter = 1;
    _isInCheck = false;

    _possibleMoves.reserve(40);

    _nbMaterial = initialNbMaterial;

    updateSquares();
    updateMoves();

    _threefoldRepetition = false;
    _repetitions.clear();
    updateThreefoldRepetition();
}

bool cornedpanda::Board::isWhiteToMove() const
{
    return _isWhiteToMove;
}

// https://chessprogramming.wikispaces.com/Forsyth-Edwards+Notation
const char * cornedpanda::Board::setFen(const char * fen)
{
    const char * p = fen;

    // function to read and ignore begining spaces in p
    auto funReadSpaces = [&p]() 
    { 
        while (*p and (*p==' ' or *p=='\t')) p++; 
        return (p); 
    };

    // function to read an integer in p
    auto funReadInt = [&p](int & n)
    {
        n = 0;
        while (*p) 
        {
            int x = *p - '0';
            p++;
            if (x<0 or x>9) break;
            n *= 10;
            n += x;
        }
    };

    // read piece placement
    if (not funReadSpaces()) return nullptr;
    // clear previous board
    std::memset((void*)_squares, 0, sizeof(_squares));
    // for rank 8 to rank 1
    for (uint64_t rank=cornedpanda::RANK_8; rank!=0ULL; rank>>=8, p++)
    {
        // for file A to file H
        for (uint64_t file=FILE_A; file!=FILE_A<<8; file<<=1, p++)
        {
            // read a caracter in p
            char c = *p;
            if (c <= 0) return nullptr;

            int x = c - '0';
            if (x>=1 and x<=8) 
            {
                // if the caracter is a number (of empty squares),
                // ignore the corresponding squares (board already cleared)
                file <<= x-1;
            } 
            else 
            {
                // if the caracter is a letter, 
                // add the corresponding piece in the board
                uint64_t idx = rank & file;
                switch (c) 
                {
                    case 'P': _squares[PIECE_P] |= idx; break;
                    case 'R': _squares[PIECE_R] |= idx; break;
                    case 'N': _squares[PIECE_N] |= idx; break;
                    case 'B': _squares[PIECE_B] |= idx; break;
                    case 'Q': _squares[PIECE_Q] |= idx; break;
                    case 'K': _squares[PIECE_K] |= idx; break;
                    case 'p': _squares[PIECE_p] |= idx; break;
                    case 'r': _squares[PIECE_r] |= idx; break;
                    case 'n': _squares[PIECE_n] |= idx; break;
                    case 'b': _squares[PIECE_b] |= idx; break;
                    case 'q': _squares[PIECE_q] |= idx; break;
                    case 'k': _squares[PIECE_k] |= idx; break;
                    default: return nullptr;
                }
            }
        }
    }

    // read side to move
    if (not funReadSpaces()) return nullptr;
    _isWhiteToMove = *p == 'w';
    p++;

    // read castling ability
    const uint64_t CASTLING_K_rook_from  = 0x0000000000000080;
    const uint64_t CASTLING_Q_rook_from  = 0x0000000000000001;
    const uint64_t CASTLING_k_rook_from  = 0x8000000000000000;
    const uint64_t CASTLING_q_rook_from  = 0x0100000000000000;
    const uint64_t CASTLING_KQ_king_from = 0x0000000000000010;
    const uint64_t CASTLING_kq_king_from = 0x1000000000000000;
    if (not funReadSpaces()) return nullptr;
    _castlings = 0ULL;
    while (true)
    {
        char c = *p;
        p++;
        if (c=='K') _castlings |= CASTLING_KQ_king_from | CASTLING_K_rook_from;
        else if (c=='Q') _castlings |= CASTLING_KQ_king_from | CASTLING_Q_rook_from;
        else if (c=='k') _castlings |= CASTLING_kq_king_from | CASTLING_k_rook_from;
        else if (c=='q') _castlings |= CASTLING_kq_king_from | CASTLING_q_rook_from;
        else break;  // end of castlings or '-'
    }

    // read en passant target square
    if (not funReadSpaces()) return nullptr;
    p = fenStringToIndex(p, _enPassantTarget);

    // read halfmove clock
    if (not funReadSpaces()) return nullptr;
    funReadInt(_halfmoveClock);

    // read fullmove counter
    if (not funReadSpaces()) return nullptr;
    funReadInt(_fullmoveCounter);

    updateSquares();
    updateInCheck();
    updateMoves();

    // update _threefoldRepetition
    _threefoldRepetition = false;
    _repetitions.clear();
    updateThreefoldRepetition();

    // TODO julien tu dois faire un test unitaire sinon je te fume
    _nbMaterial = zeroNbMaterial;
    int sum = 0;
    for (int i=PIECE_P; i < PIECE_0; i++)
    {
        int nb = countNbOnes(_squares[i]);
        _nbMaterial[i] += nb;
        sum += nb;
    }
    _nbMaterial[PIECE_0] = 64-sum;

    return p;
}

std::string cornedpanda::Board::getFen() const
{
    std::stringstream ss;

    // write piece placement
    // for rank 8 to rank 1
    for (uint64_t rank=cornedpanda::RANK_8; rank!=0ULL; rank>>=8)
    {
        // for file A to file H
        for (uint64_t file=FILE_A; file!=FILE_A<<8; file<<=1)
        {
            uint64_t idx = rank & file;
            if (_emptySquares & idx)
            {
                // count empty squares 
                int n = 1;
                uint64_t file_ = file<<1;
                while (file_ != FILE_A<<8)
                {
                    if ((_emptySquares & file_ & rank) == 0ULL) break;
                    n++;
                    file_ <<= 1;
                }
                ss << n;
                file = file_>>1;
            }
            else
            {
                // find the piece and write it
                if (_squares[PIECE_P] & idx) ss << 'P';
                else if (_squares[PIECE_p] & idx) ss << 'p';
                else if (_squares[PIECE_R] & idx) ss << 'R';
                else if (_squares[PIECE_r] & idx) ss << 'r';
                else if (_squares[PIECE_N] & idx) ss << 'N';
                else if (_squares[PIECE_n] & idx) ss << 'n';
                else if (_squares[PIECE_B] & idx) ss << 'B';
                else if (_squares[PIECE_b] & idx) ss << 'b';
                else if (_squares[PIECE_Q] & idx) ss << 'Q';
                else if (_squares[PIECE_q] & idx) ss << 'q';
                else if (_squares[PIECE_K] & idx) ss << 'K';
                else if (_squares[PIECE_k] & idx) ss << 'k';
                else assert(false);
            }
        }
        // end of line: '/' or ' '  
        if (rank != RANK_1) ss << '/';
        else ss << ' ';
    }

    // write side to move
    ss << (_isWhiteToMove ? "w " : "b ");

    // write castling ability
    bool isCastling_K = ( _castlings & (CASTLING_K_from) ) == CASTLING_K_from;
    bool isCastling_Q = ( _castlings & (CASTLING_Q_from) ) == CASTLING_Q_from;
    bool isCastling_k = ( _castlings & (CASTLING_k_from) ) == CASTLING_k_from;
    bool isCastling_q = ( _castlings & (CASTLING_q_from) ) == CASTLING_q_from;
    if (isCastling_K or isCastling_Q or isCastling_k or isCastling_q)
    {
        if (isCastling_K) ss << 'K';
        if (isCastling_Q) ss << 'Q';
        if (isCastling_k) ss << 'k';
        if (isCastling_q) ss << 'q';
        ss << ' ';
    }
    else
    {
        ss << "- ";
    }

    // write en passant target square
    ss << indexToFenString(_enPassantTarget) << ' ';

    // write halfmove clock
    ss << _halfmoveClock << ' ';

    // write fullmove counter
    ss << _fullmoveCounter << ' ' ;

    return ss.str();
}

void cornedpanda::Board::updateSquares()
{
    _whiteSquares = _squares[PIECE_P] | _squares[PIECE_R] | _squares[PIECE_N] | _squares[PIECE_B] | _squares[PIECE_Q] | _squares[PIECE_K];
    _blackSquares = _squares[PIECE_p] | _squares[PIECE_r] | _squares[PIECE_n] | _squares[PIECE_b] | _squares[PIECE_q] | _squares[PIECE_k];
    _occupiedSquares = _whiteSquares | _blackSquares;
    _emptySquares = ~_occupiedSquares; 

#ifdef DEBUG
    uint64_t s = 0ULL;
    for (int i=PIECE_P; i<=PIECE_k; i++)
    {
        if ((s & _squares[i]) != 0ULL)
        {
            cornedpanda::logStream << "error in cornedpanda::Board::updateSquares \n";
            for (int k=PIECE_P; k<=PIECE_k; k++)
            {
                cornedpanda::logStream << "_squares[" << k << "] = \n";
                printUint64Board(cornedpanda::logStream, _squares[k], "  ");
            }
        }
        assert( (s & _squares[i]) == 0ULL);
        s |= _squares[i];
    }
#endif
}

void cornedpanda::Board::updateThreefoldRepetition()
{
    auto p = _repetitions.find(_occupiedSquares);
    if (p != _repetitions.end())
    {
        p->second++;
        _threefoldRepetition = p->second >= 3;
    }
    else
    {
        _repetitions[_occupiedSquares] = 1;
    }
}

void cornedpanda::Board::updateInCheck()
{
    if (_isWhiteToMove)
        _isInCheck = (getBlackAttacks() & _squares[PIECE_K]) != 0;
    else
        _isInCheck = (getWhiteAttacks() & _squares[PIECE_k]) != 0;
}

void cornedpanda::Board::updateMoves()
{
    _possibleMoves.clear();
    int piece0, piece1;
    if (_isWhiteToMove)
    {
        piece0 = PIECE_P;
        piece1 = PIECE_K;
    }
    else
    {
        piece0 = PIECE_p;
        piece1 = PIECE_k;
    }
    // for each piece
    for (int piece = piece0; piece <= piece1; piece++) 
    {
        uint64_t current = _squares[piece];
        // for each square of this piece
        while (current) 
        { 
            uint64_t from = (current & -current);
            // add moves in _possibleMoves
            cornedpanda::Board::movesFunctions[piece]((cornedpanda::Board&)*this, from);
            current &= (current - 1);
        }
    }
}

const std::vector<cornedpanda::Move> & cornedpanda::Board::getMoves() const
{
    return _possibleMoves;
}

std::ostream & cornedpanda::operator<<(std::ostream & os, const Board & board)
{
    os << "_squares = " << std::endl;
    // for rank 8 to rank 1
    for (uint64_t rank=cornedpanda::RANK_8; rank!=0ULL; rank>>=8)
    {
        os << "    ";
        // for file A to file H
        for (uint64_t file=cornedpanda::FILE_A; file!=cornedpanda::FILE_A<<8; file<<=1)
        {
            uint64_t idx = rank & file;
            if (board._squares[PIECE_P] & idx) os << 'P';
            else if (board._squares[PIECE_R] & idx) os << 'R';
            else if (board._squares[PIECE_N] & idx) os << 'N';
            else if (board._squares[PIECE_B] & idx) os << 'B';
            else if (board._squares[PIECE_Q] & idx) os << 'Q';
            else if (board._squares[PIECE_K] & idx) os << 'K';
            else if (board._squares[PIECE_p] & idx) os << 'p';
            else if (board._squares[PIECE_r] & idx) os << 'r';
            else if (board._squares[PIECE_n] & idx) os << 'n';
            else if (board._squares[PIECE_b] & idx) os << 'b';
            else if (board._squares[PIECE_q] & idx) os << 'q';
            else if (board._squares[PIECE_k] & idx) os << 'k';
            else os << '.';
        }
        os << std::endl;
    }

    os << "_isWhiteToMove = " << board._isWhiteToMove << std::endl;
    os << "_isInCheck = " << board._isInCheck << std::endl;
    os << "_castling_K = " << ((board._castlings & CASTLING_K_from) == CASTLING_K_from) << std::endl;
    os << "_castling_Q = " << ((board._castlings & CASTLING_Q_from) == CASTLING_Q_from) << std::endl;
    os << "_castling_k = " << ((board._castlings & CASTLING_k_from) == CASTLING_k_from) << std::endl;
    os << "_castling_q = " << ((board._castlings & CASTLING_q_from) == CASTLING_q_from) << std::endl;

    if (board._enPassantTarget == 0)
        os << "_enPassantTarget = " << '-' << std::endl;
    else
        os << "_enPassantTarget = " << board.indexToFenString(board._enPassantTarget) << std::endl;
    os << "_halfmoveClock = " << board._halfmoveClock << std::endl;
    os << "_fullmoveCounter = " << board._fullmoveCounter << std::endl;

    os << "_possibleMoves = ";
    for (const Move & m : board.getMoves()) os << board.moveToString(m) << ' ';
    os << std::endl;

    return os;
}

void cornedpanda::Board::printUint64Board(std::ostream & os, uint64_t occupancy, const std::string & prefixString)
{
    uint64_t rank = 0xFF00000000000000;
    uint64_t column = 0x0101010101010101;
    for (short i = 0; i < 8; i++) 
    {
        os << prefixString;
        for (short j = 0; j < 8; j++) 
        {
            if (occupancy & (rank & column)) os << "1 ";
            else os << "0 ";
            column <<= 1ULL;
        }
        os << std::endl;
        rank >>= 8;
        column = 0x0101010101010101;
    }
}

std::string cornedpanda::Board::indexToFenString(uint64_t idx) 
{
    if (idx == 0ULL)
    {
        return std::string("-");
    }
    else
    {
        int k = indexUint64ToInt[(idx * debruijn64) >> 58];
        assert(k == log2(idx));
        char rank = '1' + char(k/8);
        char file = 'a' + char(k%8);

        std::stringstream ss;
        ss << file << rank;
        return ss.str();
    }
}

const char * cornedpanda::Board::fenStringToIndex(const char * str, uint64_t & idx) 
{
    if (str[0] == '-')
    {
        // read empty position
        idx = 0ULL;
        return str+1;
    }
    else
    {
        // read position (e.g. "d4")
        int iFile = str[0] - 'a'; 
        if (iFile<0 or iFile>7) return nullptr;

        int iRank = str[1] - '1';
        if (iRank<0 or iRank>7) return nullptr;

        int k = iRank*8 + iFile;
        idx = 1ULL << k;

        return str+2;
    }
}

std::string cornedpanda::Board::moveToString(const Move & move) 
{
    std::stringstream ss;
    ss << indexToFenString(move._from) << indexToFenString(move._to);
    
    switch (move._promotion)
    {
        case PIECE_Q:
        case PIECE_q: ss << 'q'; break;
        case PIECE_N:
        case PIECE_n: ss << 'n'; break;
        case PIECE_R:
        case PIECE_r: ss << 'r'; break;
        case PIECE_B:
        case PIECE_b: ss << 'b'; break;
        default: break;
    }
    
    return ss.str();
}

const char * cornedpanda::Board::stringToMove(const char * str, Move & move) const
{
    const char * p = str;
    while (*p and (*p==' ' or *p=='\t' or *p=='\n')) 
        p++;
    
    if (*p == 0)
        return nullptr;

    // read source square
    uint64_t idx0 = 0ULL;
    p = fenStringToIndex(p, idx0);
    if (p==nullptr or idx0==0ULL)
        return nullptr;

    // read target square
    uint64_t idx1 = 0ULL;
    p = fenStringToIndex(p, idx1);
    if (p==nullptr or idx1==0ULL)
        return nullptr;

    // read promotion
    Piece promotion = PIECE_0;
    switch (*p)
    {
        case 'q' : promotion = _isWhiteToMove ? PIECE_Q : PIECE_q; p++; break;
        case 'n' : promotion = _isWhiteToMove ? PIECE_N : PIECE_n; p++; break;
        case 'r' : promotion = _isWhiteToMove ? PIECE_R : PIECE_r; p++; break;
        case 'b' : promotion = _isWhiteToMove ? PIECE_B : PIECE_b; p++; break;
        default: break;
    }

    // compute other info about the move
    uint64_t epTarget 
        = (((idx1&RANK_4)>>16)&idx0&_squares[PIECE_P])<<8
        | (((idx1&RANK_5)<<16)&idx0&_squares[PIECE_p])>>8;

    int movingPiece = PIECE_P;
    while (movingPiece!=PIECE_0 and (_squares[movingPiece]&idx0)==0ULL) movingPiece++;

    // TODO detection capture plus rapide ?
    int capture = PIECE_P;
    while (capture != PIECE_0 and (_squares[capture] & idx1) == 0ULL)
            capture++;
    
    move = {idx0, idx1, epTarget, (Piece)movingPiece, promotion, (Piece)capture, false};

    Board copy = *this;
    copy.makeMovePartialy(move); 
    copy.updateInCheck();
    move._check = copy._isInCheck;

    return p;
}

int cornedpanda::Board::countNbOnes(uint64_t occupancy)
{
	static const uint64_t S[] = { 1, 2, 4, 8, 16, 32 };

	static const uint64_t B[] = { 0x5555555555555555, 0x3333333333333333, 0x0F0F0F0F0F0F0F0F, 
        0x00FF00FF00FF00FF, 0x0000FFFF0000FFFF, 0x00000000FFFFFFFF };

	uint64_t count; 
	count = occupancy - ((occupancy >> 1) & B[0]);
	count = ((count >> S[1]) & B[1]) + (count & B[1]);
	count = ((count >> S[2]) + count) & B[2];
	count = ((count >> S[3]) + count) & B[3];
	count = ((count >> S[4]) + count) & B[4];
	count = ((count >> S[5]) + count) & B[5];

	return count;
}

cornedpanda::GameState cornedpanda::Board::getGameState() const
{
    if (_halfmoveClock >= 100 or _threefoldRepetition) 
        return GAME_STATE_DRAW;

    if (not _possibleMoves.empty()) 
        return GAME_STATE_RUNNING;

    if (not _isInCheck) 
        return GAME_STATE_DRAW;

    return _isWhiteToMove ? GAME_STATE_BLACK_WON : GAME_STATE_WHITE_WON;
}

void cornedpanda::Board::makeMovePartialy(const cornedpanda::Move & m)
{
    const uint64_t CASTLING_king_from    = 0x1000000000000010;
    const uint64_t CASTLING_rook_from    = 0x8100000000000081;
    const uint64_t CASTLING_rook_to      = 0x2800000000000028;
    const uint64_t ENPASSANT_captures    = 0x000000FFFF000000;

    assert(m._piece != PIECE_0);
    assert((m._piece >= PIECE_p and m._piece <= PIECE_k and not _isWhiteToMove) 
            or (m._piece >= PIECE_P and m._piece <= PIECE_K and _isWhiteToMove));

    // update _squares 
    uint64_t idx0 = m._from;
    uint64_t idx1 = m._to;
    uint64_t notidx0 = ~idx0;
    uint64_t notidx1 = ~idx1;
    // update rooks if castling move 
    uint64_t c_k = idx0 & _castlings & CASTLING_king_from;
    uint64_t c_r = (idx1>>2 | idx1<<1) & _castlings & CASTLING_rook_from & (c_k<<3 | c_k>>4);
    uint64_t sq_R = _squares[PIECE_R] & c_r;
    _squares[PIECE_R] |= (sq_R>>2 | sq_R<<3) & CASTLING_rook_to;
    _squares[PIECE_R] &= ~c_r;
    uint64_t sq_r = _squares[PIECE_r] & c_r;
    _squares[PIECE_r] |= (sq_r>>2 | sq_r<<3) & CASTLING_rook_to;
    _squares[PIECE_r] &= ~c_r;
    // update pawns if en-passant
    uint64_t to_ep = idx1 & _enPassantTarget; 
    uint64_t from_ep = idx0 & (_squares[PIECE_P] | _squares[PIECE_p]);
    uint64_t capture_ep = (to_ep>>8 | to_ep<<8) & (from_ep>>1 | from_ep<<1) & ENPASSANT_captures;
    _squares[PIECE_P] &= ~capture_ep;
    _squares[PIECE_p] &= ~capture_ep;
    // update _squares for move m 
    for (int i=0; i<12; i++)
    {
        // remove old destination
        _squares[i] &= notidx1;
        // remove old start and add new destination
        if (_squares[i] & idx0)
        {
            // remove old index
            _squares[i] &= notidx0;
            // add new destination
            _squares[i] |= idx1;
        }
    }
    // update promotion and precomputed squares
    if (m._promotion != PIECE_0)
    {
        // remove pawn at destination 
        _squares[PIECE_P] &= notidx1;
        _squares[PIECE_p] &= notidx1;
        // add promotion at destination 
        _squares[m._promotion] |= idx1;
        // update _nbMaterial
        _nbMaterial[m._piece]--;
        _nbMaterial[m._promotion]++;
        // update _isWhiteToMove
        _isWhiteToMove = not _isWhiteToMove;
        // update square and check
        updateSquares();
        updateInCheck();
    }
    else
    {
        // update _isWhiteToMove
        _isWhiteToMove = not _isWhiteToMove;
        // update square and check
        updateSquares();
        _isInCheck = m._check;
    }

    // castlings
    _castlings &= ~(idx0 & CASTLING_all_from );

    // update _enPassantTarget
    _enPassantTarget = m._epTarget;

    // update _halfmoveClock 
    if (m._capture != PIECE_0 or m._piece == PIECE_P or m._piece == PIECE_p)
        _halfmoveClock = 0;
    else
        _halfmoveClock++;

    // update _fullmoveCounter and _material (white or black)
    if (_isWhiteToMove)
        _fullmoveCounter++;

    // update _nbMaterial
    // if capture, one more PIECE_0
    // if no capture, m._capture==PIECE_0 i.e. do nothing
    _nbMaterial[m._capture]--;
    _nbMaterial[PIECE_0]++;

    assert(m._capture == PIECE_0 
            or (m._capture >= PIECE_p and m._capture <= PIECE_k and not _isWhiteToMove) 
            or (m._capture >= PIECE_P and m._capture <= PIECE_K and _isWhiteToMove));
}

void cornedpanda::Board::makeMove(const cornedpanda::Move & m)
{
    makeMovePartialy(m);
    updateMoves();
    updateThreefoldRepetition();
}

void cornedpanda::Board::pushWhiteMoves(uint64_t from, uint64_t destinations, Piece piece)
{
    // for each destination
    while (destinations) 
    {
        uint64_t to = destinations & -destinations;
        uint64_t epTarget = (((to&RANK_4)>>16)&from&_squares[PIECE_P])<<8;
        Piece promotion = (RANK_7 & from & _squares[PIECE_P]) != 0ULL ? PIECE_Q : PIECE_0;
        // TODO detection capture plus rapide ?
        int capture = PIECE_p;
        while (capture != PIECE_0 and (_squares[capture] & to) == 0ULL)
            capture++;
        Move m = {from, to, epTarget, piece, promotion, (Piece)capture, false};

        // m is a white move and we want to check whether it is legal, 
        // meaning whether the white king is in check after the move
        Board copy = *this;
        copy.makeMovePartialy(m); 
        uint64_t controlledSquares = copy.getBlackAttacks();
        if ((controlledSquares & copy._squares[PIECE_K]) == 0)
        {
            uint64_t controlledSquares2 = copy.getWhiteAttacks();
            m._check = controlledSquares2 & copy._squares[PIECE_k];
            _possibleMoves.push_back(m); 
        }

        destinations &= (destinations - 1);
    }
}

void cornedpanda::Board::pushBlackMoves(uint64_t from, uint64_t destinations, Piece piece)
{
    // for each destination
    while (destinations) 
    {
        uint64_t to = destinations & -destinations;
        uint64_t epTarget = (((to&RANK_5)<<16)&from&_squares[PIECE_p])>>8;
        Piece promotion = (RANK_2 & from & _squares[PIECE_p]) != 0ULL ? PIECE_q : PIECE_0;
        //TODO detection capture plus rapide ?
        int capture = PIECE_P;
        // capture of a white piece
        while (capture != PIECE_p and (_squares[capture] & to) == 0ULL)
            capture++;
        if (capture == PIECE_p)
            capture = PIECE_0;
        Move m = {from, to, epTarget, piece, promotion, (Piece)capture, false};

        // m is a black move and we want to check whether it is legal, 
        // meaning whether the black king is in check after the move
        Board copy = *this;
        copy.makeMovePartialy(m); 
        uint64_t controlledSquares = copy.getWhiteAttacks();
        if ((controlledSquares & copy._squares[PIECE_k]) == 0)
        {
            uint64_t controlledSquares2 = copy.getBlackAttacks();
            m._check = controlledSquares2 & copy._squares[PIECE_K];
            _possibleMoves.push_back(m); 
        }

        destinations &= (destinations - 1);
    }
}

uint64_t cornedpanda::Board::getWhiteAttacks() const 
{
    uint64_t possibleAttacks = 0;
    // for each white piece
    for (int piece = PIECE_P; piece <= PIECE_K; piece++) 
    { 
        uint64_t current = _squares[piece];
        while (current)
        {
            uint64_t from = (current & -current);
            uint64_t currentPossible = attacksFunctions[piece](*this, from);
            while (currentPossible) 
            {
                // getting a destination among all
                possibleAttacks |= currentPossible & -currentPossible; 
                currentPossible &= (currentPossible - 1);
            }
            current &= (current - 1);
        }
    }
    return possibleAttacks;
}

uint64_t cornedpanda::Board::getBlackAttacks() const
{
    uint64_t possibleAttacks = 0;
    // for each black piece
    for (int piece = PIECE_p; piece <= PIECE_k; piece++) 
    {
        uint64_t current = _squares[piece];
        while (current) 
        { 
            uint64_t from = (current & -current);
            uint64_t currentPossible = attacksFunctions[piece](*this, from);
            while (currentPossible) 
            {
                possibleAttacks |= currentPossible & -currentPossible; 
                currentPossible &= (currentPossible - 1);
            }
            current &= (current - 1);
        }
    }
    return possibleAttacks;
}

uint64_t cornedpanda::Board::getWhitePawnPushes(uint64_t from) const 
{
    // one square above if possible
    uint64_t pushDestinations = (from << 8) & _emptySquares; 
    // two squares above if one square was possible and the arrival square is on the RANK_4
    pushDestinations |= ((pushDestinations << 8) & _emptySquares & RANK_4 & _emptySquares); 
    return pushDestinations; 
}

uint64_t cornedpanda::Board::getBlackPawnPushes(uint64_t from) const 
{
    // one square above if possible
    uint64_t pushDestinations = (from >> 8) & _emptySquares;
    // two squares above if one square was possible and the arrival square is on the RANK_5
    pushDestinations |= ((pushDestinations >> 8) & _emptySquares & RANK_5 & _emptySquares); 
    return pushDestinations;
}

uint64_t cornedpanda::Board::getWhitePawnAttacks(uint64_t from) const 
{
    uint64_t from9 = from << 9;
    uint64_t from7 = from << 7;
    uint64_t attackDestinations 
        = (from9 & _blackSquares & FILE_A_COMP)
        | (from7 & _blackSquares & FILE_H_COMP)
        | (RANK_6 & _enPassantTarget & (from9 | from7));
    return attackDestinations;
}

uint64_t cornedpanda::Board::getBlackPawnAttacks(uint64_t from) const 
{
    uint64_t from9 = from >> 9;
    uint64_t from7 = from >> 7;
    uint64_t attackDestinations 
        = (from7 & _whiteSquares & FILE_A_COMP)
        | (from9 & _whiteSquares & FILE_H_COMP)
        | (RANK_3 & _enPassantTarget & (from9 | from7));
    return attackDestinations;
}

void cornedpanda::Board::getWhitePawnMoves(uint64_t from) 
{
    uint64_t destinations = getWhitePawnPushes(from) | getWhitePawnAttacks(from);
    pushWhiteMoves(from, destinations, PIECE_P);
}

void cornedpanda::Board::getBlackPawnMoves(uint64_t from) 
{
    uint64_t destinations = getBlackPawnPushes(from) | getBlackPawnAttacks(from);
    pushBlackMoves(from, destinations, PIECE_p);
}

void cornedpanda::Board::getWhiteKnightMoves(uint64_t from)
{
    uint64_t destinations = getWhiteKnightAttacks(from);
    pushWhiteMoves(from, destinations, PIECE_N);
}

void cornedpanda::Board::getBlackKnightMoves(uint64_t from) 
{
    uint64_t destinations = getBlackKnightAttacks(from);
    pushBlackMoves(from, destinations, PIECE_n);
}

uint64_t cornedpanda::Board::getWhiteKnightAttacks(uint64_t from)  const
{
    uint64_t destinations
        = (from << 17 & FILE_A_COMP)                // NNE
        | (from << 10 & FILE_B_COMP & FILE_A_COMP)  // NEE
        | (from >> 6 & FILE_B_COMP & FILE_A_COMP)   // SEE
        | (from >> 15 & FILE_A_COMP)                // SSE
        | (from << 15 & FILE_H_COMP)                // NNO
        | (from << 6 & FILE_H_COMP & FILE_G_COMP)   // NOO
        | (from >> 10 & FILE_H_COMP & FILE_G_COMP)  // SOO
        | (from >> 17 & FILE_H_COMP);               // SSO

    return destinations & ~_whiteSquares;
}

uint64_t cornedpanda::Board::getBlackKnightAttacks(uint64_t from)  const
{
    uint64_t destinations 
        = (from << 17 & FILE_A_COMP)                // NNE
        | (from << 10 & FILE_B_COMP & FILE_A_COMP)  // NEE
        | (from >> 6 & FILE_B_COMP & FILE_A_COMP)   // SEE
        | (from >> 15 & FILE_A_COMP)                // SSE
        | (from << 15 & FILE_H_COMP)                // NNO
        | (from << 6 & FILE_H_COMP & FILE_G_COMP)   // NOO
        | (from >> 10 & FILE_H_COMP & FILE_G_COMP)  // SOO
        | (from >> 17 & FILE_H_COMP);               // SSO

    return destinations & ~_blackSquares;
}

void cornedpanda::Board::getWhiteRookMoves(uint64_t from) 
{
    uint64_t destinations = getWhiteRookAttacks(from);
    pushWhiteMoves(from, destinations, PIECE_R);
}

void cornedpanda::Board::getBlackRookMoves(uint64_t from)
{
    uint64_t destinations = getBlackRookAttacks(from);
    pushBlackMoves(from, destinations, PIECE_r);
}

uint64_t cornedpanda::Board::getWhiteRookAttacks(uint64_t from) const
{
    int k = indexUint64ToInt[(from * debruijn64) >> 58];
    return Rmagic(k, _occupiedSquares) & ~_whiteSquares;
}

uint64_t cornedpanda::Board::getBlackRookAttacks(uint64_t from) const
{
    int k = indexUint64ToInt[(from * debruijn64) >> 58];
    return Rmagic(k, _occupiedSquares) & ~_blackSquares;
}

void cornedpanda::Board::getWhiteBishopMoves(uint64_t from)
{
    uint64_t destinations = getWhiteBishopAttacks(from);
    pushWhiteMoves(from, destinations, PIECE_B);
}

void cornedpanda::Board::getBlackBishopMoves(uint64_t from)
{
    uint64_t destinations = getBlackBishopAttacks(from);
    pushBlackMoves(from, destinations, PIECE_b);
}

uint64_t cornedpanda::Board::getWhiteBishopAttacks(uint64_t from) const
{
    int k = indexUint64ToInt[(from * debruijn64) >> 58];
    return Bmagic(k, _occupiedSquares) & ~_whiteSquares;
}

uint64_t cornedpanda::Board::getBlackBishopAttacks(uint64_t from) const
{
    int k = indexUint64ToInt[(from * debruijn64) >> 58];
    return Bmagic(k, _occupiedSquares) & ~_blackSquares;
}

void cornedpanda::Board::getWhiteQueenMoves(uint64_t from)
{
    uint64_t destinations = getWhiteQueenAttacks(from);
    pushWhiteMoves(from, destinations, PIECE_Q);
}

void cornedpanda::Board::getBlackQueenMoves(uint64_t from)
{
    uint64_t destinations = getBlackQueenAttacks(from);
    pushBlackMoves(from, destinations, PIECE_q);
}

uint64_t cornedpanda::Board::getWhiteQueenAttacks(uint64_t from) const
{
    int k = indexUint64ToInt[(from * debruijn64) >> 58];
    return Qmagic(k, _occupiedSquares) & ~_whiteSquares;
}

uint64_t cornedpanda::Board::getBlackQueenAttacks(uint64_t from) const
{
    int k = indexUint64ToInt[(from * debruijn64) >> 58];
    return Qmagic(k, _occupiedSquares) & ~_blackSquares;
}

void cornedpanda::Board::getWhiteKingMoves(uint64_t from)
{
    uint64_t destinations = getWhiteKingAttacks(from);

    // castling KQ
    if (not _isInCheck) 
    {
        const uint64_t CASTLING_KQ_king_from  = 0x0000000000000010;
        const uint64_t CASTLING_K_king_to     = 0x0000000000000040;
        const uint64_t CASTLING_K_rook_from   = 0x0000000000000080;
        const uint64_t CASTLING_K_empty       = 0x0000000000000060;
        const uint64_t CASTLING_Q_king_to     = 0x0000000000000004;
        const uint64_t CASTLING_Q_rook_from   = 0x0000000000000001;
        const uint64_t CASTLING_Q_empty       = 0x000000000000000E;
        auto checkOkFun = [this] (uint64_t from, uint64_t to) 
        {
            // TODO detection capture plus rapide ?
            int capture = PIECE_p;
            while (capture != PIECE_0 and (_squares[capture] & to) == 0ULL)
                capture++;
            Move m = {from, to, 0ULL, PIECE_K, PIECE_0, (Piece)capture, false};
            Board copy = *this;
            copy.makeMovePartialy(m); 
            return (copy.getBlackAttacks() & copy._squares[PIECE_K]) == 0;
        };
        uint64_t c_KQ_king_from = CASTLING_KQ_king_from & _squares[PIECE_K] & _castlings;
        // castling K
        uint64_t c_K_rook_from = _castlings & CASTLING_K_rook_from & _squares[PIECE_R];
        uint64_t c_K_empty = _emptySquares & CASTLING_K_empty;
        if ((c_KQ_king_from | c_K_rook_from | c_K_empty) == 0x00000000000000F0
                and checkOkFun(CASTLING_KQ_king_from, 0x0000000000000020))
        {
            destinations |= CASTLING_K_king_to;
        }
        // castling Q
        uint64_t c_Q_rook_from = _castlings & CASTLING_Q_rook_from & _squares[PIECE_R];
        uint64_t c_Q_empty = _emptySquares & CASTLING_Q_empty;
        if ((c_KQ_king_from | c_Q_rook_from | c_Q_empty) == 0x000000000000001F
                and checkOkFun(CASTLING_KQ_king_from, 0x0000000000000008))
        {
            destinations |= CASTLING_Q_king_to;
        }
    }

    pushWhiteMoves(from, destinations, PIECE_K);
}

void cornedpanda::Board::getBlackKingMoves(uint64_t from)
{
    uint64_t destinations = getBlackKingAttacks(from);

    // castling kq
    if (not _isInCheck) 
    {
        const uint64_t CASTLING_kq_king_from  = 0x1000000000000000;
        const uint64_t CASTLING_k_king_to     = 0x4000000000000000;
        const uint64_t CASTLING_k_rook_from   = 0x8000000000000000;
        const uint64_t CASTLING_k_empty       = 0x6000000000000000;
        const uint64_t CASTLING_q_king_to     = 0x0400000000000000;
        const uint64_t CASTLING_q_rook_from   = 0x0100000000000000;
        const uint64_t CASTLING_q_empty       = 0x0E00000000000000;
        auto checkOkFun = [this] (uint64_t from, uint64_t to) 
        {
            // TODO detection capture plus rapide ?
            int capture = PIECE_P;
            // capture of a white piece
            while (capture != PIECE_p and (_squares[capture] & to) == 0ULL)
                capture++;
            if (capture == PIECE_p)
                capture = PIECE_0;
            Move m = {from, to, 0ULL, PIECE_k, PIECE_0, (Piece)capture, false};
            Board copy = *this;
            copy.makeMovePartialy(m); 
            return (copy.getBlackAttacks() & copy._squares[PIECE_k]) == 0;
        };
        uint64_t c_kq_king_from = CASTLING_kq_king_from & _squares[PIECE_k] & _castlings;
        // castling k
        uint64_t c_k_rook_from = _castlings & CASTLING_k_rook_from & _squares[PIECE_r];
        uint64_t c_k_empty = _emptySquares & CASTLING_k_empty;
        if ((c_kq_king_from | c_k_rook_from | c_k_empty) == 0xF000000000000000
                and checkOkFun(CASTLING_kq_king_from, 0x2000000000000000))
        {
            destinations |= CASTLING_k_king_to;
        }
        // castling q
        uint64_t c_q_rook_from = _castlings & CASTLING_q_rook_from & _squares[PIECE_r];
        uint64_t c_q_empty = _emptySquares & CASTLING_q_empty;
        if ((c_kq_king_from | c_q_rook_from | c_q_empty) == 0x1F00000000000000
                and checkOkFun(CASTLING_kq_king_from, 0x0800000000000000))
        {
            destinations |= CASTLING_q_king_to;
        }
    }

    pushBlackMoves(from, destinations, PIECE_k);
}

uint64_t cornedpanda::Board::getWhiteKingAttacks(uint64_t from) const
{
    uint64_t destinations = from 
        | (from & FILE_H_COMP) << 1
        | (from & FILE_A_COMP) >> 1;
    destinations |= destinations >> 8;
    destinations |= destinations << 8;
    destinations ^= from;
    return destinations &= ~_whiteSquares;
}

uint64_t cornedpanda::Board::getBlackKingAttacks(uint64_t from) const
{
    uint64_t destinations = from
        | (from & FILE_H_COMP) << 1
        | (from & FILE_A_COMP) >> 1;
    destinations |= destinations >> 8;
    destinations |= destinations << 8;
    destinations ^= from;
    return destinations &= ~_blackSquares;
}

