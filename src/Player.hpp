// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef _PLAYER_HPP_
#define _PLAYER_HPP_

#include "BoardEval.hpp"

#include <map>
#include <string>
#include <random>

namespace cornedpanda
{

    class Player 
    {
            bool _isInit;
            std::mt19937 _engine;
            std::uniform_real_distribution<float> _distribution;

        public:
            // uci options
            std::map<std::string, float> _parameters;

        protected:
            BoardEval _board;
            Move _bestMove;
            bool _isBestMoveOk;   // a best move is available
            bool _isBestMoveNone; // no best move (game ended)

            // stats
            int _statsDepth;
            int _statsSelDepth;
            uint64_t _statsNodes;
            uint64_t _statsQNodes;
            int _statsCurrMoveNumber;
            Move _statsCurrMove;

        public:
            Player();
            virtual ~Player();
            virtual void init();
            bool isInit() const;
            int random(int b);
            void printBoard(std::ostream & os) const;
            virtual std::string formatMovesStats() const;
            std::map<std::string, int> & getBoardParameters();

            virtual void initBestMove();
            virtual void iterBestMove();  // assumes initBestMove() has been called and isBestMoveOk() == false
            virtual bool isBestMoveOk();
            virtual std::string getBestMove();  // assumes isBestMoveOk() == true
            virtual void printStatsBestMove();  // assumes getBestMove() has been called
            void getStats(int & depth, int & seldepth, uint64_t & nodes, uint64_t & qnodes, int & movenum, Move & move) const;

            // implement abstract methods of senjo (uci engine)
            bool isWhiteToMove() const;
            const char * setFen(const char * fen);
            std::string getFen() const;
            // do nothing but can be overridden to reuse computation after uci move
            virtual const char * makeMoveString(const char * str); 
    };

}

#endif

