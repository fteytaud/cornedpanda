
#ifndef _LOG_HPP_
#define _LOG_HPP_

#include "senjo/Output.h"

#include <fstream>

namespace cornedpanda
{
    extern std::ofstream logStream;

    using Output = senjo::Output;

    void sigHandler(int);

    void intHandler(int i);

    void exitHandler();

    void initLogStream(const char * filename);
}

#endif

