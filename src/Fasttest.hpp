// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef _FASTTEST_HPP_
#define _FASTTEST_HPP_

#include "Board.hpp"
#include "Player.hpp"

#include <algorithm>

namespace cornedpanda 
{

    void makeMovesFromString(Board& board, const char * ptrString)
    {
        while (true) 
        {
            cornedpanda::Move move;
            ptrString = board.stringToMove(ptrString, move);
            if (ptrString == nullptr) break;
            board.makeMove(move);
        }
    }

    void makeMovesFromString(Player & player, const char * ptrString)
    {
        while (true) 
        {
            if (ptrString == nullptr) break;
            ptrString = player.makeMoveString(ptrString);
        }
    }

    bool findMove(const Board& board, const char * str, cornedpanda::Move & refMove)
    {
        cornedpanda::Move mf;
        board.stringToMove(str, mf);
        uint64_t from = mf._from;
        uint64_t to = mf._to;
        auto f = [from, to] (const cornedpanda::Move & m) { return m._from == from and m._to == to; };
        const std::vector<cornedpanda::Move> & moves = board.getMoves();
        auto iter = std::find_if(moves.begin(), moves.end(), f);
        if (iter != moves.end())
        {
            refMove = *iter;
            return true;
        }
        else
        {
            return false;
        }
    }

}

#endif

