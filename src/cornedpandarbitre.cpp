// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "Board.hpp"
#include "Fasttest.hpp"
#include "Log.hpp"

#include <algorithm>
#include <iostream>
#include <regex>
#include <string>

// chess arbiter
// home-made protocol:
// - send "gamestate" + a list of valid moves (e.g. "e2e4 d7d5 f1b5 c7c6") to arbiter's stdin 
//   -> receive game state from arbiter's stdout ("running", "black won", "white won" or "draw")
// - send "validmove" + a list of valid moves + a move to test
//   -> receive "true" if the last move is valid or "false" if it is not
// - send "evalwhite" + a list of valid moves 
//   -> receive the score of the board after the moves (for the white player)
// - send "getmoves" + a list of valid moves 
//   -> receive possibles moves 
// - send "printboard" + a list of valid moves 
//   -> receive board data (ended by "done")
// - send "quit" 
int main()
{

    // global init
    // TODO do log output after each command
    cornedpanda::initLogStream("cornedpandarbitre.log");
    cornedpanda::Board::init();

    // main loop
    while (true)
    {
        std::smatch match;

        // read input (moves)
        std::string inputString;
        std::getline(std::cin, inputString);
        // add empty space for regex parsing
        inputString += " ";

        // parse first word if any
        std::regex_search(inputString, match, std::regex("\\s*(\\S+)"));

        // ignore empty input
        if (match.empty())
        {
            std::cout << "error: empty command" << std::endl;
            continue;
        }

        std::string command = match[1].str();

        // "help"
        if (command == "help")
        {
            std::cout << "chess arbiter protocol:" << std::endl;
            std::cout << "  - gamestate <valid moves> -> running | draw | white won | black won" << std::endl;
            std::cout << "  - validmove <valid moves> <move to test> -> true | false" << std::endl;
            std::cout << "  - evalwhite <valid moves> -> <score> " << std::endl;
            std::cout << "  - getmoves <valid moves> -> <possible moves> " << std::endl;
            std::cout << "  - printboard <valid moves> -> <board data> " << std::endl;
            std::cout << "  - help -> <this help>" << std::endl;
            std::cout << "  - quit" << std::endl;
            std::cout << "example:" << std::endl;
            std::cout << "  $ cornedpandarbitre.out " << std::endl; 
            std::cout << "  gamestate d2d4 d7d5" << std::endl;
            std::cout << "  running" << std::endl;
            std::cout << "  validmove d2d4 d7d5 e7e6" << std::endl;
            std::cout << "  false" << std::endl;
            std::cout << "  evalwhite d2d4 d7d5 g1f3" << std::endl;
            std::cout << "  302" << std::endl;
            std::cout << "  quit" << std::endl;
            std::cout << "  $" << std::endl;
            continue;
        }

        // "quit"
        if (command == "quit")
            break;

        // "validmove"
        if (command == "validmove")
        {
            // parse words
            std::regex r("[^\\s]+");
            std::sregex_iterator iter(inputString.begin(), inputString.end(), r);
            // ignore the "validmove" of the string
            ++iter;
            // get moves
            std::string previousMoves, lastMove;
            for (; iter != std::sregex_iterator(); ++iter)
            {
                previousMoves += " " + lastMove;
                lastMove = iter->str();
            }
            // if the last move is a rook/knight/bishop promotion, replace it by a queen promotion 
            std::regex rbRegex("(....)([r|n|b])");
            lastMove = std::regex_replace(lastMove, rbRegex, "$1q");
            // test last move (assuming previous moves are valid)
            // make previous moves 
            cornedpanda::Board board;
            cornedpanda::makeMovesFromString(board, previousMoves.c_str());
            // search last move in the possible moves of the board
            auto predicate = [&lastMove] (const cornedpanda::Move & m) { return cornedpanda::Board::moveToString(m) == lastMove; };
            const std::vector<cornedpanda::Move> & moves = board.getMoves();
            bool found = std::any_of(moves.begin(), moves.end(), predicate);
            std::cout << (found ? "true" :"false") << std::endl;
            continue;
        }

        // other commands (which use a board after some moves)
        // get moves (all the string after the command)
        std::regex_search(inputString, match, std::regex("\\s*\\S+\\s+(.*$)"));
        std::string moves = match[1].str();
        // make moves, on a board
        cornedpanda::BoardEval board;
        cornedpanda::makeMovesFromString(board, moves.c_str());

        // "gamestate"
        if (command == "gamestate")
        {
            const std::string & gameState = cornedpanda::GameStateStrings[board.getGameState()];
            std::cout << gameState << std::endl;
            continue;
        }

        // "evalwhite"
        if (command == "evalwhite")
        {
            int evalWhite = board.evaluate();
            std::cout << evalWhite << ' ' << std::endl;
            continue;
        }

        // "getmoves"
        if (command == "getmoves")
        {
            for (const cornedpanda::Move & m : board.getMoves())
                std::cout << cornedpanda::Board::moveToString(m) << ' ';
            std::cout << std::endl;
            continue;
        }

        // "printboard"
        if (command == "printboard")
        {
            std::cout << board ;
            std::cout << "done" << std::endl;
            continue;
        }

        std::cout << "error: unknown command (" << inputString << ")" << std::endl;

    }

    return 0;
}

