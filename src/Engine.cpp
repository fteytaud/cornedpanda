// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "Engine.hpp"
#include "Log.hpp"

#include "PlayerAB.hpp"
#include "PlayerMAXFETS.hpp"
#include "PlayerID.hpp"

#include <chrono>
#include <iomanip>
#include <sstream>

Engine::Engine() 
{
#ifdef DEBUG
    cornedpanda::logStream << "Engine::Engine\n";
#endif
    _players["Player"] = new cornedpanda::Player;
    _players["PlayerAB"] = new cornedpanda::PlayerAB;
    _players["PlayerABQ"] = new cornedpanda::PlayerABQ;
    _players["PlayerABQD"] = new cornedpanda::PlayerABQD;
    _players["PlayerABQLimited"] = new cornedpanda::PlayerABQLIMITED;
    _players["PlayerABQDLimited"] = new cornedpanda::PlayerABQDLIMITED;
    _players["PlayerID"] = new cornedpanda::PlayerID;
    _players["PlayerMAXFETS"] = new cornedpanda::PlayerMAXFETS;
    _iterPlayer = _players.find("PlayerAB");
}

Engine::~Engine()
{
    for (auto p : _players)
        delete p.second;
}

std::string Engine::GetEngineName() const 
{
#ifdef DEBUG
    cornedpanda::logStream << "Engine::GetEngineName\n";
#endif
    return "cornedpanda";
}

std::string Engine::GetEngineVersion() const 
{
#ifdef DEBUG
    cornedpanda::logStream << "Engine::GetEngineVersion\n";
#endif
    return "1.0";
}

std::string Engine::GetAuthorName() const 
{
#ifdef DEBUG
    cornedpanda::logStream << "Engine::GetAuthorName\n";
#endif
    return "Fabien Teytaud & Julien Dehos";
}

std::list<senjo::EngineOption> Engine::GetOptions() const 
{
#ifdef DEBUG
    cornedpanda::logStream << "Engine::GetOptions\n";
#endif
    std::list<senjo::EngineOption> options;

    // add an option for player selection
    std::set<std::string> playerNames;
    for (const auto & pair : _players)
        playerNames.insert(pair.first);
    senjo::EngineOption playerOption("Engine_player", _iterPlayer->first, 
            senjo::EngineOption::OptionType::ComboBox, INT64_MIN, INT64_MAX, playerNames);
    options.push_back(playerOption);

    // add parameters from all players
    for (const auto & playerPair : _players)
    {
        // player parameters
        for (const auto & paramPair : playerPair.second->_parameters)
        {
            // add option if not already existing
            auto optionNameFunc = [paramPair](const senjo::EngineOption & o)
                { return o.GetName() == paramPair.first; };
            if (not std::any_of(options.begin(), options.end(), optionNameFunc))
            {
                std::stringstream ss;
                ss << std::setprecision(2) << paramPair.second;
                options.push_back(senjo::EngineOption(paramPair.first, ss.str()));
            }
        }

        // BoardEval options
        for (const auto & paramPair : playerPair.second->getBoardParameters())
        {
            // add option if not already existing
            auto optionNameFunc = [paramPair](const senjo::EngineOption & o)
                { return o.GetName() == paramPair.first; };
            if (not std::any_of(options.begin(), options.end(), optionNameFunc))
            {
                std::stringstream ss;
                ss << paramPair.second;
                options.push_back(senjo::EngineOption(paramPair.first, ss.str()));
            }
        }
    }

    return options;
}

bool Engine::SetEngineOption(const std::string& optionName,
        const std::string& optionValue) 
{
#ifdef DEBUG
    cornedpanda::logStream << "Engine::SetEngineOption\n";
#endif
    bool found = false;
    // player selection
    if (optionName == "Engine_player")
    {
        auto iterPlayer = _players.find(optionValue);
        if (iterPlayer != _players.end()) 
        {
            _iterPlayer = iterPlayer;
            found = true;
        }
    }
    // player options
    for (const auto & playerPair : _players)
    {
        // player parameters
        auto & playerParameters = playerPair.second->_parameters;
        auto iterPlayerParam = playerParameters.find(optionName);
        if (iterPlayerParam != playerParameters.end()) 
        {
            iterPlayerParam->second = std::stof(optionValue);
            found = true;
        }
        // board parameters
        auto & boardParameters = playerPair.second->getBoardParameters();
        auto iterBoardParam = boardParameters.find(optionName);
        if (iterBoardParam != boardParameters.end()) 
        {
            iterBoardParam->second = std::stoi(optionValue);
            found = true;
        }
    }
    return found;
}

void Engine::Initialize() 
{
#ifdef DEBUG
    cornedpanda::logStream << "Engine::Initialize\n";
#endif
    if (not cornedpanda::Board::isInit())
        cornedpanda::Board::init();
    if (not _iterPlayer->second->isInit())
        _iterPlayer->second->init();
}

bool Engine::IsInitialized() const 
{
#ifdef DEBUG
    cornedpanda::logStream << "Engine::IsInitialized\n";
#endif
    return cornedpanda::Board::isInit() and _iterPlayer->second->isInit();
}

const char* Engine::SetPosition(const char* fen) 
{
#ifdef DEBUG
    cornedpanda::logStream << "Engine::SetPosition <- " << fen << '\n';
#endif
    const char * p =  _iterPlayer->second->setFen(fen);
#ifdef DEBUG
    cornedpanda::logStream << "Engine::SetPosition -> \n";
#endif
    return p;
}

const char* Engine::MakeMove(const char* str) 
{
#ifdef DEBUG
    cornedpanda::logStream << "Engine::MakeMove <- " << str << '\n' << _iterPlayer->second->getFen();
#endif
    const char * p = _iterPlayer->second->makeMoveString(str);
#ifdef DEBUG
    cornedpanda::logStream << "Engine::MakeMove <-\n";
#endif
    return p;
}

std::string Engine::GetFEN() const 
{
#ifdef DEBUG
    cornedpanda::logStream << "Engine::GetFEN <-\n";
#endif
    std::string fen = _iterPlayer->second->getFen();
#ifdef DEBUG
    cornedpanda::logStream << "Engine::GetFEN -> " << fen << '\n';
#endif
    return fen;
}

void Engine::PrintBoard() const 
{
#ifdef DEBUG
    cornedpanda::logStream << "Engine::PrintBoard\n";
#endif
    _iterPlayer->second->printBoard(std::cout);
}

bool Engine::WhiteToMove() const 
{
#ifdef DEBUG
    cornedpanda::logStream << "Engine::WhiteToMove <-\n";
#endif
    bool isWhiteToMove = _iterPlayer->second->isWhiteToMove();
#ifdef DEBUG
    cornedpanda::logStream << "Engine::WhiteToMove -> " << isWhiteToMove << '\n';
#endif
    return isWhiteToMove;
}

void Engine::ClearSearchData() 
{
#ifdef DEBUG
    cornedpanda::logStream << "Engine::ClearSearchData\n";
#endif
}


//--------------------------------------------------------------------------
//! \brief The last ponder move was played
//! The Go() method may return a ponder move which is the expected response
//! to the bestmove returned by Go().  If pondering is enabled the UCI
//! adapter may tell the engine to ponder this move, e.g. start searching
//! for a reply to the ponder move.  If, while the engine is pondering, the
//! ponder move is played this method will be called.  In general the engine
//! should make what it has learned from its pondering available for the next
//! Go() call.
//--------------------------------------------------------------------------
void Engine::PonderHit() 
{
#ifdef DEBUG
    cornedpanda::logStream << "Engine::PonderHit\n";
#endif
    // TODO Engine::PonderHit
}

//--------------------------------------------------------------------------
//! \brief Get statistics from the last (or current) search
//! \param[out] depth The maximum depth achieved
//! \param[out] seldepth The maximum selective depth achieved
//! \param[out] nodes The number of nodes searched
//! \param[out] qnodes The number of \p nodes that were in quiescence search
//! \param[out] msecs The number of milliseconds spent searching
//! \param[out] movenum The last/current move number searched
//! \param[out] move The last/current move searched (e.g. "e2e4")
//! \param[in] movelen The size of the \p move parameter
//--------------------------------------------------------------------------
/*
void Engine::GetStats(
        int* depth,
        int* seldepth,
        uint64_t* nodes,
        uint64_t* qnodes,
        uint64_t* msecs,
        int* movenum,
        char* move,
        const size_t movelen) const 
*/
void Engine::GetStats(int * depth, int * seldepth, uint64_t * nodes, uint64_t * qnodes,
        uint64_t*, int * movenum, char * move, const size_t movelen) const 
{

    // TODO stats uci
    cornedpanda::Player * ptrPlayer = _iterPlayer->second;
    cornedpanda::Move m;
    ptrPlayer->getStats(*depth, *seldepth, *nodes, *qnodes, *movenum, m);
    std::string moveStr = cornedpanda::Board::moveToString(m);
    strncpy(move, moveStr.c_str(), movelen);
    
#ifdef DEBUG
    cornedpanda::logStream << "Engine::GetStats\n";
#endif
}

//--------------------------------------------------------------------------
//! \brief Do performance test on the current position
//! Perft is useful for determining the speed of you move generator.
//! Perft is vital for validating whether your move generator is
//! producing the correct moves.  In any given position, a perft search to
//! a specific depth should always visit the same number of leaf nodes.
//! For example, Perft(6) from STARTPOS should yield 119060324.
//! \param[in] depth How many half-moves (plies) to search
//! \return The number of leaf nodes visited at \p depth
//--------------------------------------------------------------------------
//uint64_t Engine::MyPerft(const int depth) 
uint64_t Engine::MyPerft(const int ) 
{

#ifdef DEBUG
    cornedpanda::logStream << "Engine::MyPerft <-\n";
#endif

    if (not IsInitialized())
    {
        cornedpanda::Output() << "Engine::MyPerft -- Engine not initialized !!!";
        return 0;
    }

    // TODO Engine::MyPerft

#ifdef DEBUG
    cornedpanda::logStream << "Engine::MyPerft ->\n";
#endif

    return 0;
}

//--------------------------------------------------------------------------
//! \brief Execute search on current position to find best move
//! \param[in] depth Maximum number of half-moves (plies) to search
//! \param[in] movestogo Number of moves remaining until next time control
//! \param[in] movetime Maximum milliseconds to spend on this move
//! \param[in] wtime Milliseconds remaining on white's clock
//! \param[in] winc White increment per move in milliseconds
//! \param[in] btime Milliseconds remaining on black's clock
//! \param[in] binc Black increment per move in milliseconds
//! \param[out] ponder If not NULL set to the move engine should ponder next
//! \return Best move in coordinate notation (e.g. "e2e4", "g8f6", "e7f8q")
//--------------------------------------------------------------------------
/*
std::string Engine::MyGo(const int depth,
        const int movestogo,
        const uint64_t movetime,
        const uint64_t wtime, 
        const uint64_t winc,
        const uint64_t btime, 
        const uint64_t binc,
        std::string* ponder) 
*/
std::string Engine::MyGo(const int, const int, const uint64_t, const uint64_t, const uint64_t, const uint64_t, const uint64_t, std::string*) 
{
#ifdef DEBUG
    cornedpanda::logStream << "Engine::MyGo <-\n";
#endif

    if (not IsInitialized())
    {
        cornedpanda::Output() << "Engine::MyGo -- Engine not initialized !!!";
        return std::string();
    }

    cornedpanda::Player * player = _iterPlayer->second;

    uint64_t t0 = senjo::Now();
    player->initBestMove();
    while (not (player->isBestMoveOk() or TimeoutOccurred() or StopRequested()))
        player->iterBestMove();
    std::string move = player->getBestMove();
    uint64_t t1 = senjo::Now();

    // TODO mode debug uci
    player->printStatsBestMove();

    if (UseTimer())
    {
        // TODO utilisation du timer senjo
        //float totalUsedTime = senjo::ToSeconds(GetStopTime()- GetStartTime());
        float totalUsedTime = senjo::ToSeconds(t1-t0);
        cornedpanda::Output() << "Engine::MyGo : totalUsedTime = " << totalUsedTime;
#ifdef DEBUG
        cornedpanda::logStream << "Engine::MyGo : totalUsedTime = " << totalUsedTime << '\n';
#endif
    }

    cornedpanda::Output() << "Engine::MyGo -> " << move;
#ifdef DEBUG
    cornedpanda::logStream << "Engine::MyGo -> " << move << '\n';
#endif
    return move;
}

