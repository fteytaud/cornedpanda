
#include "senjo/UCIAdapter.h"
#include "senjo/Output.h"

#include "Engine.hpp"
#include "Log.hpp"

int main(int, char**)
{

#ifdef DEBUG
    cornedpanda::initLogStream("cornedpanda.log");
#endif

    Engine engine;
    senjo::UCIAdapter adapter;

    if (!adapter.Start(engine)) 
    {
        senjo::Output() << "Unable to start UCIAdapter";
        return 1;
    }

    char sbuf[16384];
    memset(sbuf, 0, sizeof(sbuf));

    while (fgets(sbuf, sizeof(sbuf), stdin)) 
    {
        char* cmd = sbuf;
        senjo::NormalizeString(cmd);

        if (!adapter.DoCommand(cmd)) 
        {
            break;
        }
    }

    return 0;
}

