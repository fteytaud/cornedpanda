// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef _BOARD_HPP_
#define _BOARD_HPP_

#include <array>
#include <fstream>
#include <functional>
#include <iostream>
#include <string>
#include <vector>
#include <cstdint>
#include <map>

namespace cornedpanda
{
    enum Piece
    {
        PIECE_P, PIECE_R, PIECE_N, PIECE_B, PIECE_Q, PIECE_K, // white pieces
        PIECE_p, PIECE_r, PIECE_n, PIECE_b, PIECE_q, PIECE_k, // black pieces
        PIECE_0                                               // no piece
    };

    enum GameState 
    {
        GAME_STATE_RUNNING, GAME_STATE_WHITE_WON, GAME_STATE_BLACK_WON, GAME_STATE_DRAW
    };

    const std::vector<std::string> GameStateStrings = {"running", "white won", "black won", "draw"};

    // a player may change _promotion but should not change other attributes
    struct Move
    {
        uint64_t _from;       // start square
        uint64_t _to;         // end square
        uint64_t _epTarget;   // en passant target square
        Piece _piece;         // moving piece
        Piece _promotion;     // promote to that piece
        Piece _capture;       // is the move doing a capture
        bool _check;          // does the move put in check

        bool operator==(const Move &m1) const;
    };

    // bitboard 
    class Board
    {
            static bool _isInit;

        protected:

            // occupied squares for each piece
            uint64_t _squares[12]; 

            bool _isWhiteToMove;

            // king/rook positions valid for castling
            uint64_t _castlings;

            uint64_t _enPassantTarget;

            // 50 moves rule
            int _halfmoveClock; 

            int _fullmoveCounter;

            uint64_t _occupiedSquares, _emptySquares, _whiteSquares, _blackSquares;

            bool _isInCheck; 

            std::vector<Move> _possibleMoves;

            // number of each piece on the board including empty squares
            // meaning sum(_nbMaterial) == 64
            std::array<int,13> _nbMaterial;

            bool _threefoldRepetition;
            std::map<uint64_t, int> _repetitions;

        public:

            // call these once at the beginning of the application
            static void init();
            static bool isInit();

            Board();

            // TODO add something like: virtual void setParameter(const std::string & param, int value)
            std::map<std::string, int> _parameters;

            GameState getGameState() const;
            bool isWhiteToMove() const;
            const std::vector<Move> & getMoves() const;
            void makeMove(const Move & move);  // updates moves

            static std::string moveToString(const Move & move);
            const char * stringToMove(const char * str, Move & move) const;
            const char * setFen(const char * fen);  // updates moves
            std::string getFen() const;

            static void printUint64Board(std::ostream & os, uint64_t occupancy, const std::string & prefixString);

        protected:

            void makeMovePartialy(const Move & move);  // does not update _possibleMoves
            void updateSquares();
            void updateMoves();
            void updateInCheck();
            void updateThreefoldRepetition();

            static int countNbOnes(uint64_t occupancy);

            static std::string indexToFenString(uint64_t idx);
            static const char * fenStringToIndex(const char * str, uint64_t & idx);

            // a push/attack can be illegal (can put its king in check)
            uint64_t getWhiteAttacks() const;
            uint64_t getBlackAttacks() const;

            uint64_t getWhitePawnPushes(uint64_t from) const;
            uint64_t getBlackPawnPushes(uint64_t from) const;
            uint64_t getWhitePawnAttacks(uint64_t from) const;
            uint64_t getBlackPawnAttacks(uint64_t from) const;
            uint64_t getWhiteRookAttacks(uint64_t from) const;
            uint64_t getBlackRookAttacks(uint64_t from) const;
            uint64_t getWhiteKnightAttacks(uint64_t from) const;
            uint64_t getBlackKnightAttacks(uint64_t from) const;
            uint64_t getWhiteBishopAttacks(uint64_t from) const;
            uint64_t getBlackBishopAttacks(uint64_t from) const;
            uint64_t getWhiteQueenAttacks(uint64_t from) const;
            uint64_t getBlackQueenAttacks(uint64_t from) const;
            uint64_t getWhiteKingAttacks(uint64_t from) const;
            uint64_t getBlackKingAttacks(uint64_t from) const;

            void pushWhiteMoves(uint64_t from, uint64_t destinations, Piece piece);
            void pushBlackMoves(uint64_t from, uint64_t destinations, Piece piece);

            void getWhitePawnMoves(uint64_t from) ;
            void getBlackPawnMoves(uint64_t from) ;
            void getWhiteRookMoves(uint64_t from) ;
            void getBlackRookMoves(uint64_t from) ;
            void getWhiteKnightMoves(uint64_t from) ;
            void getBlackKnightMoves(uint64_t from) ;
            void getWhiteBishopMoves(uint64_t from) ;
            void getBlackBishopMoves(uint64_t from) ;
            void getWhiteQueenMoves(uint64_t from) ;
            void getBlackQueenMoves(uint64_t from) ;
            void getWhiteKingMoves(uint64_t from) ;
            void getBlackKingMoves(uint64_t from) ;

            const std::array<std::function<void(Board &, uint64_t)>, 12> movesFunctions = 
            {{
                &cornedpanda::Board::getWhitePawnMoves,
                &cornedpanda::Board::getWhiteRookMoves,
                &cornedpanda::Board::getWhiteKnightMoves,
                &cornedpanda::Board::getWhiteBishopMoves,
                &cornedpanda::Board::getWhiteQueenMoves,
                &cornedpanda::Board::getWhiteKingMoves,
                &cornedpanda::Board::getBlackPawnMoves,
                &cornedpanda::Board::getBlackRookMoves,
                &cornedpanda::Board::getBlackKnightMoves,
                &cornedpanda::Board::getBlackBishopMoves,
                &cornedpanda::Board::getBlackQueenMoves,
                &cornedpanda::Board::getBlackKingMoves
             }};

            const std::array<std::function<uint64_t(const Board &, uint64_t)>, 12> attacksFunctions =
            {{
                &cornedpanda::Board::getWhitePawnAttacks,
                &cornedpanda::Board::getWhiteRookAttacks,
                &cornedpanda::Board::getWhiteKnightAttacks,
                &cornedpanda::Board::getWhiteBishopAttacks,
                &cornedpanda::Board::getWhiteQueenAttacks,
                &cornedpanda::Board::getWhiteKingAttacks,
                &cornedpanda::Board::getBlackPawnAttacks,
                &cornedpanda::Board::getBlackRookAttacks,
                &cornedpanda::Board::getBlackKnightAttacks,
                &cornedpanda::Board::getBlackBishopAttacks,
                &cornedpanda::Board::getBlackQueenAttacks,
                &cornedpanda::Board::getBlackKingAttacks
             }};

            friend std::ostream & operator<<(std::ostream & os, const Board & board);
    };

    std::ostream & operator<<(std::ostream & os, const Board & board);

    static const uint64_t RANK_8 = 0xFF00000000000000;
    static const uint64_t RANK_7 = 0x00FF000000000000;
    static const uint64_t RANK_6 = 0x0000FF0000000000;
    static const uint64_t RANK_5 = 0x000000FF00000000;
    static const uint64_t RANK_4 = 0x00000000FF000000;
    static const uint64_t RANK_3 = 0x0000000000FF0000;
    static const uint64_t RANK_2 = 0x000000000000FF00;
    static const uint64_t RANK_1 = 0x00000000000000FF;

    static const uint64_t FILE_A = 0x0101010101010101;
    static const uint64_t FILE_B = 0x0202020202020202;
    static const uint64_t FILE_C = 0x0404040404040404;
    static const uint64_t FILE_D = 0x0808080808080808;
    static const uint64_t FILE_E = 0x1010101010101010;
    static const uint64_t FILE_F = 0x2020202020202020;
    static const uint64_t FILE_G = 0x4040404040404040;
    static const uint64_t FILE_H = 0x8080808080808080;

    static const uint64_t WHITE_TERRITORY = RANK_1 | RANK_2 | RANK_3 | RANK_4;
    static const uint64_t BLACK_TERRITORY = RANK_8 | RANK_7 | RANK_6 | RANK_5;

    static const uint64_t KING_SIDE = FILE_F | FILE_G | FILE_H;
    static const uint64_t QUEEN_SIDE = FILE_A | FILE_B | FILE_C;

    static const uint64_t CENTER = 1ULL << 27 | 1ULL << 28 | 1ULL << 35 | 1ULL << 36;
    static const uint64_t LARGE_CENTER = 1ULL << 18 | 1ULL << 19 | 1ULL << 20 | 1ULL << 21 | 1ULL << 26 
        | 1ULL << 29 | 1ULL << 34 | 1ULL << 37 | 1ULL << 42 | 1ULL << 43 | 1ULL << 44 | 1ULL << 45;
    static const uint64_t BORDERS = 1ULL | 1ULL << 1 | 1ULL << 2 | 1ULL << 3 | 1ULL << 4 | 1ULL << 5 
        | 1ULL << 6 | 1ULL << 7 | 1ULL << 8 | 1ULL << 15 | 1ULL << 16 | 1ULL << 23 | 1ULL << 24 
        | 1ULL << 31 | 1ULL << 31 | 1ULL << 32 | 1ULL << 39 | 1ULL << 40 | 1ULL << 47 | 1ULL << 48 | 1ULL << 55
        | 1ULL << 56 | 1ULL << 57 | 1ULL << 58 | 1ULL << 59 | 1ULL << 60 | 1ULL << 61 | 1ULL << 62 | 1ULL << 63;
    static const uint64_t LARGE_BORDERS = 1ULL << 9 | 1ULL << 10 | 1ULL << 11 | 1ULL << 12 | 1ULL << 13 
        | 1ULL << 14 | 1ULL << 17 | 1ULL << 22 | 1ULL << 25 | 1ULL << 30 | 1ULL << 33 | 1ULL << 38 | 1ULL << 41 
        | 1ULL << 46 | 1ULL << 49 | 1ULL << 50 | 1ULL << 51 | 1ULL << 52 | 1ULL << 53 | 1ULL << 54;

    static const uint64_t DARK_SQUARES = 1ULL | 1ULL << 2 | 1ULL << 4 | 1ULL << 6
        | 1ULL << 8 | 1ULL << 10 | 1ULL << 12 | 1ULL << 14
        | 1ULL << 16 | 1ULL << 18 | 1ULL << 20 | 1ULL << 22
        | 1ULL << 24 | 1ULL << 26 | 1ULL << 28 | 1ULL << 30
        | 1ULL << 32 | 1ULL << 34 | 1ULL << 36 | 1ULL << 38
        | 1ULL << 40 | 1ULL << 42 | 1ULL << 44 | 1ULL << 46
        | 1ULL << 48 | 1ULL << 50 | 1ULL << 52 | 1ULL << 54
        | 1ULL << 56 | 1ULL << 58 | 1ULL << 60 | 1ULL << 62;

    static const uint64_t LIGHT_SQUARES = 1ULL << 1 | 1ULL << 3 | 1ULL << 5 | 1ULL << 7
        | 1ULL << 9 | 1ULL << 11 | 1ULL << 13 | 1ULL << 15
        | 1ULL << 17 | 1ULL << 19 | 1ULL << 21 | 1ULL << 23
        | 1ULL << 25 | 1ULL << 27 | 1ULL << 29 | 1ULL << 31
        | 1ULL << 33 | 1ULL << 35 | 1ULL << 37 | 1ULL << 39
        | 1ULL << 41 | 1ULL << 43 | 1ULL << 45 | 1ULL << 47
        | 1ULL << 49 | 1ULL << 51 | 1ULL << 53 | 1ULL << 55
        | 1ULL << 57 | 1ULL << 59 | 1ULL << 61 | 1ULL << 63;
}

#endif

