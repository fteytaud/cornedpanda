// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef _PLAYERID_HPP_
#define _PLAYERID_HPP_

#include "Player.hpp"
#include "BoardEval.hpp"

// TODO iterative deepening :
// move ordering / best-first (transposition table -> std::unordered_map ?)
// aspiration window
// alpha-beta (negamax)
// quiescence search
// delta pruning

namespace cornedpanda
{

    class PlayerID : public Player
    {
        int _alpha_ID;
        int _beta_ID;
        int _depth_ID;

        public:
            PlayerID();

            void initBestMove();
            void iterBestMove();
            std::string getBestMove();
            void printStatsBestMove();

        private:
            void alphabeta(const BoardEval & refBoard, int currentDepth, int maxDepth) ;
    };

}

#endif

