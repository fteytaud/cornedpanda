// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef _BOARD_TEST_HPP_
#define _BOARD_TEST_HPP_

#include <cxxtest/TestSuite.h>
#include <cxxtest/GlobalFixture.h>

#include <cassert>

#include "Board.hpp"
#include "Log.hpp"
#include "Fasttest.hpp"

class Board_Fixture : public CxxTest::GlobalFixture
{
    public:

        bool setUpWorld() 
        {
            cornedpanda::Board::init();
            cornedpanda::logStream.open("Board_test.log");
            cornedpanda::logStream << "---- begin Board_test ----\n";
            return true;
        }

        bool tearDownWorld() 
        {
            cornedpanda::logStream << "---- end Board_test ----\n";
            cornedpanda::logStream.close();
            return true;
        }
};

static Board_Fixture boardFixture;

class Board_test :  public CxxTest::TestSuite, public cornedpanda::Board 
{

    public:

        //////////////////////////////////////////////////
        // Constructor
        //////////////////////////////////////////////////

        void test_Board_1()
        {
            Board b;
            TS_ASSERT_EQUALS(b.getGameState(), cornedpanda::GAME_STATE_RUNNING);
        }

        //////////////////////////////////////////////////
        // fenStringToIndex
        //////////////////////////////////////////////////

        void test_fenStringToIndex_1() 
        {
            char str [] = "a1";
            uint64_t idx;
            const char * p = cornedpanda::Board::fenStringToIndex(str, idx);
            TS_ASSERT( p != nullptr );
            TS_ASSERT_EQUALS( idx, 0x1ULL );
        }

        void test_fenStringToIndex_2() 
        {
            char str [] = "b1";
            uint64_t idx;
            const char * p = cornedpanda::Board::fenStringToIndex(str, idx);
            TS_ASSERT( p != nullptr );
            TS_ASSERT_EQUALS( idx, 0x2ULL );
        }

        void test_fenStringToIndex_3() 
        {
            char str [] = "a2";
            uint64_t idx;
            const char * p = cornedpanda::Board::fenStringToIndex(str, idx);
            TS_ASSERT( p != nullptr );
            TS_ASSERT_EQUALS( idx, 0x100ULL );
        }

        void test_fenStringToIndex_4() 
        {
            char str [] = "h8";
            uint64_t idx;
            const char * p = cornedpanda::Board::fenStringToIndex(str, idx);
            TS_ASSERT( p != nullptr );
            TS_ASSERT_EQUALS( idx, 0x8000000000000000ULL);
        }

        void test_fenStringToIndex_5() 
        {
            char str [] = "b0";
            uint64_t idx;
            const char * p = cornedpanda::Board::fenStringToIndex(str, idx);
            TS_ASSERT( p == nullptr );
        }

        void test_fenStringToIndex_6() 
        {
            char str [] = "-";
            uint64_t idx;
            const char * p = cornedpanda::Board::fenStringToIndex(str, idx);
            TS_ASSERT( p != nullptr );
            TS_ASSERT_EQUALS( idx, 0x0ULL);
        }

        //////////////////////////////////////////////////
        // indexToFenString
        //////////////////////////////////////////////////

        void test_indexToFenString_1() 
        {
            uint64_t idx = 0x1ULL;
            std::string s = cornedpanda::Board::indexToFenString(idx);
            TS_ASSERT_EQUALS( s, std::string("a1"));
        }

        void test_indexToFenString_2() 
        {
            uint64_t idx = 0x2ULL;
            std::string s = cornedpanda::Board::indexToFenString(idx);
            TS_ASSERT_EQUALS( s, std::string("b1"));
        }

        void test_indexToFenString_3() 
        {
            uint64_t idx = 0x100ULL;
            std::string s = cornedpanda::Board::indexToFenString(idx);
            TS_ASSERT_EQUALS( s, std::string("a2"));
        }

        void test_indexToFenString_4() 
        {
            uint64_t idx = 0x8000000000000000ULL;
            std::string s = cornedpanda::Board::indexToFenString(idx);
            TS_ASSERT_EQUALS( s, std::string("h8"));
        }

        void test_indexToFenString_5() 
        {
            uint64_t idx = 0x0ULL;
            std::string s = cornedpanda::Board::indexToFenString(idx);
            TS_ASSERT_EQUALS( s, std::string("-"));
        }


        //////////////////////////////////////////////////
        // setFen
        //////////////////////////////////////////////////

        void test_setFen_1() 
        {
            Board_test b;
            char fen [] = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 3 12 ";
            const char * p = b.setFen(fen);

            TS_ASSERT( p != 0 );

            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_P], 0x000000000000FF00ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_R], 0x0000000000000081ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_N], 0x0000000000000042ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_B], 0x0000000000000024ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_Q], 0x0000000000000008ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_K], 0x0000000000000010ULL);

            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_p], 0x00FF000000000000ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_r], 0x8100000000000000ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_n], 0x4200000000000000ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_b], 0x2400000000000000ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_q], 0x0800000000000000ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_k], 0x1000000000000000ULL);

            TS_ASSERT_EQUALS( b._isWhiteToMove, true );

            TS_ASSERT_EQUALS( b._castlings, 0x9100000000000091ULL);

            TS_ASSERT_EQUALS( b._enPassantTarget, 0x0ULL );
            TS_ASSERT_EQUALS( b._halfmoveClock, 3 );
            TS_ASSERT_EQUALS( b._fullmoveCounter, 12 );
        }

        void test_setFen_2() 
        {
            Board_test b;
            char fen [] = "rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1";
            const char * p = b.setFen(fen);

            TS_ASSERT( p != 0 );

            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_P], 0x000000001000EF00ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_R], 0x0000000000000081ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_N], 0x0000000000000042ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_B], 0x0000000000000024ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_Q], 0x0000000000000008ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_K], 0x0000000000000010ULL);

            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_p], 0x00FF000000000000ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_r], 0x8100000000000000ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_n], 0x4200000000000000ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_b], 0x2400000000000000ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_q], 0x0800000000000000ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_k], 0x1000000000000000ULL);

            TS_ASSERT_EQUALS( b._isWhiteToMove, false );

            TS_ASSERT_EQUALS( b._castlings, 0x9100000000000091ULL);

            TS_ASSERT_EQUALS( b._enPassantTarget, 1ULL << 20 )
                TS_ASSERT_EQUALS( b._halfmoveClock, 0 );
            TS_ASSERT_EQUALS( b._fullmoveCounter, 1 );
        }

        void test_setFen_3() 
        {
            Board_test b;
            char fen [] = "rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w - c6 0 2";
            const char * p = b.setFen(fen);

            TS_ASSERT( p != 0 );

            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_P], 0x000000001000EF00ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_R], 0x0000000000000081ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_N], 0x0000000000000042ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_B], 0x0000000000000024ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_Q], 0x0000000000000008ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_K], 0x0000000000000010ULL);

            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_p], 0x00FB000400000000ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_r], 0x8100000000000000ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_n], 0x4200000000000000ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_b], 0x2400000000000000ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_q], 0x0800000000000000ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_k], 0x1000000000000000ULL);

            TS_ASSERT_EQUALS( b._isWhiteToMove, true );

            TS_ASSERT_EQUALS( b._castlings, 0x0000000000000000ULL);

            TS_ASSERT_EQUALS( b._enPassantTarget, 1ULL << 42 );
            TS_ASSERT_EQUALS( b._halfmoveClock, 0 );
            TS_ASSERT_EQUALS( b._fullmoveCounter, 2 );
        }

        void test_setFen_4() 
        {
            Board_test b;
            char fen [] = "rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b Kk - 1 2 ";
            const char * p = b.setFen(fen);

            TS_ASSERT( p != 0 );

            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_P], 0x000000001000EF00ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_R], 0x0000000000000081ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_N], 0x0000000000200002ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_B], 0x0000000000000024ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_Q], 0x0000000000000008ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_K], 0x0000000000000010ULL);

            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_p], 0x00FB000400000000ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_r], 0x8100000000000000ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_n], 0x4200000000000000ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_b], 0x2400000000000000ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_q], 0x0800000000000000ULL);
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_k], 0x1000000000000000ULL);

            TS_ASSERT_EQUALS( b._isWhiteToMove, false );

            TS_ASSERT_EQUALS( b._castlings, 0x9000000000000090ULL);

            TS_ASSERT_EQUALS( b._enPassantTarget, 0x0ULL );
            TS_ASSERT_EQUALS( b._halfmoveClock, 1 );
            TS_ASSERT_EQUALS( b._fullmoveCounter, 2 );
        }

        //////////////////////////////////////////////////
        // getFen
        //////////////////////////////////////////////////

        void test_getFen_1() 
        {
            Board_test b;

            b._squares[cornedpanda::PIECE_P] = 0x000000000000FF00ULL;
            b._squares[cornedpanda::PIECE_R] = 0x0000000000000081ULL;
            b._squares[cornedpanda::PIECE_N] = 0x0000000000000042ULL;
            b._squares[cornedpanda::PIECE_B] = 0x0000000000000024ULL;
            b._squares[cornedpanda::PIECE_Q] = 0x0000000000000008ULL;
            b._squares[cornedpanda::PIECE_K] = 0x0000000000000010ULL;

            b._squares[cornedpanda::PIECE_p] = 0x00FF000000000000ULL;
            b._squares[cornedpanda::PIECE_r] = 0x8100000000000000ULL;
            b._squares[cornedpanda::PIECE_n] = 0x4200000000000000ULL;
            b._squares[cornedpanda::PIECE_b] = 0x2400000000000000ULL;
            b._squares[cornedpanda::PIECE_q] = 0x0800000000000000ULL;
            b._squares[cornedpanda::PIECE_k] = 0x1000000000000000ULL;

            b._isWhiteToMove = true ;

            TS_ASSERT_EQUALS( b._castlings, 0x9100000000000091ULL);

            b._enPassantTarget = 0x0ULL ;
            b._halfmoveClock = 3 ;
            b._fullmoveCounter = 12 ;

            b.updateSquares();
            b.updateMoves();

            std::string s0 = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 3 12 ";
            std::string s1 = b.getFen();

            TS_ASSERT_EQUALS( s1, s0);
        }

        void test_getFen_2() 
        {
            Board_test b;

            b._squares[cornedpanda::PIECE_P] = 0x000000001000EF00ULL;
            b._squares[cornedpanda::PIECE_R] = 0x0000000000000081ULL;
            b._squares[cornedpanda::PIECE_N] = 0x0000000000000042ULL;
            b._squares[cornedpanda::PIECE_B] = 0x0000000000000024ULL;
            b._squares[cornedpanda::PIECE_Q] = 0x0000000000000008ULL;
            b._squares[cornedpanda::PIECE_K] = 0x0000000000000010ULL;

            b._squares[cornedpanda::PIECE_p] = 0x00FF000000000000ULL;
            b._squares[cornedpanda::PIECE_r] = 0x8100000000000000ULL;
            b._squares[cornedpanda::PIECE_n] = 0x4200000000000000ULL;
            b._squares[cornedpanda::PIECE_b] = 0x2400000000000000ULL;
            b._squares[cornedpanda::PIECE_q] = 0x0800000000000000ULL;
            b._squares[cornedpanda::PIECE_k] = 0x1000000000000000ULL;

            b._isWhiteToMove = false ;

            TS_ASSERT_EQUALS( b._castlings, 0x9100000000000091ULL);

            b._enPassantTarget = 1ULL << 20 ;
            b._halfmoveClock = 0 ;
            b._fullmoveCounter = 1 ;

            b.updateSquares();
            b.updateMoves();

            std::string s0 = "rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1 ";
            std::string s1 = b.getFen();

            TS_ASSERT_EQUALS( s1, s0);
        }

        void test_getFen_3() 
        {
            Board_test b;

            b._squares[cornedpanda::PIECE_P] = 0x000000001000EF00ULL;
            b._squares[cornedpanda::PIECE_R] = 0x0000000000000081ULL;
            b._squares[cornedpanda::PIECE_N] = 0x0000000000000042ULL;
            b._squares[cornedpanda::PIECE_B] = 0x0000000000000024ULL;
            b._squares[cornedpanda::PIECE_Q] = 0x0000000000000008ULL;
            b._squares[cornedpanda::PIECE_K] = 0x0000000000000010ULL;

            b._squares[cornedpanda::PIECE_p] = 0x00FB000400000000ULL;
            b._squares[cornedpanda::PIECE_r] = 0x8100000000000000ULL;
            b._squares[cornedpanda::PIECE_n] = 0x4200000000000000ULL;
            b._squares[cornedpanda::PIECE_b] = 0x2400000000000000ULL;
            b._squares[cornedpanda::PIECE_q] = 0x0800000000000000ULL;
            b._squares[cornedpanda::PIECE_k] = 0x1000000000000000ULL;

            b._isWhiteToMove = true ;
            b._castlings = 0ULL;
            b._enPassantTarget = 1ULL << 42 ;
            b._halfmoveClock = 0 ;
            b._fullmoveCounter = 2 ;

            b.updateSquares();
            b.updateMoves();

            std::string s0 = "rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w - c6 0 2 ";
            std::string s1 = b.getFen();

            TS_ASSERT_EQUALS( s1, s0);
        }

        void test_getFen_4() 
        {
            Board_test b;

            b._squares[cornedpanda::PIECE_P] = 0x000000001000EF00ULL;
            b._squares[cornedpanda::PIECE_R] = 0x0000000000000081ULL;
            b._squares[cornedpanda::PIECE_N] = 0x0000000000200002ULL;
            b._squares[cornedpanda::PIECE_B] = 0x0000000000000024ULL;
            b._squares[cornedpanda::PIECE_Q] = 0x0000000000000008ULL;
            b._squares[cornedpanda::PIECE_K] = 0x0000000000000010ULL;

            b._squares[cornedpanda::PIECE_p] = 0x00FB000400000000ULL;
            b._squares[cornedpanda::PIECE_r] = 0x8100000000000000ULL;
            b._squares[cornedpanda::PIECE_n] = 0x4200000000000000ULL;
            b._squares[cornedpanda::PIECE_b] = 0x2400000000000000ULL;
            b._squares[cornedpanda::PIECE_q] = 0x0800000000000000ULL;
            b._squares[cornedpanda::PIECE_k] = 0x1000000000000000ULL;

            b._isWhiteToMove = false;
            b._castlings = 0x9000000000000090ULL;
            b._enPassantTarget = 0ULL ;
            b._halfmoveClock = 1 ;
            b._fullmoveCounter = 2 ;

            b.updateSquares();
            b.updateMoves();

            std::string s0 = "rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b Kk - 1 2 ";
            std::string s1 = b.getFen();

            TS_ASSERT_EQUALS( s1, s0);
        }


        //////////////////////////////////////////////////
        // isWhiteToMove
        //////////////////////////////////////////////////

        void test_isWhiteToMove_1() 
        {
            Board_test b;
            TS_ASSERT_EQUALS( b.isWhiteToMove(), true);
        }

        void test_isWhiteToMove_2() 
        {
            Board_test b;
            std::string str = "a2a3";
            cornedpanda::Move m;
            b.stringToMove(str.c_str(), m);
            b.makeMove(m);
            TS_ASSERT_EQUALS( b.isWhiteToMove(), false);
            TS_ASSERT( (b._emptySquares & m._from) != 0ULL);
            TS_ASSERT( (b._occupiedSquares & m._to) != 0ULL);
        }

        void test_isWhiteToMove_3() 
        {
            Board_test b;
            std::string str = "a2a3 b7b6";
            const char * p = str.c_str();
            cornedpanda::Move m;
            p = b.stringToMove(p, m);
            b.makeMove(m);
            p = b.stringToMove(p, m);
            b.makeMove(m);
            TS_ASSERT_EQUALS( b.isWhiteToMove(), true);
            TS_ASSERT( (b._emptySquares & m._from) != 0ULL);
            TS_ASSERT( (b._occupiedSquares & m._to) != 0ULL);
        }

        //////////////////////////////////////////////////
        // moveToString
        //////////////////////////////////////////////////

        void test_moveToString_1() 
        {
            Board_test b;
            cornedpanda::Move m;
            m._from = 1ULL << 50;
            m._to = 1ULL << 58;
            m._promotion = cornedpanda::PIECE_Q;
            std::string s = b.moveToString(m);
            TS_ASSERT_EQUALS( s, std::string("c7c8q"));
        }

        void test_moveToString_2() 
        {
            Board_test b;
            cornedpanda::Move m;
            m._from = 1ULL << 50;
            m._to = 1ULL << 58;
            m._promotion = cornedpanda::PIECE_N;
            std::string s = b.moveToString(m);
            TS_ASSERT_EQUALS( s, std::string("c7c8n"));
        }

        //////////////////////////////////////////////////
        // stringToMove
        //////////////////////////////////////////////////

        void test_stringToMove_1() 
        {
            // todo
            Board_test b;
            b.setFen("rn1qkbnr/ppPppppp/8/8/8/8/PPPPPPpP/RNBQKB1R w - - 3 12 ");
            std::string s = "c7c8q";
            cornedpanda::Move m;
            b.stringToMove(s.c_str(), m);
            TS_ASSERT_EQUALS( m._from, 1ULL << 50 );
            TS_ASSERT_EQUALS( m._to, 1ULL << 58 );
            TS_ASSERT_EQUALS( m._promotion, cornedpanda::PIECE_Q);
        }

        void test_stringToMove_2() 
        {
            Board_test b;
            b.setFen("rnbqkbnr/ppPppppp/8/8/8/8/PPPPPPpP/RNBQKB1R b - - 3 12 ");
            std::string s = "g2g1n";
            cornedpanda::Move m;
            b.stringToMove(s.c_str(), m);
            TS_ASSERT_EQUALS( m._from, 1ULL << 14 );
            TS_ASSERT_EQUALS( m._to, 1ULL << 6 );
            TS_ASSERT_EQUALS( m._promotion, cornedpanda::PIECE_n);
        }

        //////////////////////////////////////////////////
        // getWhiteMoves
        //////////////////////////////////////////////////

        void test_getWhiteMoves_1() 
        {
            Board_test b;
            b.setFen("K6Q/5k2/p7/8/N5PP/8/5bb1/1r6 w - - 1 20 ");

            cornedpanda::logStream << "---- position apres getWhiteMoves_1 ----\n";
            cornedpanda::logStream << b;
            cornedpanda::logStream << "---- taille des coups : " << b.getMoves().size() << std::endl;

            TS_ASSERT_EQUALS( 0U, b.getMoves().size());

        }

        void test_getWhiteMoves_2() 
        {
            Board_test b;
            b.setFen("7k/8/8/8/8/2n5/r7/K7 w - - 1 20 ");

            cornedpanda::logStream << "---- position apres whiteMoves_2 ----\n";
            cornedpanda::logStream << b;
            cornedpanda::logStream << "---- nb legal moves : " << b.getMoves().size() << std::endl;

            TS_ASSERT_EQUALS( b.getMoves().size(), 0U );

        }

        //////////////////////////////////////////////////
        // getBlackMoves
        //////////////////////////////////////////////////

        void test_getBlackMoves_1() 
        {
            Board_test b;
            b.setFen("k1K5/8/1P6/8/8/8/8/8 b - - 1 20 ");

            cornedpanda::logStream << "---- position apres getBlackMoves_1 ----\n";
            cornedpanda::logStream << b;
            cornedpanda::logStream << "---- taille des coups : " << b.getMoves().size() << std::endl;

            TS_ASSERT_EQUALS( 0U, b.getMoves().size());

        }

        void test_getBlackMoves_2() 
        {
            Board_test b;
            b.setFen("2k4r/7p/2Q2p1b/3Np3/4P1p1/r2P4/5PPP/1R4K1 b - - 1 20 ");

            cornedpanda::logStream << "---- position apres getBlackMoves_2 ----\n";
            cornedpanda::logStream << b;
            cornedpanda::logStream << "---- taille des coups : " << b.getMoves().size() << std::endl;

            TS_ASSERT_EQUALS( 1U, b.getMoves().size());
        }

        //////////////////////////////////////////////////
        // threefold
        //////////////////////////////////////////////////

        void test_threefold_1()
        {
            Board_test b;
            const char moveString [] = "a2a3 a7a6 a1a2 a8a7 a2a1 a7a8 a1a2 a8a7 a2a1 a7a8 ";
            cornedpanda::makeMovesFromString(b, moveString);
            cornedpanda::logStream << "---- test_threefold_1 ----\n";
            cornedpanda::logStream << b;
            TS_ASSERT_EQUALS( b._threefoldRepetition, true );
        }

        void test_threefold_2()
        {
            Board_test b;
            const char moveString [] = "a2a3 a7a6 a1a2 a8a7 a2a1 a7a8 a1a2 a8a7 a2a1 ";
            cornedpanda::makeMovesFromString(b, moveString);
            cornedpanda::logStream << "---- test_threefold_2 ----\n";
            cornedpanda::logStream << b;
            TS_ASSERT_EQUALS( b._threefoldRepetition, false);
        }

        //////////////////////////////////////////////////
        // castling
        //////////////////////////////////////////////////

        void test_castling_K_1()
        {
            Board_test b;
            const char moveString [] = "g1h3 a7a6 g2g3 b7b6 f1g2 c7c6";
            cornedpanda::makeMovesFromString(b, moveString);
            cornedpanda::Move m;
            bool found = cornedpanda::findMove(b, "e1g1", m);
            cornedpanda::logStream << "---- test_castling_K_1 ----\n";
            cornedpanda::logStream << b;
            TS_ASSERT( found );
        }

        void test_castling_K_2()
        {
            Board_test b;
            const char moveString [] = "g1h3 a7a6 g2g3 b7b6 f1g2 c7c6 e1g1";
            cornedpanda::makeMovesFromString(b, moveString);
            cornedpanda::logStream << "\n---- test_castling_K_2 ----\n";
            cornedpanda::logStream << b;
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_K], 64ULL );
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_R], 33ULL );
        }

        void test_castling_Q_1()
        {
            Board_test b;
            const char moveString [] = "b1c3 b7b6 d2d4 c7c6 c1e3 d7d6 d1d2 e7e6";
            cornedpanda::makeMovesFromString(b, moveString);
            cornedpanda::Move m;
            bool found = cornedpanda::findMove(b, "e1c1", m);
            cornedpanda::logStream << "---- test_castling_Q_1 ----\n";
            cornedpanda::logStream << b;
            TS_ASSERT( found );
        }

        void test_castling_Q_2()
        {
            Board_test b;
            const char moveString [] = "b1c3 b7b6 d2d4 c7c6 c1e3 d7d6 d1d2 e7e6 e1c1";
            cornedpanda::makeMovesFromString(b, moveString);
            cornedpanda::logStream << "\n---- test_castling_Q_2 ----\n";
            cornedpanda::logStream << b;
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_K], 4ULL );
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_R], 136ULL );
        }

        //////////////////////////////////////////////////
        // enpassant
        //////////////////////////////////////////////////

        // teste la gestion d'une prise en passant (detection et capture)
        void test_enpassant_1()
        {
            Board_test b;
            const char moveString [] = "c2c4 b7b5 c4c5 d7d5 c5d6";
            cornedpanda::makeMovesFromString(b, moveString);
            cornedpanda::logStream << "\n---- test_enpassant_1 ----\n";
            cornedpanda::logStream << b;
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_P], 0x000008000000FB00ULL );
            TS_ASSERT_EQUALS( b._squares[cornedpanda::PIECE_p], 0x00F5000200000000ULL );
        }

        // teste la creation d'un move avec prise en passant 
        void test_enpassant_2()
        {
            Board_test b;
            const char moveString [] = "c2c4 b7b5 c4c5 d7d5";
            cornedpanda::makeMovesFromString(b, moveString);
            cornedpanda::Move m;
            bool found = cornedpanda::findMove(b, "c5d6", m);
            cornedpanda::logStream << "\n---- test_enpassant_2 ----\n";
            cornedpanda::logStream << b;
            TS_ASSERT( found );
        }

        // teste la mise a jour de epTarget
        void test_enpassant_3()
        {
            Board_test b;
            const char moveString [] = "c2c4 b7b5 c4c5";
            cornedpanda::makeMovesFromString(b, moveString);
            cornedpanda::Move m;
            bool found = cornedpanda::findMove(b, "d7d5", m);
            cornedpanda::logStream << "\n---- test_enpassant_3 ----\n";
            cornedpanda::logStream << b;
            TS_ASSERT( found );
            TS_ASSERT_EQUALS( m._epTarget, 0x0000080000000000ULL );
        }

        // teste la mise a jour de epTarget
        void test_enpassant_4()
        {
            Board_test b;
            const char moveString [] = "f2f4 h7h6 f4f5 h6h5 d2d4 h5h4 d4d5 e7e5";
            cornedpanda::makeMovesFromString(b, moveString);
            cornedpanda::logStream << "\n---- test_enpassant_4 ----\n";
            cornedpanda::logStream << b;
            cornedpanda::Move m;
            TS_ASSERT( cornedpanda::findMove(b, "d5e6", m) );
            TS_ASSERT( cornedpanda::findMove(b, "f5e6", m) );
        }

        //////////////////////////////////////////////////
        // getGameState
        //////////////////////////////////////////////////

        void test_getGameState_1() 
        {
            Board_test b;
            b.setFen("k6q/5K2/P7/8/n5pp/8/5BB1/1R6 b - - 1 20 ");

            cornedpanda::logStream << "---- position apres GameState_1 ----\n";
            cornedpanda::logStream << b;
            cornedpanda::logStream << "---- gameState : " << cornedpanda::GameStateStrings[b.getGameState()] << std::endl;

            TS_ASSERT_EQUALS( b.getGameState(), cornedpanda::GameState::GAME_STATE_WHITE_WON );

        }

        void test_getGameState_2() 
        {
            Board_test b;
            b.setFen("k1K5/8/1P6/8/8/8/8/8 b - - 1 20 ");

            cornedpanda::logStream << "---- position apres getGameState_2 ----\n";
            cornedpanda::logStream << b;
            cornedpanda::logStream << "---- gameState : " << cornedpanda::GameStateStrings[b.getGameState()] << std::endl;

            TS_ASSERT_EQUALS( cornedpanda::GameState::GAME_STATE_DRAW, b.getGameState());
        }


        //////////////////////////////////////////////////
        // promotion
        //////////////////////////////////////////////////

        void test_promotion_1()
        {
            Board_test b;
            const char moveString [] = "b2b4 b8c6 a2a3 g8f6 h2h3 a7a5 b4a5 f6e4 d2d3 e4f6 c1d2 d7d5 d2c3 d5d4 c3d2 d8d6 g2g3 e8d8 d2b4 c6b4 a3b4 d6b4 c2c3 b4b2 a1a4 d4c3 a4a2 b2a2 g3g4 c3c2 b1c3 c2d1q";
            cornedpanda::makeMovesFromString(b, moveString);
            cornedpanda::logStream << "---- test_promotion_1 ----\n";
            cornedpanda::logStream << b;
            cornedpanda::Move m;
            TS_ASSERT_EQUALS( true, cornedpanda::findMove(b, "c3d1", m) );
            TS_ASSERT_EQUALS( true, cornedpanda::findMove(b, "e1d1", m) );
            TS_ASSERT_EQUALS( 2, b.getMoves().size() );
        }

        //////////////////////////////////////////////////
        // test some moves
        //////////////////////////////////////////////////

        void test_somemoves_1()
        {
            Board_test b;
            const char moveString [] = "d2d4 g8f6 g1f3 c7c5 d4c5 d8a5 c1d2 a5c5 d2e3 c5a5 c2c3 b8c6 b2b4 a5h5 b1a3 f6e4 f3d2 e4d2 e3d2 h5d5 d2e3 d5e5 d1c2 d7d5 g2g3 e8d8 f1g2 g7g6 e1g1 c8e6 e3f4 e5f6 f1d1 d8c8 c2d2 a7a6 e2e4 d5e4 a3c4 b7b5 c4b6 c8b7 d2e3 a8a7 e3e4 f6c3 a1c1 e6d5 e4d5 c3f6 d5c6 f6c6 d1d7 b7b6 f4e3";
            cornedpanda::makeMovesFromString(b, moveString);
            cornedpanda::logStream << "\n---- test_somemoves_1 ----\n";
            cornedpanda::logStream << b;
            cornedpanda::Move m;
            TS_ASSERT( cornedpanda::findMove(b, "c6c5", m) );
            TS_ASSERT( b.getMoves().size() == 1U);
        }

};

#endif

