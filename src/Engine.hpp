// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef _ENGINE_HPP_
#define _ENGINE_HPP_

#include "senjo/EngineOption.h"
#include "senjo/ChessEngine.h"

#include "Player.hpp"

class Engine : public senjo::ChessEngine 
{
    public:

        Engine();
        ~Engine();

    private:

        typedef std::map<std::string, cornedpanda::Player *> PlayerMap_t;
        PlayerMap_t _players;
        PlayerMap_t::iterator _iterPlayer;

        //--------------------------------------------------------------------------
        //! \brief Get the engine name
        //! \return The engine name
        //--------------------------------------------------------------------------
        std::string GetEngineName() const; 

        //--------------------------------------------------------------------------
        //! \brief Get the engine version (e.g. "major.minor.build" e.g. "1.0.0")
        //! \return The engine version
        //--------------------------------------------------------------------------
        std::string GetEngineVersion() const;

        //--------------------------------------------------------------------------
        //! \brief Get the engine quthor name (e.g. "John Doe <jdoe@nowhere.com>")
        //! \return The engine author name
        //--------------------------------------------------------------------------
        std::string GetAuthorName() const;

        //--------------------------------------------------------------------------
        //! \brief Get options supported by the engine, and their current values
        //! \return A list of the engine's options and their current values
        //--------------------------------------------------------------------------
        std::list<senjo::EngineOption> GetOptions() const;

        //--------------------------------------------------------------------------
        //! \brief Set a particular option to a given value
        //! Option value may be empty, particularly if the option type is Button
        //! \param[in] optionName The option name
        //! \param[in] optionValue The new option value
        //! \return false if the option name or value is invalid
        //--------------------------------------------------------------------------
        bool SetEngineOption(const std::string& optionName,
                const std::string& optionValue);

        //--------------------------------------------------------------------------
        //! \brief Initialize the engine
        //--------------------------------------------------------------------------
        void Initialize();

        //--------------------------------------------------------------------------
        //! \brief Is the engine initialized?
        //! \return true if the engine is initialized
        //--------------------------------------------------------------------------
        bool IsInitialized() const;

        //--------------------------------------------------------------------------
        //! \brief Set the board position according to a given FEN string
        //! The engine should use Output() to report errors in the FEN string.
        //! Only use position info from the given FEN string, don't process any moves
        //! or other data present in the FEN string.
        //! \param[in] fen The FEN string
        //! \return Position in \p fen after last char needed to load the position,
        //!         NULL if the FEN string does not contain a valid position
        //--------------------------------------------------------------------------
        const char* SetPosition(const char* fen);

        //--------------------------------------------------------------------------
        //! \brief Execute a single move on the current position
        //! Determine whether the first word in the given string is a valid move
        //! and if it is apply the move to the current position.  Moves should be
        //! in coordinate notation (e.g. "e2e4", "g8f6", "e7f8q").
        //! \param[in] str A string containing one or more moves or anything else
        //! \return Position in \p str after the first move,
        //!         NULL if the first word isn't a valid move
        //--------------------------------------------------------------------------
        const char* MakeMove(const char* str);

        //--------------------------------------------------------------------------
        //! \brief Get a FEN string representation of the current board position
        //! \return A FEN string representation of the current board postiion
        //--------------------------------------------------------------------------
        std::string GetFEN() const;

        //--------------------------------------------------------------------------
        //! \brief Output a text representation of the current board position
        //--------------------------------------------------------------------------
        void PrintBoard() const;

        //--------------------------------------------------------------------------
        //! \brief Is it white to move in the current position?
        //! \return true if it is white to move in the current position
        //--------------------------------------------------------------------------
        bool WhiteToMove() const;

        //--------------------------------------------------------------------------
        //! \brief Clear any engine data that can persist between searches
        //! Examples of search data are the transposition table and killer moves.
        //--------------------------------------------------------------------------
        void ClearSearchData();

        //--------------------------------------------------------------------------
        //! \brief The last ponder move was played
        //! The Go() method may return a ponder move which is the expected response
        //! to the bestmove returned by Go().  If pondering is enabled the UCI
        //! adapter may tell the engine to ponder this move, e.g. start searching
        //! for a reply to the ponder move.  If, while the engine is pondering, the
        //! ponder move is played this method will be called.  In general the engine
        //! should make what it has learned from its pondering available for the next
        //! Go() call.
        //--------------------------------------------------------------------------
        void PonderHit();

        //--------------------------------------------------------------------------
        //! \brief Get statistics from the last (or current) search
        //! \param[out] depth The maximum depth achieved
        //! \param[out] seldepth The maximum selective depth achieved
        //! \param[out] nodes The number of nodes searched
        //! \param[out] qnodes The number of \p nodes that were in quiescence search
        //! \param[out] msecs The number of milliseconds spent searching
        //! \param[out] movenum The last/current move number searched
        //! \param[out] move The last/current move searched (e.g. "e2e4")
        //! \param[in] movelen The size of the \p move parameter
        //--------------------------------------------------------------------------
        void GetStats(int* depth,
                int* seldepth = NULL,
                uint64_t* nodes = NULL,
                uint64_t* qnodes = NULL,
                uint64_t* msecs = NULL,
                int* movenum = NULL,
                char* move = NULL,
                const size_t movelen = 0) const;

    protected:

        //--------------------------------------------------------------------------
        //! \brief Do performance test on the current position
        //! Perft is useful for determining the speed of you move generator.
        //! Perft is vital for validating whether your move generator is
        //! producing the correct moves.  In any given position, a perft search to
        //! a specific depth should always visit the same number of leaf nodes.
        //! For example, Perft(6) from STARTPOS should yield 119060324.
        //! \param[in] depth How many half-moves (plies) to search
        //! \return The number of leaf nodes visited at \p depth
        //--------------------------------------------------------------------------
        uint64_t MyPerft(const int depth);

        //--------------------------------------------------------------------------
        //! \brief Execute search on current position to find best move
        //! \param[in] depth Maximum number of half-moves (plies) to search
        //! \param[in] movestogo Number of moves remaining until next time control
        //! \param[in] movetime Maximum milliseconds to spend on this move
        //! \param[in] wtime Milliseconds remaining on white's clock
        //! \param[in] winc White increment per move in milliseconds
        //! \param[in] btime Milliseconds remaining on black's clock
        //! \param[in] binc Black increment per move in milliseconds
        //! \param[out] ponder If not NULL set to the move engine should ponder next
        //! \return Best move in coordinate notation (e.g. "e2e4", "g8f6", "e7f8q")
        //--------------------------------------------------------------------------
        std::string MyGo(const int depth,
                const int movestogo = 0,
                const uint64_t movetime = 0,
                const uint64_t wtime = 0, 
                const uint64_t winc = 0,
                const uint64_t btime = 0, 
                const uint64_t binc = 0,
                std::string* ponder = NULL);
};

#endif

