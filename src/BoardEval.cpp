// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "BoardEval.hpp"
#include "Log.hpp"

namespace cornedpanda
{
    static const int materialWeights[13] =
    {
        // white weights == black weights
        1000, 5000, 3000, 3000, 9000, BoardEval::MAX_EVAL,
        1000, 5000, 3000, 3000, 9000, BoardEval::MAX_EVAL,
        0
    };
}

cornedpanda::BoardEval::BoardEval()
{
    _parameters["BoardEval_materialCoef"] = 1;
    _parameters["BoardEval_kingSafetyCoef"] = 100;
    _parameters["BoardEval_centerCoef"] = 250;
    _parameters["BoardEval_mobilityCoef"] = 1;
    _parameters["BoardEval_developmentCoef"] = 100;
}

int cornedpanda::BoardEval::getColorCoef() const
{
    return isWhiteToMove()*2-1;
}

int cornedpanda::BoardEval::estimateMaxGain(const Move & m) const
{
    return materialWeights[m._capture] + 2*materialWeights[PIECE_P];
}

bool cornedpanda::BoardEval::isEndGame() const
{
    // TODO refactorer avec evaluate ?
    if (getGameState() != GAME_STATE_RUNNING)
        return true;

    int whiteMaterial = 0;
    int blackMaterial = 0;
    for (int i=PIECE_P; i<PIECE_K; i++)
    {
        whiteMaterial += _nbMaterial[i] * materialWeights[i];
        blackMaterial += _nbMaterial[i+PIECE_p] * materialWeights[i+PIECE_p];
    }

    return (whiteMaterial + blackMaterial < 20);
}

int cornedpanda::BoardEval::evaluate() const
{
    if (getGameState() == GAME_STATE_BLACK_WON)
    {
 //       cornedpanda::Output() << "Black wins ! " << *this << "\n";
        return MIN_EVAL;
    }
    else if (getGameState() == GAME_STATE_WHITE_WON)
    {
   //     cornedpanda::Output() << "White wins ! " << *this << "\n";
        return MAX_EVAL;
    }
    else if (getGameState() == GAME_STATE_DRAW)
    {
     //   cornedpanda::Output() << "Draw ! " << *this << "\n";
        return 0;
    }

    int whiteMaterial = 0;
    int blackMaterial = 0;
    for (int i=PIECE_P; i<PIECE_K; i++)
    {
        whiteMaterial += _nbMaterial[i] * materialWeights[i];
        blackMaterial += _nbMaterial[i+PIECE_p] * materialWeights[i+PIECE_p];
    }

    if (whiteMaterial + blackMaterial < 20)
    {
        //cornedpanda::Output() << "evaluateEndGame ! " << "\n";
        return evaluateEndGame(whiteMaterial, blackMaterial);
    }

    if ((whiteMaterial + blackMaterial < 30) or (countNbOnes(_whiteSquares & RANK_1) + countNbOnes(_blackSquares & RANK_8) < 9))
    {
        //cornedpanda::Output() << "evaluateMiddleGame ! " << "\n";
        return evaluateWhiteMiddleGame(whiteMaterial) - evaluateBlackMiddleGame(blackMaterial);
    }


    //cornedpanda::Output() << *this << "\n";
    //cornedpanda::Output() << "evaluateOpening ! " << "\n";
    return evaluateWhiteOpening(whiteMaterial) - evaluateBlackOpening(blackMaterial);
}

int cornedpanda::BoardEval::evaluateWhiteOpening(int whiteMaterial) const
{
    /*
    cornedpanda::Output() << "evaluateWhiteOpening ! " << "\n";
    cornedpanda::Output() << "whitematos ! " << whiteMaterial << "\n";
    cornedpanda::Output() << "evaluateWhiteDev ! " << evaluateWhiteDevelopment() << "\n";
    cornedpanda::Output() << "evaluateWhiteKingSafe ! " << evaluateWhiteKingSafety() << "\n";
    cornedpanda::Output() << "evaluateWhiteCenter ! " << evaluateWhiteCenter() << "\n";
    cornedpanda::Output() << "evaluateWhiteMobility ! " << evaluateWhiteMobility() << "\n";
*/
    return 
        _parameters.at("BoardEval_materialCoef") * (whiteMaterial)
        + _parameters.at("BoardEval_developmentCoef") * (evaluateWhiteDevelopment())
        + _parameters.at("BoardEval_kingSafetyCoef") * (evaluateWhiteKingSafety())
        + _parameters.at("BoardEval_centerCoef") * (evaluateWhiteCenter())
        + _parameters.at("BoardEval_mobilityCoef") * (evaluateWhiteMobility());
}

int cornedpanda::BoardEval::evaluateBlackOpening(int blackMaterial) const
{
    
    /*
    cornedpanda::Output() << "evaluateblackOpening ! " << "\n";
    cornedpanda::Output() << "blackmatos ! " << blackMaterial << "\n";
    cornedpanda::Output() << "evaluateBlackDev ! " << evaluateBlackDevelopment() << "\n";
    cornedpanda::Output() << "evaluateBlackKingSafe ! " << evaluateBlackKingSafety() << "\n";
    cornedpanda::Output() << "evaluateBlackCenter ! " << evaluateBlackCenter() << "\n";
    cornedpanda::Output() << "evaluateBlackMobility ! " << evaluateBlackMobility() << "\n";
    */
    return 
        _parameters.at("BoardEval_materialCoef") * (blackMaterial)
        + _parameters.at("BoardEval_developmentCoef") * (evaluateBlackDevelopment())
        + _parameters.at("BoardEval_kingSafetyCoef") * (evaluateBlackKingSafety())
        + _parameters.at("BoardEval_centerCoef") * (evaluateBlackCenter())
        + _parameters.at("BoardEval_mobilityCoef") * (evaluateBlackMobility());
}

int cornedpanda::BoardEval::evaluateWhiteMiddleGame(int whiteMaterial) const
{
    //cornedpanda::Output() << "evaluateWhiteMidGame ! " << "\n";
    return (whiteMaterial)
        + 100 * (evaluateWhiteKingSafety())
        + 100 * (evaluateWhiteCenter())
        + 4 * (evaluateWhiteMobility())
        + 128 - std::abs(evaluateWhiteLightSquares() - evaluateWhiteDarkSquares())
        + 20 * evaluateWhiteBishops();
}

int cornedpanda::BoardEval::evaluateBlackMiddleGame(int blackMaterial) const
{
    //cornedpanda::Output() << "evaluateBlackMidGame ! " << "\n";
    return (blackMaterial)
        + 100 * (evaluateBlackKingSafety())
        + 100 * (evaluateBlackCenter())
        + 4 * (evaluateBlackMobility())
        + 128 - std::abs(evaluateBlackLightSquares() - evaluateBlackDarkSquares())
        + 20 * evaluateBlackBishops();
}

int cornedpanda::BoardEval::evaluateEndGame(int whiteMaterial, int blackMaterial) const
{
    //cornedpanda::Output() << "evaluateEndGame ! " << "\n";
    // TODO
    return (whiteMaterial - blackMaterial)
        + evaluatePawnsRanks()
        + evaluateKingPosition();
}

int cornedpanda::BoardEval::evaluatePawnsRanks() const
{
    return 4 * countNbOnes(_squares[PIECE_P] & RANK_7)
        +  3 * countNbOnes(_squares[PIECE_P] & RANK_6)
        +  2 * countNbOnes(_squares[PIECE_P] & RANK_5)
        -  4 * countNbOnes(_squares[PIECE_p] & RANK_2)
        -  3 * countNbOnes(_squares[PIECE_p] & RANK_3)
        -  2 * countNbOnes(_squares[PIECE_p] & RANK_4);
}

int cornedpanda::BoardEval::evaluateKingPosition() const
{
    return 8 * countNbOnes(_squares[PIECE_K] & CENTER)
        + 4 * countNbOnes(_squares[PIECE_K] & LARGE_CENTER)
        + 2 * countNbOnes(_squares[PIECE_K] & LARGE_BORDERS)
        + 1 * countNbOnes(_squares[PIECE_K] & BORDERS)
        - 8 * countNbOnes(_squares[PIECE_k] & CENTER)
        - 4 * countNbOnes(_squares[PIECE_k] & LARGE_CENTER)
        - 2 * countNbOnes(_squares[PIECE_k] & LARGE_BORDERS)
        - 1 * countNbOnes(_squares[PIECE_k] & BORDERS);
}

int cornedpanda::BoardEval::evaluateShannon(int whiteMaterial, int blackMaterial) const 
{
    int score;
    if (getGameState() == GAME_STATE_BLACK_WON)
        score = MIN_EVAL;
    else if (getGameState() == GAME_STATE_WHITE_WON)
        score = MAX_EVAL;
    else if (getGameState() == GAME_STATE_DRAW)
        score = 0;
    else 
        score = 1000*(whiteMaterial - blackMaterial)

            +  5*(evaluateBlackPawnWeaknesses() - evaluateWhitePawnWeaknesses())
            +  1*(evaluateWhiteMobility() - evaluateBlackMobility())

            // TODO fonction d'evaluation de shannon a tuner (mais pas utilise donc pas grave...)
            +  (_isWhiteToMove*(-2)+1) * (_isInCheck) * 5000
            /*
               + 10 *(evaluateWhiteCenter() - evaluateBlackCenter())
               + 10 *(evaluateWhiteDevelopment() - evaluateBlackDevelopment())
               + 10 *(evaluateBlackKingSafety() - evaluateBlackKingSafety())
               */
            ;

#ifdef DEBUG
    if (score < MIN_EVAL || score > MAX_EVAL)
    {
        cornedpanda::Output() << *this <<  '\n';
        cornedpanda::Output() << "BoardEval::evaluate : " << score << '\n';
        cornedpanda::Output() << "BoardEval::evaluateBlackPawnWeaknesses" << evaluateBlackPawnWeaknesses() << '\n';
        cornedpanda::Output() << "BoardEval::evaluateWhitePawnWeaknesses" << evaluateWhitePawnWeaknesses() << '\n';
        cornedpanda::Output() << "BoardEval::evaluateBlackMobility" << evaluateBlackMobility() << '\n';
        cornedpanda::Output() << "BoardEval::evaluateWhiteMobility" << evaluateWhiteMobility() << '\n';
    }
#endif
    assert(score >= MIN_EVAL);
    assert(score <= MAX_EVAL);

    // here score is from white's point of view
    return score;
}

int cornedpanda::BoardEval::evaluateWhiteCenter() const
{
    uint64_t center = 0;
    center = 1ULL << 27 | 1ULL << 28 | 1ULL << 35 | 1ULL << 36;
    uint64_t whitePieces = _squares[PIECE_P] | _squares[PIECE_N] | _squares[PIECE_B] | _squares[PIECE_Q];
    return countNbOnes(whitePieces & center);
}

int cornedpanda::BoardEval::evaluateWhiteDevelopment() const
{
    uint64_t knightSquares = 1ULL << 18 | 1ULL << 21;
    uint64_t bishopSquares = 1ULL << 26 | 1ULL << 33 | 1ULL << 29 |  1ULL << 38;
    uint64_t rookSquares = 1ULL << 2 | 1ULL << 3 | 1ULL << 4 | 1ULL << 5;
    uint64_t kingSquares = 1ULL << 1 | 1ULL << 2 | 1ULL << 3 | 1ULL << 6 | 1ULL << 7;
    return 3*countNbOnes(knightSquares & _squares[PIECE_N]) + 2*countNbOnes(bishopSquares & _squares[PIECE_B]) + countNbOnes(rookSquares & _squares[PIECE_R]) + countNbOnes(kingSquares & _squares[PIECE_K]) + (1 - bool(_squares[PIECE_Q] & RANK_1));
}

int cornedpanda::BoardEval::evaluateWhiteKingSafety() const
{
    //return int((_squares[PIECE_K] & (1ULL << 3 | 1ULL << 4)) == 0) 
    //* countNbOnes((_squares[PIECE_K] << 7 | _squares[PIECE_K] << 8 | _squares[PIECE_K] << 9) & _squares[PIECE_P] & RANK_2);
    return int((_squares[PIECE_K] & (1ULL << 6 | 1ULL << 2 | 1ULL << 1)) != 0) * 5 
        * countNbOnes((_squares[PIECE_K] << 7 | _squares[PIECE_K] << 8 | _squares[PIECE_K] << 9) & _squares[PIECE_P] & RANK_2);
}

int cornedpanda::BoardEval::evaluateWhitePawnWeaknesses() const
{
    int cptDoubled, cptIsolated, cptBackward; // backward pawns on ranks 2 & 3
    uint64_t whitePawns = _squares[PIECE_P];
    uint64_t pA = whitePawns & FILE_A;
    uint64_t pB = whitePawns & FILE_B;
    uint64_t pC = whitePawns & FILE_C;
    uint64_t pD = whitePawns & FILE_D;
    uint64_t pE = whitePawns & FILE_E;
    uint64_t pF = whitePawns & FILE_F;
    uint64_t pG = whitePawns & FILE_G;
    uint64_t pH = whitePawns & FILE_H;
    cptDoubled = bool(pA & (pA - 1)) + bool(pB & (pB - 1)) + bool(pC & (pC - 1)) + bool(pD & (pD - 1)) 
        + bool(pE & (pE - 1)) + bool(pF & (pF - 1)) + bool(pG & (pG - 1)) + bool(pH & (pH - 1));
    cptIsolated = bool(pA & (!pB)) + bool(pB & (!pA & !pC)) + bool(pC & (!pB & !pD)) + bool(pD & (!pC & !pE)) 
        + bool(pE & (!pD & !pF)) + bool(pF & (!pE & !pG)) + bool(pG & (!pF & !pH)) + bool(pH & (!pG));

    // backward pawns on rank 2
    uint64_t pAr2 = pA & RANK_2;
    uint64_t pBr2 = pB & RANK_2;
    uint64_t pCr2 = pC & RANK_2;
    uint64_t pDr2 = pD & RANK_2;
    uint64_t pEr2 = pE & RANK_2;
    uint64_t pFr2 = pF & RANK_2;
    uint64_t pGr2 = pG & RANK_2;
    uint64_t pHr2 = pH & RANK_2;

    // backward pawns on rank 3
    uint64_t pAr3 = pA & RANK_3;
    uint64_t pBr3 = pB & RANK_3;
    uint64_t pCr3 = pC & RANK_3;
    uint64_t pDr3 = pD & RANK_3;
    uint64_t pEr3 = pE & RANK_3;
    uint64_t pFr3 = pF & RANK_3;
    uint64_t pGr3 = pG & RANK_3;
    uint64_t pHr3 = pH & RANK_3;

    // computation for rank 2
    cptBackward = bool(pAr2 & (!pBr2)) + bool(pBr2 & (!pAr2 & !pCr2)) + bool(pCr2 & (!pBr2 & !pDr2)) 
        + bool(pDr2 & (!pCr2 & !pEr2)) + bool(pEr2 & (!pDr2 & !pFr2)) + bool(pFr2 & (!pEr2 & !pGr2)) 
        + bool(pGr2 & (!pFr2 & !pHr2)) + bool(pHr2 & (!pGr2));

    // computation for rank 3
    cptBackward += bool(pAr3 & (!pBr2 & !pBr3)) + bool(pBr3 & (!pAr2 & !pAr3 & !pCr2 & !pCr3)) 
        + bool(pCr3 & (!pBr2 & !pBr3 & !pDr2 & !pDr3)) + bool(pDr3 & (!pCr2 & !pCr3 & !pEr2 & !pEr3)) 
        + bool(pEr3 & (!pDr2 & !pDr3 & !pFr2 & !pFr3)) + bool(pFr3 & (!pEr2 & !pEr3 & !pGr2 & !pGr3)) 
        + bool(pGr3 & (!pFr2 & !pFr3 & !pHr2 & !pHr3)) + bool(pHr3 & (!pGr2 & !pGr3));

    return cptDoubled + cptIsolated + cptBackward;
}

int cornedpanda::BoardEval::evaluateWhiteMobility() const
{
    // white turn
    if (_isWhiteToMove)
        return getMoves().size();

    // black turn
    BoardEval copy(*this);
    int piece0, piece1;
    copy._isWhiteToMove = true;
    copy._possibleMoves.clear();
    piece0 = PIECE_P;
    piece1 = PIECE_K;

    // for each piece
    for (int piece = piece0; piece <= piece1; piece++) 
    {
        uint64_t current = _squares[piece];
        // for each square of this piece
        while (current) 
        { 
            uint64_t from = (current & -current);
            // add moves in _possibleMoves
            cornedpanda::BoardEval::movesFunctions[piece]((cornedpanda::Board&)copy, from);
            current &= (current - 1);
        }
    }
    return copy._possibleMoves.size();
}

int cornedpanda::BoardEval::evaluateBlackCenter() const
{
    uint64_t center = 0;
    center = 1ULL << 27 | 1ULL << 28 | 1ULL << 35 | 1ULL << 36;
    uint64_t blackPieces = _squares[PIECE_p] | _squares[PIECE_n] | _squares[PIECE_b] | _squares[PIECE_q];
    return countNbOnes(blackPieces & center);
}

int cornedpanda::BoardEval::evaluateBlackDevelopment() const
{
    uint64_t knightSquares = 1ULL << 42 | 1ULL << 45;
    uint64_t bishopSquares = 1ULL << 25 | 1ULL << 34 | 1ULL << 37 | 1ULL << 30;
    uint64_t rookSquares = 1ULL << 58 | 1ULL << 59 | 1ULL << 60 | 1ULL << 61;
    uint64_t kingSquares = 1ULL << 57 | 1ULL << 58 | 1ULL << 59 | 1ULL << 62 | 1ULL << 63;
    return 3*countNbOnes(knightSquares & _squares[PIECE_n]) 
        + 2*countNbOnes(bishopSquares & _squares[PIECE_b]) 
        + countNbOnes(rookSquares & _squares[PIECE_r]) 
        + countNbOnes(kingSquares & _squares[PIECE_k]) 
        + (1 - bool(_squares[PIECE_q] & RANK_8));
}

int cornedpanda::BoardEval::evaluateBlackKingSafety() const
{
    //return  int((_squares[PIECE_k] & (1ULL << 59 | 1ULL << 60)) == 0) * countNbOnes((_squares[PIECE_k] >> 7 | _squares[PIECE_k] >> 8 | _squares[PIECE_k] >> 9) & _squares[PIECE_p] & RANK_7);
    return  int((_squares[PIECE_k] & (1ULL << 57 | 1ULL << 58 | 1ULL << 62)) != 0) * 5
        * countNbOnes((_squares[PIECE_k] >> 7 | _squares[PIECE_k] >> 8 | _squares[PIECE_k] >> 9) & _squares[PIECE_p] & RANK_7);
}

int cornedpanda::BoardEval::evaluateBlackPawnWeaknesses() const
{
    int cptDoubled, cptIsolated, cptBackward;
    uint64_t blackPawns = _squares[PIECE_p];
    uint64_t pA = blackPawns & FILE_A;
    uint64_t pB = blackPawns & FILE_B;
    uint64_t pC = blackPawns & FILE_C;
    uint64_t pD = blackPawns & FILE_D;
    uint64_t pE = blackPawns & FILE_E;
    uint64_t pF = blackPawns & FILE_F;
    uint64_t pG = blackPawns & FILE_G;
    uint64_t pH = blackPawns & FILE_H;
    cptDoubled = bool(pA & (pA - 1)) + bool(pB & (pB - 1)) + bool(pC & (pC - 1)) + bool(pD & (pD - 1)) 
        + bool(pE & (pE - 1)) + bool(pF & (pF - 1)) + bool(pG & (pG - 1)) + bool(pH & (pH - 1));
    cptIsolated = bool(pA & (!pB)) + bool(pB & (!pA & !pC)) + bool(pC & (!pB & !pD)) + bool(pD & (!pC & !pE)) 
        + bool(pE & (!pD & !pF)) + bool(pF & (!pE & !pG)) + bool(pG & (!pF & !pH)) + bool(pH & (!pG));

    // backward pawns on rank 7
    uint64_t pAr7 = pA & RANK_7;
    uint64_t pBr7 = pB & RANK_7;
    uint64_t pCr7 = pC & RANK_7;
    uint64_t pDr7 = pD & RANK_7;
    uint64_t pEr7 = pE & RANK_7;
    uint64_t pFr7 = pF & RANK_7;
    uint64_t pGr7 = pG & RANK_7;
    uint64_t pHr7 = pH & RANK_7;

    // backward pawns on rank 6
    uint64_t pAr6 = pA & RANK_6;
    uint64_t pBr6 = pB & RANK_6;
    uint64_t pCr6 = pC & RANK_6;
    uint64_t pDr6 = pD & RANK_6;
    uint64_t pEr6 = pE & RANK_6;
    uint64_t pFr6 = pF & RANK_6;
    uint64_t pGr6 = pG & RANK_6;
    uint64_t pHr6 = pH & RANK_6;

    // computation for rank 7
    cptBackward = bool(pAr7 & (!pBr7)) + bool(pBr7 & (!pAr7 & !pCr7)) + bool(pCr7 & (!pBr7 & !pDr7)) 
        + bool(pDr7 & (!pCr7 & !pEr7)) + bool(pEr7 & (!pDr7 & !pFr7)) + bool(pFr7 & (!pEr7 & !pGr7)) 
        + bool(pGr7 & (!pFr7 & !pHr7)) + bool(pHr7 & (!pGr7));

    // computation for rank 6
    cptBackward += bool(pAr6 & (!pBr7 & !pBr6)) + bool(pBr6 & (!pAr7 & !pAr6 & !pCr7 & !pCr6)) 
        + bool(pCr6 & (!pBr7 & !pBr6 & !pDr7 & !pDr6)) + bool(pDr6 & (!pCr7 & !pCr6 & !pEr7 & !pEr6)) 
        + bool(pEr6 & (!pDr7 & !pDr6 & !pFr7 & !pFr6)) + bool(pFr6 & (!pEr7 & !pEr6 & !pGr7 & !pGr6)) 
        + bool(pGr6 & (!pFr7 & !pFr6 & !pHr7 & !pHr6)) + bool(pHr6 & (!pGr7 & !pGr6));

    return cptDoubled + cptIsolated + cptBackward;
}

int cornedpanda::BoardEval::evaluateBlackMobility() const
{
    // black turn
    if (!_isWhiteToMove)
        return getMoves().size();

    // white turn
    BoardEval copy(*this);
    int piece0, piece1;
    copy._isWhiteToMove = false;
    copy._possibleMoves.clear();
    piece0 = PIECE_p;
    piece1 = PIECE_k;

    // for each piece
    for (int piece = piece0; piece <= piece1; piece++) 
    {
        uint64_t current = _squares[piece];
        // for each square of this piece
        while (current) 
        { 
            uint64_t from = (current & -current);
            // add moves in _possibleMoves
            cornedpanda::Board::movesFunctions[piece]((cornedpanda::Board&)copy, from);
            current &= (current - 1);
        }
    }
    return copy._possibleMoves.size();
}

int cornedpanda::BoardEval::evaluateWhiteLightSquares() const
{
    return countNbOnes(_whiteSquares & LIGHT_SQUARES);
}

int cornedpanda::BoardEval::evaluateWhiteDarkSquares() const
{
    return countNbOnes(_whiteSquares & DARK_SQUARES);
}

int cornedpanda::BoardEval::evaluateBlackLightSquares() const
{
    return countNbOnes(_blackSquares & LIGHT_SQUARES);
}

int cornedpanda::BoardEval::evaluateBlackDarkSquares() const
{
    return countNbOnes(_blackSquares & DARK_SQUARES);
}

int cornedpanda::BoardEval::evaluateWhiteBishops() const
{
    uint64_t bishop = _squares[PIECE_B];
    int nbBishops = countNbOnes(bishop);
    if (nbBishops > 1)
    {
        // bishop pair
        return 2;
    }
    else
        if (nbBishops == 0)
        {
            // no bishop
            return 0;
        }

    uint64_t centerLightPawns = LIGHT_SQUARES & CENTER & _squares[PIECE_P];
    uint64_t centerDarkPawns = DARK_SQUARES & CENTER & _squares[PIECE_P];

    if ( ( LIGHT_SQUARES & bishop & (centerLightPawns != 0) )
            or ( DARK_SQUARES & bishop & (centerDarkPawns != 0) )
       )
        return 0;
    else
        return 1;
}

int cornedpanda::BoardEval::evaluateBlackBishops() const
{
    uint64_t bishop = _squares[PIECE_b];
    int nbBishops = countNbOnes(bishop);
    if (nbBishops > 1)
    {
        // bishop pair
        return 2;
    }
    else
        if (nbBishops == 0)
        {
            // no bishop
            return 0;
        }

    uint64_t centerLightPawns = LIGHT_SQUARES & CENTER & _squares[PIECE_P];
    uint64_t centerDarkPawns = DARK_SQUARES & CENTER & _squares[PIECE_P];

    if ( ( LIGHT_SQUARES & bishop & (centerLightPawns != 0) )
            or ( DARK_SQUARES & bishop & (centerDarkPawns != 0) )
       )
        return 0;
    else
        return 1;
}

int cornedpanda::BoardEval::evaluateWhiteRooks() const
{
    uint64_t whitePawns = _squares[PIECE_P];
    whitePawns |= (whitePawns << 8);
    whitePawns |= (whitePawns << 16);
    whitePawns |= (whitePawns << 32);
    whitePawns |= (whitePawns >> 8);
    whitePawns |= (whitePawns >> 16);
    whitePawns |= (whitePawns >> 32);
    // rooks on open files or half open files + bonus if 7th rank
    return 2 * int(_squares[PIECE_R] & ~whitePawns) + int(_squares[PIECE_R] & RANK_7);

}

int cornedpanda::BoardEval::evaluateBlackRooks() const
{
    uint64_t blackPawns = _squares[PIECE_p];
    blackPawns |= (blackPawns << 8);
    blackPawns |= (blackPawns << 16);
    blackPawns |= (blackPawns << 32);
    blackPawns |= (blackPawns >> 8);
    blackPawns |= (blackPawns >> 16);
    blackPawns |= (blackPawns >> 32);
    // rooks on open files or half open files + bonus if 7th rank
    return 2 * int(_squares[PIECE_r] & ~blackPawns) + int(_squares[PIECE_r] & RANK_2);

}

