// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef _FASTMATH_HPP_
#define _FASTMATH_HPP_

namespace cornedpanda 
{

    inline float fastsqrt(float a) 
    {
        if (!a)
            return 0;
        float x = a/2.;
        x = (x+a/x)/2.;
        x = (x+a/x)/2.;
        x = (x+a/x)/2.;
        return x;
    }

    inline float fastlog2 (float x)
    {
        union { float f; uint32_t i; } vx = { x };
        float y = vx.i;
        y *= 1.0 / (1 << 23);
        return y - 126.94269504f;
    }

    inline float fastlog(float x)
    {
        return 0.69314718f * fastlog2 (x);
    }

}

#endif

