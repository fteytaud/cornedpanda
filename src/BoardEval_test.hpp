// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef _BOARDEVAL_TEST_HPP_
#define _BOARDEVAL_TEST_HPP_

#include <cxxtest/TestSuite.h>
#include <cxxtest/GlobalFixture.h>

#include <cassert>

#include "BoardEval.hpp"
#include "Log.hpp"
#include "Fasttest.hpp"

class BoardEval_test :  public CxxTest::TestSuite, public cornedpanda::BoardEval 
{

    public:

        //////////////////////////////////////////////////
        // evaluate
        //////////////////////////////////////////////////

        void test_evaluate_1() 
        {
            BoardEval_test b;
            b.setFen("2K5/2q5/8/8/8/8/2r5/2k5 w - - 1 20 ");

            TS_ASSERT_EQUALS( b._possibleMoves.size(), 0);
            TS_ASSERT_EQUALS( b._threefoldRepetition, 0);

            cornedpanda::logStream << "\n---- position apres evaluate_1 ----\n";
            cornedpanda::logStream << b;
            cornedpanda::logStream << "---- score : " << b.evaluate() << std::endl;

            TS_ASSERT_EQUALS( b._isInCheck, 1);
            TS_ASSERT_EQUALS( b._possibleMoves.size(), 0);
            TS_ASSERT_EQUALS( b.getGameState(), cornedpanda::GameState::GAME_STATE_BLACK_WON );
            TS_ASSERT_EQUALS( b.evaluate(), -1000000 );

        }

        void test_evaluate_2() 
        {
            BoardEval_test b;
            b.setFen("7k/8/8/8/8/2n5/r7/K7 w - - 1 20 ");

            cornedpanda::logStream << "---- position apres evaluate_2 ----\n";
            cornedpanda::logStream << b;
            cornedpanda::logStream << "---- score : " << b.evaluate() << std::endl;

            TS_ASSERT_EQUALS( b.getGameState(), cornedpanda::GameState::GAME_STATE_BLACK_WON );
            TS_ASSERT_EQUALS( b.evaluate(), -1000000 );

        }

};

#endif

