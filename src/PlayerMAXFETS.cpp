// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "Log.hpp"
#include "Fastmath.hpp"
#include "PlayerMAXFETS.hpp"

#include <cassert>
#include <sstream>

///////////////////////////////////////////////////////////////////////////////
// Node functions
///////////////////////////////////////////////////////////////////////////////

cornedpanda::PlayerMAXFETS::Node::Node(const Node &) 
{
    assert(false); 
}

cornedpanda::PlayerMAXFETS::Node::Node(const cornedpanda::BoardEval & board):
    _board(board), 
    _move(), 
    _possibleMoves(_board.getMoves()), 
    _childNodes(), 
    _ptrParentNode(nullptr), 
    _ptrBestChildNodeEval(nullptr), 
    _ptrBestChildNodeSims(nullptr), 
    _nbSims(0), 
    _minmaxEval(BoardEval::MIN_EVAL), 
    _depth(0), 
    _maxDepth(0) 
{
}

cornedpanda::PlayerMAXFETS::Node::Node(Node * ptrParentNode, const Move & move):
    _board(ptrParentNode->_board), 
    _move(move), 
    _possibleMoves(),
    _childNodes(),
    _ptrParentNode(ptrParentNode), 
    _ptrBestChildNodeEval(nullptr), 
    _ptrBestChildNodeSims(nullptr), 
    _nbSims(0), 
    _minmaxEval(BoardEval::MIN_EVAL), 
    _depth(ptrParentNode->_depth+1),
    _maxDepth(_depth)
{
    _board.makeMove(move);
    _possibleMoves = _board.getMoves();
}

///////////////////////////////////////////////////////////////////////////////
// Player functions
///////////////////////////////////////////////////////////////////////////////

cornedpanda::PlayerMAXFETS::PlayerMAXFETS()
{
    _parameters["PlayerMAXFETS_kuct"] = 3000;
    _parameters["PlayerMAXFETS_nbSimulations"] = 10000;
}

void cornedpanda::PlayerMAXFETS::initBestMove()
{
    Player::initBestMove();
    _iSims = 0;
    _nSims = _parameters["PlayerMAXFETS_nbSimulations"];
    _nodes.clear();
    _nodes.reserve(_nSims + 1000);  // warning: _nodes must be fully reserved !!!
    _nodes.emplace_back(_board);
    _ptrRootNode = &(_nodes.front());
}

void cornedpanda::PlayerMAXFETS::iterBestMove()
{
    computeOneMctsIteration();
    _isBestMoveOk = _iSims >= _nSims;
}

std::string cornedpanda::PlayerMAXFETS::getBestMove()
{
    if (_isBestMoveNone)
        return "none";

    // the best move is the move which has the greatest nbSims
    assert(_ptrRootNode);
    assert(_ptrRootNode->_ptrBestChildNodeSims);
    _bestMove = _ptrRootNode->_ptrBestChildNodeSims->_move;
    return _board.moveToString(_bestMove);
}

std::string cornedpanda::PlayerMAXFETS::formatMovesStats() const
{
    std::stringstream moveStream;
    const auto & childNodes = _ptrRootNode->_childNodes;

    // format move stats
    for (Node * ptrNode : childNodes)
    {
        BoardEval b(_board);
        b.makeMove(ptrNode->_move);
        moveStream << "allmoves =\t" << Board::moveToString(ptrNode->_move) 
            << "\teval=" << -b.evaluate() * b.getColorCoef()
            << "\tminmax=" << ptrNode->_minmaxEval 
            << "\tnbSims=" << ptrNode->_nbSims 
            << "\tmaxDepth=" << ptrNode->_maxDepth << " PANDATOKEN";
    }

    // format principal variation according to minmax eval
    for (Node * ptrNode = _ptrRootNode; ptrNode and not ptrNode->_childNodes.empty(); ptrNode = ptrNode->_ptrBestChildNodeEval)
        moveStream << "PV_eval =\t" << Board::moveToString(ptrNode->_ptrBestChildNodeEval->_move) 
            << "\teval=" << - ptrNode->_ptrBestChildNodeEval->_minmaxEval
            << "\tsims=" << ptrNode->_ptrBestChildNodeEval->_nbSims << " PANDATOKEN" ;

    // format principal variation according to the number of simulations
    for (Node * ptrNode = _ptrRootNode; ptrNode and not ptrNode->_childNodes.empty(); ptrNode = ptrNode->_ptrBestChildNodeSims)
        moveStream << "PV_sims =\t" << Board::moveToString(ptrNode->_ptrBestChildNodeSims->_move) 
            << "\teval=" << - ptrNode->_ptrBestChildNodeSims->_minmaxEval
            << "\tsims=" << ptrNode->_ptrBestChildNodeSims->_nbSims << " PANDATOKEN" ;

    return moveStream.str();
}

void cornedpanda::PlayerMAXFETS::printStatsBestMove()
{
    Player::printStatsBestMove();

    cornedpanda::Output() << "PlayerMAXFETS::printStatsBestMove -- PlayerMAXFETS_kuct = " << _parameters["PlayerMAXFETS_kuct"];
    cornedpanda::Output() << "PlayerMAXFETS::printStatsBestMove -- PlayerMAXFETS_nbSimulations = " << _parameters["PlayerMAXFETS_nbSimulations"];
    cornedpanda::Output() << "PlayerMAXFETS::printStatsBestMove -- _iSims = " << _iSims;
    cornedpanda::Output() << "PlayerMAXFETS::printStatsBestMove -- _nSims = " << _nSims;
#ifdef DEBUG
    cornedpanda::logStream << "PlayerMAXFETS::printStatsBestMove -- PlayerMAXFETS_kuct = " << _parameters["PlayerMAXFETS_kuct"] << '\n';
    cornedpanda::logStream << "PlayerMAXFETS::printStatsBestMove -- PlayerMAXFETS_nbSimulations = " << _parameters["PlayerMAXFETS_nbSimulations"] << '\n';
    cornedpanda::logStream << "PlayerMAXFETS::printStatsBestMove -- _iSims = " << _iSims << '\n';
    cornedpanda::logStream << "PlayerMAXFETS::printStatsBestMove -- _nSims = " << _nSims << '\n';
#endif

}

///////////////////////////////////////////////////////////////////////////////
// MCTS functions 
///////////////////////////////////////////////////////////////////////////////

void cornedpanda::PlayerMAXFETS::computeOneMctsIteration()
{
    Node * ptrSelectedNode = _ptrRootNode;
    assert(ptrSelectedNode->_depth == 0); 

    // MCTS selection (tree traversal to a leaf node)
    while (not ptrSelectedNode->_childNodes.empty())
    {
        // update the depth of the selected node for stats
        if (ptrSelectedNode->_ptrParentNode == _ptrRootNode)
            _statsCurrMove = ptrSelectedNode->_move;

        // the current node is not a leaf so select a child node
        ptrSelectedNode = computeSelection(ptrSelectedNode);
        assert(ptrSelectedNode);
    }

    // if game ended, just backpropagate
    // the selection step ensures _possibleMoves is empty if the game is ended 
    if (ptrSelectedNode->_possibleMoves.empty())
    {
        assert(ptrSelectedNode->_board.getGameState() != GAME_STATE_RUNNING);
        computeBackpropagationEnded(ptrSelectedNode);
        _iSims++;
    }
    // if the game is not ended, do MCTS expansion/simulation/backpropagation with AMAF 
    else
    {
        // AMAF expansion/simulation/backpropagation
        int nbNewSims, newMaxDepth;
        computeExpansionAmaf(ptrSelectedNode, nbNewSims, newMaxDepth);
        computeSimulationAmaf(ptrSelectedNode);
        computeBackpropagationAmaf(ptrSelectedNode, nbNewSims, newMaxDepth);
        _iSims += nbNewSims;
        // update max depth
        _statsDepth = std::max(_statsDepth, ptrSelectedNode->_maxDepth);
        // no selective depth since MAXFETS playout is just a board evaluation
        _statsSelDepth = _statsDepth;
    }

    _statsNodes = _iSims;
}

cornedpanda::PlayerMAXFETS::Node * cornedpanda::PlayerMAXFETS::computeSelection(Node * ptrNode)
{
    // compute UCB for all child node and get the best node
    assert(ptrNode);
    assert(not ptrNode->_childNodes.empty());
    float kuct = _parameters.at("PlayerMAXFETS_kuct");
    Node * ptrBestNode = nullptr;
    float bestScore = BoardEval::MIN_EVAL_F;
    for (Node * ptrChildNode : ptrNode->_childNodes)
    {
        // compute UCB
        assert(ptrChildNode->_nbSims > 0);
        assert(ptrNode->_depth == 0 or ptrChildNode->_nbSims <= ptrNode->_nbSims);
        float exploitation = (float)ptrChildNode->_minmaxEval;
        float exploration = fastsqrt(fastlog(ptrNode->_nbSims) / (float)ptrChildNode->_nbSims);
        float score = exploitation + kuct * exploration;
        // update best node
        if (score > bestScore)
        {
            ptrBestNode = ptrChildNode;
            bestScore = score;
        }
    }
    // return the best node
    assert(ptrBestNode);
    assert(bestScore > BoardEval::MIN_EVAL_F);
    return ptrBestNode;
}

void cornedpanda::PlayerMAXFETS::computeExpansionAmaf(Node * ptrSelectedNode, int & nbNewSims, int & newMaxDepth)
{
    assert(ptrSelectedNode);
    assert(not ptrSelectedNode->_possibleMoves.empty());
    assert(ptrSelectedNode->_childNodes.empty());
    // expand all child nodes using possible moves
    for (const Move & move : ptrSelectedNode->_possibleMoves)
    {
        _nodes.emplace_back(ptrSelectedNode, move);
        ptrSelectedNode->_childNodes.push_back(std::addressof(_nodes.back()));
    }
    ptrSelectedNode->_possibleMoves.clear();
    // get some infos about this expansion
    nbNewSims = ptrSelectedNode->_childNodes.size(); 
    newMaxDepth = ptrSelectedNode->_maxDepth+1;
}

void cornedpanda::PlayerMAXFETS::computeSimulationAmaf(Node * ptrSelectedNode)
{
    assert(ptrSelectedNode);
    assert(ptrSelectedNode->_possibleMoves.empty());
    assert(not ptrSelectedNode->_childNodes.empty()); 
    // simulate new nodes (created during expansion) and update minmax of selected node
    bool selectedWhite = ptrSelectedNode->_board.isWhiteToMove();
    bool rootWhite = _ptrRootNode->_board.isWhiteToMove();
    int coef = (selectedWhite == rootWhite) ? _ptrRootNode->_board.getColorCoef() : - _ptrRootNode->_board.getColorCoef();
    for (Node * ptrNode : ptrSelectedNode->_childNodes)
    {
        ptrNode->_minmaxEval = coef * ptrNode->_board.evaluate();
        ptrNode->_nbSims++;
    };
}

void cornedpanda::PlayerMAXFETS::computeBackpropagationAmaf(Node * ptrSelectedNode, int nbNewSims, int newMaxDepth)
{
    assert(ptrSelectedNode);
    assert(ptrSelectedNode->_possibleMoves.empty());
    assert(not ptrSelectedNode->_childNodes.empty()); 
    // backpropagate simulation results (from selected node to root node)
    for (Node * p = ptrSelectedNode; p; p = p->_ptrParentNode)
    {
        // find best child nodes (according to minmaxEval/nbSims) 
        int bestEval = BoardEval::MIN_EVAL;
        int bestSims = -1;
        for (Node * pChild : p->_childNodes)
        {
            if (pChild->_minmaxEval > bestEval)
            {
                bestEval = pChild->_minmaxEval;
                p->_ptrBestChildNodeEval = pChild;
            }
            if (pChild->_nbSims > bestSims)
            {
                bestSims = pChild->_nbSims;
                p->_ptrBestChildNodeSims = pChild;
            }
        }
        // update node infos
        p->_minmaxEval = - bestEval;
        p->_nbSims += nbNewSims;
        p->_maxDepth = std::max(p->_maxDepth, newMaxDepth);
    }
}

void cornedpanda::PlayerMAXFETS::computeBackpropagationEnded(Node * ptrSelectedNode)
{
    assert(ptrSelectedNode);
    assert(ptrSelectedNode->_possibleMoves.empty());
    assert(ptrSelectedNode->_childNodes.empty());
    for (Node * p = ptrSelectedNode; p; p = p->_ptrParentNode)
        p->_nbSims++;
}

