
#include "magicmoves/magicmoves.hpp"
#include "Board.hpp"
#include "Log.hpp"

int main(int, char**)
{

#ifdef DEBUG
    cornedpanda::initLogStream("cornedpanda_magicmoves_test.log");
#endif

    initmagicmoves();

    for (int square=0; square<64; square++)
    {
        uint64_t occupancy = 0xFFFFFFFFFFFFFFFF;

        uint64_t rookAttack = Rmagic(square, occupancy);   
        std::cout << "Rook " << square << std::endl;
        cornedpanda::Board::printUint64Board(std::cout, rookAttack, "");
        std::cout << std::endl;

        uint64_t bishopAttack = Bmagic(square, occupancy);   
        std::cout << "Bishop " << square << std::endl;
        cornedpanda::Board::printUint64Board(std::cout, bishopAttack, "");
        std::cout << std::endl;
    }

    return 0;
}

