// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "PlayerID.hpp"
#include "Log.hpp"

#include <sstream>

// https://chessprogramming.wikispaces.com/Iterative+Deepening
// https://github.com/nescitus/cpw-engine

cornedpanda::PlayerID::PlayerID()
{
    _parameters["PlayerID_maxDepth"] = 6;
    //_parameters["PlayerID_aspiration"] = 500;
}

void cornedpanda::PlayerID::initBestMove()
{
    const auto & moves = _board.getMoves();
    if (moves.empty())
        _isBestMoveNone = true;

    // init to first move in case all moves suck
    _bestMove = moves.front(); 

    // init stats 
    _statsDepth = 0;
    _statsSelDepth = 0;
    _statsNodes = 0;
    _statsQNodes = 0;
    _statsCurrMoveNumber = 0;
    _statsCurrMove = moves.front();

    // search best move using alpha-beta
    //float maxDepth = _parameters["PlayerID_maxDepth"];
    //alphabeta(_board, 0, maxDepth, -1e7, 1e7);
    _isBestMoveOk = true;
}

void cornedpanda::PlayerID::iterBestMove()
{
}

std::string cornedpanda::PlayerID::getBestMove()
{
    return "none";
}

//void cornedpanda::PlayerID::alphabeta(const BoardEval & refBoard, int currentDepth, int maxDepth) 
void cornedpanda::PlayerID::alphabeta(const BoardEval & refBoard, int currentDepth, int ) 
{
    _statsNodes++;
    _statsDepth = std::max(_statsDepth, currentDepth);

/*
    // test the end of the search (do not change the order of the ifs !!!)
    if (refBoard.isEndGame())
        return refBoard.evaluate() * refBoard.getColorCoef();
    if (currentDepth == maxDepth)
        return refBoard.evaluate() * refBoard.getColorCoef();
        //return quiesce(refBoard, currentDepth, alpha, beta);
*/
    // continue the search 
    const std::vector<Move> & moves = refBoard.getMoves();
    for (unsigned i=0; i<moves.size(); i++)
    {
        const Move & m = moves[i];
        if (currentDepth == 0)
        {
            _statsCurrMoveNumber = i;
            _statsCurrMove = m;
        }
        BoardEval boardCopy(refBoard);
        boardCopy.makeMove(m);
        /*
        int score = -alphabeta(boardCopy, currentDepth+1, maxDepth, -beta, -alpha);
        if (score >= beta)
            return beta;
        if (score > alpha)
        {
            alpha = score;
            if (currentDepth == 0) 
            {
                _bestMove = m;
            }
        }
        */
    }

    //return alpha;
}

void cornedpanda::PlayerID::printStatsBestMove()
{
    Player::printStatsBestMove();

    // output info
    float maxDepth = _parameters["PlayerID_maxDepth"];
    cornedpanda::Output() << "cornedpanda::PlayerID::getMove : PlayerID_maxDepth = " << maxDepth << '\n';
#ifdef DEBUG
    cornedpanda::logStream << "cornedpanda::PlayerID::getMove : PlayerID_maxDepth = " << maxDepth << '\n';
#endif
}

