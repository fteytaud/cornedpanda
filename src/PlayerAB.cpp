// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "PlayerAB.hpp"
#include "Log.hpp"

#include <sstream>

// TODO verifier la quiescence search et le delta-pruning

// https://github.com/nescitus/cpw-engine/blob/master/Quiescence.cpp
// https://chessprogramming.wikispaces.com/Delta+Pruning

///////////////////////////////////////////////////////////////////////////////
// PlayerAB
///////////////////////////////////////////////////////////////////////////////

cornedpanda::PlayerAB::PlayerAB()
{
    _parameters["PlayerAB_maxDepth"] = 4;
}

void cornedpanda::PlayerAB::initBestMove()
{
    Player::initBestMove();
    _moves.clear();
    _scores.clear();
}

void cornedpanda::PlayerAB::iterBestMove()
{
    float maxDepth = _parameters["PlayerAB_maxDepth"];
    alphabeta(maxDepth);
    _isBestMoveOk = true;
}

void cornedpanda::PlayerAB::alphabeta(int maxDepth) 
{
    assert(maxDepth > 0);
    _statsNodes++;
    // TODO liste PV move 

    // start alpha-beta search 
    const std::vector<Move> & moves = _board.getMoves();
    assert(not moves.empty());
    int alpha = -1e7;
    int beta = 1e7;
    for (unsigned i=0; i<moves.size(); i++)
    {
        const Move & m = moves[i];
        _statsCurrMoveNumber = i;
        _statsCurrMove = m;
        BoardEval boardCopy(_board);
        boardCopy.makeMove(m);
        int score = -alphabetaRec(boardCopy, 1, maxDepth, -beta, -alpha);
        _moves.push_back(m);
        _scores.push_back(score);
        if (score > alpha)
        {
            alpha = score;
            _bestMove = m;
        }
    }
}

int cornedpanda::PlayerAB::alphabetaRec(const BoardEval & refBoard, int currentDepth, int maxDepth, int alpha, int beta) 
{
    _statsNodes++;
    _statsDepth = std::max(_statsDepth, currentDepth);

    // test the end of the search (do not change the order of the ifs !!!)
    if (refBoard.getGameState() != GAME_STATE_RUNNING)
        return refBoard.evaluate() * refBoard.getColorCoef();
    if (currentDepth == maxDepth)
        return quiesce(refBoard, currentDepth, alpha, beta);

    // continue the search 
    const std::vector<Move> & moves = refBoard.getMoves();
    for (unsigned i=0; i<moves.size(); i++)
    {
        const Move & m = moves[i];
        BoardEval boardCopy(refBoard);
        boardCopy.makeMove(m);
        int score = -alphabetaRec(boardCopy, currentDepth+1, maxDepth, -beta, -alpha);
        if (score >= beta)
            return beta;
        if (score > alpha)
            alpha = score;
    }
    return alpha;
}

int cornedpanda::PlayerAB::quiesce(const BoardEval & refBoard, int, int, int) 
{
    // no quiescence search, just evaluate the board
    return refBoard.evaluate() * refBoard.getColorCoef();
}

std::string cornedpanda::PlayerAB::formatMovesStats() const
{
    assert(_moves.size() == _scores.size());
    std::stringstream moveStream;
    for (unsigned i=0; i<_moves.size(); i++)
    {
        // make move
        const Move & m = _moves[i]; 
        BoardEval b(_board);
        b.makeMove(m);
        // evaluate move
        int eval = -b.evaluate() * b.getColorCoef();
        // print evaluation
        moveStream << "allmoves =\t" << Board::moveToString(m) 
            << "\teval=" << eval 
            << "\tscore=" << _scores[i] << " PANDATOKEN";
    }
    return moveStream.str();
}

void cornedpanda::PlayerAB::printStatsBestMove()
{

    Player::printStatsBestMove();

    // output info
    float maxDepth = _parameters["PlayerAB_maxDepth"];
    cornedpanda::Output() << "PlayerAB::printStatsBestMove -- PlayerAB_maxDepth = " << maxDepth ;
#ifdef DEBUG
    cornedpanda::logStream << "PlayerAB::printStatsBestMove -- PlayerAB_maxDepth = " << maxDepth  << std::endl;
#endif

}

///////////////////////////////////////////////////////////////////////////////
// PlayerABQ
///////////////////////////////////////////////////////////////////////////////

int cornedpanda::PlayerABQ::quiesce(const BoardEval & refBoard, int currentDepth, int alpha, int beta) 
{
    _statsNodes++;
    _statsQNodes++;
    _statsSelDepth = std::max(_statsSelDepth, currentDepth);

    int eval = refBoard.evaluate() * refBoard.getColorCoef();
    if (eval >= beta)
        return beta;
    if (eval > alpha)
        alpha = eval;

    const std::vector<Move> & moves = refBoard.getMoves();
    for (const Move & m: moves) 
    {
        if (m._capture == PIECE_k or m._capture == PIECE_K)
            return BoardEval::MAX_EVAL * refBoard.getColorCoef();
        if (m._capture != PIECE_0)
        {
            BoardEval boardCopy(refBoard);
            boardCopy.makeMove(m);
            int score = -quiesce(boardCopy, currentDepth+1, -beta, -alpha);
            if (score >= beta)
                return beta;
            if (score > alpha)
                alpha = score;
        }
    }
    return alpha;
}

///////////////////////////////////////////////////////////////////////////////
// PlayerABQD
///////////////////////////////////////////////////////////////////////////////

int cornedpanda::PlayerABQD::quiesce(const BoardEval & refBoard, int currentDepth, int alpha, int beta) 
{
    _statsNodes++;
    _statsQNodes++;
    _statsSelDepth = std::max(_statsSelDepth, currentDepth);

    int eval = refBoard.evaluate() * refBoard.getColorCoef();
    if (eval >= beta)
        return beta;
    if (eval > alpha)
        alpha = eval;

    const std::vector<Move> & moves = refBoard.getMoves();
    for (const Move & m: moves) 
    {
        if (m._capture == PIECE_k or m._capture == PIECE_K)
            return BoardEval::MAX_EVAL * refBoard.getColorCoef();
        if (m._capture != PIECE_0)
        {
            // delta pruning
            int delta = refBoard.estimateMaxGain(m);
            if (eval + delta < alpha) continue;

            BoardEval boardCopy(refBoard);
            boardCopy.makeMove(m);
            int score = -quiesce(boardCopy, currentDepth+1, -beta, -alpha);
            if (score >= beta)
                return beta;
            if (score > alpha)
                alpha = score;
        }
    }
    return alpha;
}

///////////////////////////////////////////////////////////////////////////////
// PlayerABQLimited
///////////////////////////////////////////////////////////////////////////////

int cornedpanda::PlayerABQLIMITED::quiesce(const BoardEval & refBoard, int currentDepth, int alpha, int beta) 
{
    _statsNodes++;
    _statsQNodes++;
    _statsSelDepth = std::max(_statsSelDepth, currentDepth);

    int eval = refBoard.evaluate() * refBoard.getColorCoef();
    if (eval >= beta)
        return beta;
    if (eval > alpha)
        alpha = eval;

    const std::vector<Move> & moves = refBoard.getMoves();
    for (const Move & m: moves) 
    {
        if (m._capture != PIECE_0
                and (
                    (m._capture == PIECE_q or m._capture == PIECE_Q) // always simplify if queens
                    or (m._capture == PIECE_r or m._capture == PIECE_R) // always simplify if rooks
                    or (m._capture == PIECE_b or m._capture == PIECE_B) // always simplify if bishops
                    or (m._capture == PIECE_n or m._capture == PIECE_N) // always simplify if knights
                    or (currentDepth < 5) // for pawns only search until depth 12
                  )
           )
        {
            BoardEval boardCopy(refBoard);
            boardCopy.makeMove(m);
            int score = -quiesce(boardCopy, currentDepth+1, -beta, -alpha);
            if (score >= beta)
                return beta;
            if (score > alpha)
                alpha = score;
        }
    }
    return alpha;
}

///////////////////////////////////////////////////////////////////////////////
// PlayerABQDLimited
///////////////////////////////////////////////////////////////////////////////

int cornedpanda::PlayerABQDLIMITED::quiesce(const BoardEval & refBoard, int currentDepth, int alpha, int beta) 
{
    _statsNodes++;
    _statsQNodes++;
    _statsSelDepth = std::max(_statsSelDepth, currentDepth);

    int eval = refBoard.evaluate() * refBoard.getColorCoef();
    if (eval >= beta)
        return beta;
    if (eval > alpha)
        alpha = eval;

    const std::vector<Move> & moves = refBoard.getMoves();
    for (const Move & m: moves) 
    {
        if (m._capture != PIECE_0
                and (
                    (m._capture == PIECE_q or m._capture == PIECE_Q) // always simplify if queens
                    or (m._capture == PIECE_r or m._capture == PIECE_R) // always simplify if rooks
                    or (m._capture == PIECE_b or m._capture == PIECE_B) // always simplify if bishops
                    or (m._capture == PIECE_n or m._capture == PIECE_N) // always simplify if knights
                    or (currentDepth < 8) // for pawns only search until depth 12
                  )
           )
        {
            // delta pruning
            int delta = refBoard.estimateMaxGain(m);
            if (eval + delta < alpha) continue;

            BoardEval boardCopy(refBoard);
            boardCopy.makeMove(m);
            int score = -quiesce(boardCopy, currentDepth+1, -beta, -alpha);
            if (score >= beta)
                return beta;
            if (score > alpha)
                alpha = score;
        }
    }
    return alpha;
}
