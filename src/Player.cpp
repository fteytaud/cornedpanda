// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "Player.hpp"
#include "Log.hpp"

#include <cassert>
#include <chrono>
#include <regex>
#include <sstream>
#include <vector>

// for getpid (RNG seed)
#include <sys/types.h>
#include <unistd.h>

cornedpanda::Player::~Player()
{
}

cornedpanda::Player::Player() 
{
    _isInit = false;
}

void cornedpanda::Player::printBoard(std::ostream & os) const
{
    os << _board;
}

std::map<std::string, int> & cornedpanda::Player::getBoardParameters()
{
    return _board._parameters;
}

bool cornedpanda::Player::isInit() const 
{
    return _isInit;
}

void cornedpanda::Player::init()
{
    if (not _isInit)
    {
        int id = getpid();
        int t = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();
        _engine.seed(id + t);
        _isInit = true;
    }
}

bool cornedpanda::Player::isWhiteToMove() const
{
    return _board.isWhiteToMove();
}

const char * cornedpanda::Player::setFen(const char * fen)
{
    return _board.setFen(fen);
}

std::string cornedpanda::Player::getFen() const
{
    return _board.getFen();
}

const char * cornedpanda::Player::makeMoveString(const char * str)
{
    cornedpanda::Move m;
    str = _board.stringToMove(str, m);
    if (str != nullptr)
        _board.makeMove(m);
    return str;
}

void cornedpanda::Player::initBestMove()
{
    _isBestMoveNone = _board.getMoves().empty();
    _isBestMoveOk = _isBestMoveNone;

    // init stats 
    _statsDepth = 0;
    _statsSelDepth = 0;
    _statsNodes = 0;
    _statsQNodes = 0;
    _statsCurrMoveNumber = -1;
}

void cornedpanda::Player::iterBestMove()
{
    const std::vector<Move> & allMoves = _board.getMoves();
    int u = random(allMoves.size());
    _bestMove = allMoves[u];
    _statsCurrMoveNumber = u;
    _statsCurrMove = _bestMove;
    _isBestMoveOk = true;
}

bool cornedpanda::Player::isBestMoveOk()
{
    return _isBestMoveOk;
}

std::string cornedpanda::Player::getBestMove()
{
    if (_isBestMoveNone)
        return "none";
    else
        return Board::moveToString(_bestMove);
}

std::string cornedpanda::Player::formatMovesStats() const
{
    return "random move";
}

int cornedpanda::Player::random(int b)
{
    int r = b*_distribution(_engine);
    if (r >= b) r--;
    return r;
}

void cornedpanda::Player::getStats(int & depth, int & seldepth, uint64_t & nodes, uint64_t & qnodes, int & movenum, Move & move) const
{
    depth = _statsDepth;
    seldepth = _statsSelDepth;
    nodes = _statsNodes;
    qnodes = _statsQNodes;
    movenum = _statsCurrMoveNumber;
    move = _statsCurrMove;
}

void cornedpanda::Player::printStatsBestMove()
{

    const std::string & gameState = cornedpanda::GameStateStrings[_board.getGameState()];
    std::string bestMove = _isBestMoveNone ? "none" : Board::moveToString(_bestMove);
    std::string fen = _board.getFen();
    int colorCoef = _board.getColorCoef();
    int eval = _board.evaluate() * _board.getColorCoef();
    std::string colorToMove = _board.isWhiteToMove() ? "white" : "black";
    std::string statsString = formatMovesStats();
    std::regex tokenRegex("PANDATOKEN");

    cornedpanda::Output() << "Player::printStatsBestMove -- fen = " << fen ;
    cornedpanda::Output() << "Player::printStatsBestMove -- gameState = " << gameState ;
    cornedpanda::Output() << "Player::printStatsBestMove -- colorToMove = " << colorToMove ;
    cornedpanda::Output() << "Player::printStatsBestMove -- colorCoef = " << colorCoef ;
    cornedpanda::Output() << "Player::printStatsBestMove -- eval = " << eval ;

    for (std::sregex_token_iterator iter(statsString.begin(), statsString.end(), tokenRegex, -1);
            iter != std::sregex_token_iterator();
            ++iter)
    cornedpanda::Output() << "Player::printStatsBestMove -- " << *iter;

    cornedpanda::Output() << "Player::printStatsBestMove -- _statsDepth = " << _statsDepth;
    cornedpanda::Output() << "Player::printStatsBestMove -- _statsSelDepth = " << _statsSelDepth;
    cornedpanda::Output() << "Player::printStatsBestMove -- _statsNodes = " << _statsNodes;
    cornedpanda::Output() << "Player::printStatsBestMove -- _statsQNodes = " << _statsQNodes;
    cornedpanda::Output() << "Player::printStatsBestMove -- _bestMove = " << bestMove;
#ifdef DEBUG
    cornedpanda::logStream << _board << std::endl;
    cornedpanda::logStream << "Player::printStatsBestMove -- fen = " << fen << '\n' ;
    cornedpanda::logStream << "Player::printStatsBestMove -- gameState = " << gameState << '\n';
    cornedpanda::logStream << "Player::printStatsBestMove -- colorToMove = " << colorToMove << '\n';
    cornedpanda::logStream << "Player::printStatsBestMove -- colorCoef = " << colorCoef << '\n' ;
    cornedpanda::logStream << "Player::printStatsBestMove -- eval = " << eval << '\n' ;

    for (std::sregex_token_iterator iter(statsString.begin(), statsString.end(), tokenRegex, -1);
            iter != std::sregex_token_iterator();
            ++iter)
    cornedpanda::logStream << "Player::printStatsBestMove -- " << *iter << '\n';

    cornedpanda::logStream << "Player::printStatsBestMove -- _statsDepth = " << _statsDepth << '\n';
    cornedpanda::logStream << "Player::printStatsBestMove -- _statsSelDepth = " << _statsSelDepth << '\n';
    cornedpanda::logStream << "Player::printStatsBestMove -- _statsNodes = " << _statsNodes << '\n';
    cornedpanda::logStream << "Player::printStatsBestMove -- _statsQNodes = " << _statsQNodes << '\n';
    cornedpanda::logStream << "Player::printStatsBestMove -- _bestMove = " << bestMove << '\n';
#endif
}

