// Copyright © 2015 Teytaud & Dehos <{teytaud,dehos}@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

/*
./bin/cornedpanda_test.out | sed -n 's/^.*allMoves = //p' | sed -n 's/ /\
/gp'
*/

#include "Board.hpp"
#include "Log.hpp"
#include "Fasttest.hpp"

#include "PlayerAB.hpp"
#include "PlayerMAXFETS.hpp"

void runPlayer(const std::string & message, cornedpanda::Player * ptrPlayer, const char moveString [])
{
    std::cout << std::endl << message << std::endl;
    uint64_t t0 = senjo::Now();
    ptrPlayer->init();
    cornedpanda::makeMovesFromString(*ptrPlayer, moveString);
    ptrPlayer->initBestMove(); 
    while (not ptrPlayer->isBestMoveOk())
        ptrPlayer->iterBestMove(); 
    ptrPlayer->getBestMove(); 
    ptrPlayer->printStatsBestMove();
    uint64_t t1 = senjo::Now();
    std::cout << "time = " << senjo::ToSeconds(t1-t0) << " s" << std::endl;
    delete ptrPlayer;
}

int main()
{
    cornedpanda::initLogStream("cornedpanda_test.log");

    const char moveString [] = "d2d4 e7e6 e2e4 b8c6 b1c3 d7d5";
    std::cout << std::endl << "moveString = " << moveString << std::endl;
    cornedpanda::Player * p;

    p = new cornedpanda::Player;
    p->_parameters["PlayerAB_maxDepth"] = 2;
    runPlayer("Player", p, moveString);

    p = new cornedpanda::PlayerAB;
    p->_parameters["PlayerAB_maxDepth"] = 2;
    runPlayer("PlayerAB", p, moveString);

    p = new cornedpanda::PlayerABQ;
    p->_parameters["PlayerAB_maxDepth"] = 2;
    runPlayer("PlayerABQ", p, moveString);

    p = new cornedpanda::PlayerABQD;
    p->_parameters["PlayerAB_maxDepth"] = 2;
    runPlayer("PlayerABQD", p, moveString);

    p = new cornedpanda::PlayerMAXFETS;
    p->_parameters["PlayerMAXFETS_kuct"] = 2000;
    p->_parameters["PlayerMAXFETS_nbSimulations"] = 10000;
    runPlayer("PlayerMAXFETS", p, moveString);

    return 0;
}

/*
    cornedpanda::PlayerAB p;
    p.init();
    p.setFen("r1bqkbnr/ppp2ppp/2n1p3/3p4/3PP3/2N5/PPP2PPP/R1BQKBNR w KQkq - 0 4");
    //p.setFen("rnbqkb1r/2p1pppp/5n2/1p1p4/3P4/P1NBPN2/P1P2PPP/R1BQK2R w KQkq - 0 7");
    p.initBestMove(); 
    std::cout << p.getBestMove();
    std::cout << std::endl;
*/

/*
    cornedpanda::PlayerAB p;
    p.init();
    const char moveString [] = "d2d4 e7e6 e2e4 b8c6 b1c3 d7d5";
    cornedpanda::makeMovesFromString(p, moveString);
    p.initBestMove(); 
    std::cout << p.getBestMove();
    std::cout << std::endl;
*/

