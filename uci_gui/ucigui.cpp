// g++ -o ucigui.out ucigui.cpp -Wall -Wextra -std=c++11 -pthread `pkg-config --cflags --libs glibmm-2.4`
// ps aux|grep cornedpanda|awk '{print $2}'|xargs kill -9

#include <boost/tokenizer.hpp>
#include <glibmm.h>
#include <chrono>
#include <ctime>
#include <iostream>
#include <regex>
#include <string>
#include <vector>

typedef const std::string & refStr_t;

// connection to a new process (uci engine or chess arbiter) with communication (standard I/O streams)
class Connection {
    Glib::Pid _pid;
    Glib::RefPtr<Glib::IOChannel> _chStdin, _chStdout;
    public:
    Connection(refStr_t programName) {
        // open a new process
        std::vector<std::string> argv;
        argv.push_back(programName);
        int fdStdout, fdStdin;
        Glib::spawn_async_with_pipes(Glib::get_current_dir(), argv, Glib::SPAWN_SEARCH_PATH, sigc::slot<void>(), &_pid, &fdStdin, &fdStdout);
        // get I/O channels
        _chStdin = Glib::IOChannel::create_from_fd(fdStdin);
        _chStdout = Glib::IOChannel::create_from_fd(fdStdout);
    }
    // send a command to the process and get response if any
    void executeCommand(refStr_t command) {
        _chStdin->write(command + "\n");
        _chStdin->flush();
    }
    // send a command to the process and get response if any
    std::string executeCommandResponse(refStr_t command, refStr_t expectedResponses) {
        // send command
        executeCommand(command);
        // get answers
        Glib::ustring buffer;
        std::string result;
        while (true) {
            _chStdout->read_line(buffer);
            std::string line(buffer);
            result += line;
            if (line.empty() or std::regex_search(line, std::regex(expectedResponses)))
                return result;
        }
    }
    void printInfos(const std::string & headerStr) {
        std::cout << headerStr;
        std::cout.flush();
        std::stringstream ss;
        ss << "ps u -p " << _pid << " | tail -1 | awk '{print $4}'";
        system(ss.str().c_str());
    }
};

// uci engine
// see Universal Chess Interface at http://www.shredderchess.com/chess-info/features/uci-universal-chess-interface.html
class Engine : public Connection {
    typedef boost::char_separator<char> separator_t; 
    typedef boost::tokenizer<separator_t> tokenizer_t;
    public:
    Engine(refStr_t program, refStr_t optionsStr) : Connection(program) {
        std::string uciOptionsStr = doUci();
        // set engine options
        // parse a string of options "option1=value1;option2=value2"
        tokenizer_t options(optionsStr, separator_t(";"));
        for (refStr_t optionStr : options) {
            tokenizer_t option(optionStr, separator_t("="));
            std::vector<std::string> v{option.begin(), option.end()};
            if (v.size()>1 and std::regex_search(uciOptionsStr, std::regex(v[0])))
                doSetoption(v[0], v[1]);
            else {
                std::cout << "error in option setting: " << optionStr << std::endl;
                exit(-1);
            }
        }
        doIsready();
    }
    std::string doUci() { 
        return executeCommandResponse("uci", "uciok");
    }
    std::string doIsready() { 
        return executeCommandResponse("isready", "readyok");
    }
    std::string doGo() { 
        return executeCommandResponse("go", "bestmove");
    }
    void doPosition(refStr_t moves) { 
        executeCommand("position startpos moves " + moves);
    }
    void doSetoption(refStr_t name, refStr_t value) { 
        executeCommand("setoption name " + name + " value " + value);
    }
    void doUcinewgame() { 
        executeCommand("ucinewgame");
    }
    void doQuit() { 
        executeCommand("quit");
    }
};

// chess arbiter
// home-made protocol:
// - send "gamestate" + a list of moves (e.g. "e2e4 d7d5 f1b5 c7c6") to arbiter's stdin 
//   -> receive game state from arbiter's stdout ("running", "black won", "white won" or "draw")
// - send "validmove" + a list of moves 
//   -> receive "true" if the last move is valid or "false" if it is not
// - send "quit" 
//   -> quit
class Arbiter : public Connection {
    public:
    Arbiter(refStr_t program) : Connection(program) {}
    std::string doGamestate(refStr_t moves) {
        return executeCommandResponse("gamestate " + moves, "(running)|(white won)|(black won)|(draw)");
    }
    std::string doValidmove(refStr_t moves) {
        return executeCommandResponse("validmove " + moves, "(true)|(false)");
    }
    void doQuit() {
        executeCommand("quit");
    }
};

// for running games
class Game {
    Arbiter _arbiter;
    Engine _whiteEngine;
    Engine _blackEngine;
    public:
    Game(refStr_t arbiterProg, refStr_t whiteProg, refStr_t whiteArgs, refStr_t blackProg, refStr_t blackArgs) :
        _arbiter(arbiterProg), _whiteEngine(whiteProg, whiteArgs), _blackEngine(blackProg, blackArgs) 
    {}
    void quit() {
        _arbiter.doQuit(); 
        _whiteEngine.doQuit(); 
        _blackEngine.doQuit(); 
    }
    std::string play() {
        // init new game
        std::string moves;
        _whiteEngine.doUcinewgame();
        _blackEngine.doUcinewgame();
        Engine * ptrCurrent = &_whiteEngine;
        Engine * ptrNext = &_blackEngine;
        // run game
        while (true) {
            // get move from current engine
            ptrCurrent->doPosition(moves);
            std::string goStr = ptrCurrent->doGo();
            // extract move from engine response 
            std::smatch match;
            std::regex_search(goStr, match, std::regex("bestmove\\s+(\\w+)\\s"));
            std::string move = match.size()!=2 ? "" : match[1].str();
            moves += " " + move;
            std::cout << move << ' ';
            std::cout.flush();
            // check move
            if (move.empty() or std::regex_search(_arbiter.doValidmove(moves), std::regex("false"))) {
                std::cout << "INVALID MOVE ";
                return ptrCurrent == &_whiteEngine ? "black won" : "white won";
            }
            // check game state
            std::string state = _arbiter.doGamestate(moves);
            if (not std::regex_search(state, std::regex("running")))
                return state;
            // swap engines
            std::swap(ptrCurrent, ptrNext);
        }
    }
    void printInfos() {
        _whiteEngine.printInfos("white engine %mem = ");
        _blackEngine.printInfos("black engine %mem = ");
    }
};

int main(int argc, char ** argv) {
    // check command line arguments
    if (argc != 7) {
        std::cout<<"usage: "<<argv[0]<<" <arbiter> <white engine> \"[white options]\" <black engine> \"[black options]\" <nb games>"<<std::endl;
        std::cout<<"example: "<<argv[0]<<" cornedpandarbitre.out cornedpanda.out \"Engine_player=PlayerMAXFETS\" stockfish \"Skill Level=1\" 2"<<std::endl;
        exit(-1);
    }
    // get command line arguments
    char * arbiter = argv[1];
    char * whiteEngine = argv[2];
    char * whiteOptions = argv[3];
    char * blackEngine = argv[4];
    char * blackOptions = argv[5];
    int nbGames = std::stoi(argv[6]);
    std::time_t startTime = std::time(0);
    std::chrono::time_point<std::chrono::system_clock> startTimepoint = std::chrono::system_clock::now();
    std::cout << std::asctime(std::localtime(&startTime));
    std::cout << "arbiter = " << arbiter << std::endl;
    std::cout << "whiteEngine = " << whiteEngine << std::endl;
    std::cout << "whiteOptions = " << whiteOptions << std::endl;
    std::cout << "blackEngine = " << blackEngine << std::endl;
    std::cout << "blackOptions = " << blackOptions << std::endl;
    std::cout << "nbGames = " << nbGames << std::endl;
    std::cout << std::endl;
    // create engine and arbiter processes
    Game game(arbiter, whiteEngine, whiteOptions, blackEngine, blackOptions);
    // run games
    int nbWhiteWins = 0;
    int nbBlackWins = 0;
    int nbDraws = 0;
    for (int i=0; i<nbGames; i++) {
        std::time_t gameTime = std::time(0);
        std::cout << std::asctime(std::localtime(&gameTime));
        // play game
        std::cout << "game " << i << std::endl;
        std::cout.flush();
        std::string state = game.play();
        std::cout << std::endl << state; 
        game.printInfos();
        std::cout << std::endl;
        // get final state
        if (std::regex_search(state, std::regex("black won")))
            nbBlackWins++;
        if (std::regex_search(state, std::regex("white won")))
            nbWhiteWins++;
        if (std::regex_search(state, std::regex("draw")))
            nbDraws++;
    }
    game.quit();
    std::time_t endTime = std::time(0);
    std::chrono::time_point<std::chrono::system_clock> endTimepoint = std::chrono::system_clock::now();
    std::cout << std::asctime(std::localtime(&endTime));
    std::chrono::duration<double> elapsedSeconds = endTimepoint - startTimepoint;
    std::cout << "time = " << elapsedSeconds.count() << std::endl;
    std::cout << "nbGames nbWhiteWins nbBlackWins nbDraws" << std::endl;
    std::cout << std::endl;
    std::cout << nbGames << ' ' << nbWhiteWins << ' ' << nbBlackWins << ' ' << nbDraws << std::endl;

    return 0;
} 

// TODO test if default SIGINT handling is ok
// TODO use std::regex instead of boost::tokenizer ?

