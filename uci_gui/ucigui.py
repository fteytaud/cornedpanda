#!/usr/bin/env python

# this script is for python 2 !!!

import os
import re
import signal
import subprocess
import sys

# connection to a new process (uci engine or chess arbiter) with communication (standard I/O streams)
class Connection:
    def __init__(self, program):
        # open a new process
        self.process = subprocess.Popen(program, stdin=subprocess.PIPE, stdout=subprocess.PIPE, close_fds=True)
        # Connection inputs come from process output
        self.inStream = self.process.stdout
        # Connection outputs go to process input
        self.outStream = self.process.stdin

    # send a command to the process and get response if any
    def execute_command(self, command):
        # send command to the connected process
        self.outStream.write(command + "\n")
        self.outStream.flush()

    # send a command to the process and get response if any
    def execute_command_response(self, command, responses):
        # send command to the connected process
        self.outStream.write(command + "\n")
        self.outStream.flush()
        # get answers from the connected process
        result = ""
        while True:
            line = self.inStream.readline()
            result = result + line
            if line=="" or [s for s in responses if s in line]:
                return result

# uci engine
# see Universal Chess Interface at http://www.shredderchess.com/chess-info/features/uci-universal-chess-interface.html
class Engine:
    def __init__(self, program, arguments):
        self.connection = Connection(program)
        engineArgs = self.do_uci()
        # set engine options
        # parse a string of options "option1=value1;option2=value2" -> [["option1","value1"],["option2","value2"]]
        argsList = [ s.split('=') for s in arguments.split(';') ] if arguments else []
        for arg in argsList:
            if re.search("name " + arg[0] + " type", engineArgs) == None:
                print "error: no option", arg[0], "in", program
                sys.exit(-1)
            self.do_setoption(arg)
        self.do_isready()

    def do_uci(self): 
        return self.connection.execute_command_response("uci", ["uciok"])

    def do_isready(self): 
        return self.connection.execute_command_response("isready", ["readyok"])

    def do_go(self): 
        return self.connection.execute_command_response("go", ["bestmove"])

    def do_position(self, move_sequence): 
        self.connection.execute_command("position startpos moves " + move_sequence)

    def do_setoption(self, arguments): 
        self.connection.execute_command("setoption name " + arguments[0] + " value " + arguments[1])

    def do_ucinewgame(self): 
        self.connection.execute_command("ucinewgame")

    def do_quit(self): 
        self.connection.execute_command("quit")

# chess arbiter
# home-made protocol:
# - send "gamestate" + a list of moves (e.g. "e2e4 d7d5 f1b5 c7c6") to arbiter's stdin 
#   -> receive game state from arbiter's stdout ("running", "black won", "white won" or "draw")
# - send "validmove" + a list of moves 
#   -> receive "true" if the last move is valid or "false" if it is not
# - send "quit" 
#   -> quit
class Arbiter:
    def __init__(self, program):
        self.connection = Connection(program)

    def do_gamestate(self, move_sequence):
        return self.connection.execute_command_response("gamestate " + move_sequence, ["running","white won","black won","draw"])

    def do_validmove(self, move_sequence):
        return self.connection.execute_command_response("validmove " + move_sequence, ["true","false"])

    def do_quit(self): 
        self.connection.execute_command("quit")

# for running games
class Game:
    def __init__(self, whiteProgram, blackProgram, whiteArguments, blackArguments, arbiterProgram):
        self.arbiter = Arbiter(arbiterProgram)
        self.whiteEngine = Engine(whiteProgram, whiteArguments)
        self.blackEngine = Engine(blackProgram, blackArguments)

    def quit(self):
        self.arbiter.do_quit()
        self.whiteEngine.do_quit()
        self.blackEngine.do_quit()

    def play(self):
        # init new game
        move_sequence = ""
        self.whiteEngine.do_ucinewgame()
        self.blackEngine.do_ucinewgame()
        currentEngine, otherEngine = self.whiteEngine, self.blackEngine
        while True:
            # get move from current engine
            currentEngine.do_position(move_sequence)
            goStr = currentEngine.do_go()
            # extract move from engine response 
            match = re.search(r'bestmove\s+(\w+)\s', goStr)
            move = match.group(1) if match else ""
            move_sequence = move_sequence + " " + move
            print move,
            # check move
            if (move=="" or "false" in self.arbiter.do_validmove(move_sequence)):
                print "INVALID MOVE"
                state = "black won" if currentEngine==self.whiteEngine else "white won"
                return (move_sequence, state) 
            # check game state
            state = self.arbiter.do_gamestate(move_sequence)
            if (not "running" in state):
                print "\n", state
                return (move_sequence, state)
            # swap engines
            currentEngine, otherEngine = otherEngine, currentEngine

def main():
    # check command line arguments
    if len(sys.argv) != 7:
        print "usage:", sys.argv[0], '<arbiter> <white engine> "[white options]" <black engine> "[black options]" <nb games>'
        print "example:", sys.argv[0], 'cornedpandarbitre.out cornedpanda.out "" stockfish "Skill Level=1;nodestime=1000" 2'
        sys.exit(-1)
    # get command line arguments
    arbiter = sys.argv[1]
    whiteEngine = sys.argv[2]
    whiteOptions = sys.argv[3]
    blackEngine = sys.argv[4]
    blackOptions = sys.argv[5]
    nbGames = int(sys.argv[6])
    # create engine and arbiter processes
    game = Game(whiteEngine, blackEngine, whiteOptions, blackOptions, arbiter)
    # exit "more nicely" when SIGINT (C-c)
    signal_handler = lambda signal, frame: game.quit()
    signal.signal(signal.SIGINT, signal_handler)
    # run games
    nbWhiteWins = 0
    nbBlackWins = 0
    nbDraws = 0
    for i in range(nbGames):
        # play game
        print "game", i
        (move_sequence, state) = game.play()
        # get final state
        if "black" in state : 
            nbBlackWins = nbBlackWins + 1
        if "white" in state : 
            nbWhiteWins = nbWhiteWins + 1
        if "draw" in state : 
            nbDraws = nbDraws + 1
    game.quit()
    print "nbGames nbWhiteWins nbBlackWins nbDraws"
    print i+1, nbWhiteWins, nbBlackWins, nbDraws

if __name__ == "__main__":
    main()

