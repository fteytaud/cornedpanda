#! /usr/bin/env python

from getopt import *
import popen2
import sys
import string
import re
import os
import subprocess

#debug = 0
#(child_stdout, child_stdin) = popen2.popen2("somestring", bufsize, mode)
#==>
#p = Popen("somestring", shell=True, bufsize=bufsize,
#          stdin=PIPE, stdout=PIPE, close_fds=True)
#(child_stdout, child_stdin) = (p.stdout, p.stdin)
class Connection:
    #  Members :  infile and oufile

    def __init__(self, command):
        self.p = subprocess.Popen(command, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, close_fds=True)
	(infile, outfile) = (self.p.stdout, self.p.stdin)
        
        self.infile  = infile
        self.outfile = outfile       
    def execute_command(self, cmd):

	self.outfile.write(cmd + "\n")
        self.outfile.flush()
        result = ""	
	        
	if "setoption" in cmd or "position" in cmd or "newgame" in cmd : 
	    line = "sans_reponse"
	else :
	    line = self.infile.readline() 
        while (line != "uciok\n") and (line != "readyok\n") and (line != "setoption") and (not "bestmove" in line) and (line != "sans_reponse") and (line != "") and (line != "running\n") and (not "won" in line) and (not "draw" in line) :
	    result = result + line
	    line = self.infile.readline()
	
	result = result + line
	
        return result
        

class Player:
    #  Member: connection

    def __init__(self, command):
        self.connection = Connection(command)

    def uci(self): 
	return self.connection.execute_command("uci")

    def isready(self): 
	return self.connection.execute_command("isready")

    def isok(self): 
	return self.connection.execute_command("go")	

    def genmove(self, sequence): 
	return self.connection.execute_command("position startpos moves "+ sequence)

    def arguments(self, argument): 
	return self.connection.execute_command("setoption name "+ argument[0]+" value "+argument[1])
 
    def newgame(self): 
	return self.connection.execute_command("ucinewgame")

    def gen_fin(self, sequence):
	return self.connection.execute_command(sequence)


#Entree : un string de la forme [nom_du_parametre,valeur_du_parametre]
#Sortie : une liste de couples [nom_du_parametre, valeur_du_parametre]
def parse(arguments):
    table = []
    arg = arguments
    while arg != "":
	arg1 = re.search("\[(.+?),", arg).group(1)
	arg2 = re.search(",(.+?)\]", arg).group(1)
	table = table + [[arg1, arg2]]
	arg = re.search("\[(.+?\])(.*)", arg).group(2)
    return table


#Entree : ce que ressort l'algo qui calcule un coup
#Sortie : un couple [string, booleen] ou booleen == true si la partie n'est pas finie, et string == valeur de bestmove ou valeur du gagnant
def parse_coup(lignes): 
    if "bestmove" in lignes : 
	if "ponder" in lignes : #on ne prend que le coup propose
	    result = re.search("bestmove (.+?) ", lignes).group(1)
	else : 
	    result = re.search("bestmove (.+?)\n", lignes).group(1)
    	return result, True 
    print ("Erreur parse_coup : ")
    return "erreur : "+lignes, False

class Game:
    #    whiteplayer     Player
    #    blackplayer     Player
    #    arg_white	 string
    #    arg_black	 string
    #    toPlay          string
    #	 arb		 Player
    def __init__(self, whitecommand, blackcommand, whitearguments, blackarguments, arbitre):
        self.whiteplayer = Player(whitecommand)
        self.blackplayer = Player(blackcommand)
        self.arg_white = parse(whitearguments)
	self.arg_black = parse(blackarguments)
        self.toPlay = "B"
	self.arb = Player(arbitre)

    def play(self):
        sequence = ""
	coup_actuel = ""	
	
	self.whiteplayer.newgame()
	self.blackplayer.newgame()
	white_uci = self.whiteplayer.uci()
	black_uci = self.blackplayer.uci()	
	
	while len(self.arg_white) > 0 :
	    if not (self.arg_white[0][0] in white_uci): 
		print ("Argument de blanc non defini : "+self.arg_white[0][0])
		return -1
	    self.whiteplayer.arguments(self.arg_white[0])
	    self.arg_white = self.arg_white[1:]

	while len(self.arg_black) > 0 :
	    if not (self.arg_black[0][0] in black_uci): 
		print("Argument de noir non defini : "+self.arg_black[0][0])
		return -1
	    self.blackplayer.arguments(self.arg_black[0])
	    self.arg_black = self.arg_black[1:]
        
	self.whiteplayer.isready()
	self.blackplayer.isready()	

	calcul = self.whiteplayer.isok() 
	sequence = (parse_coup (calcul))[0]
	move = parse_coup(calcul)
	state = self.arb.gen_fin(sequence)
	while ("running" in state):
	    if self.toPlay == "W":
		self.whiteplayer.genmove(sequence)
	        calcul = self.whiteplayer.isok()
		move = parse_coup (calcul)
		sequence = sequence + " " + move[0]
	        self.toPlay = "B"
		state = self.arb.gen_fin(sequence)
	    elif self.toPlay == "B":
		self.blackplayer.genmove(sequence) 
		calcul = self.blackplayer.isok()
		move = parse_coup (calcul)
		sequence = sequence + " " + move[0]
                self.toPlay = "W"
		state = self.arb.gen_fin(sequence)
	print (sequence)
	print state
	return state


def usage(chaine):
    print chaine
    sys.exit(1)

# --------------------------------------------------
#    Main
# --------------------------------------------------
def main():
    # Default values
    white    = ""
    black    = ""
    arg_white     = ""
    arg_black     = ""
    nb_parties	   = ""
    arbitre	= ""  
  
    #TODO ameliorer le message d'aide pour arg_x
    helpstring = """

        Run with:

        twogtp --white \'<path to program 1> --mode gtp [program options]\' \\
            --black \'<path to program 2> --mode gtp [program options]\' \\
	    --nb_parties \'valeur\' \\
	    --arbitre \'<path to arbitre> --mode gtp [program options]\' \\
	    --arg_white \'"[nom,valeur][nom2,valeur2]..."\'etc... sans espaces (sauf eventuellement dans les "noms" et "valeurs").\\
	    --arg_black \'"[nom,valeur][nom2,valeur2]..."\'etc... sans espaces (sauf eventuellement dans les "noms" et "valeurs").\\
	    Notes : 
	    - les guillemets autour des options sont facultatifs s'il n'y a pas d'espaces dans les noms et valeurs.
 	    - si l'un des parametres depend d'autres, il doit etre place apres (par exemple, utiliser uct pour cornedpanda doit etre le premier parametre si on veut changer ses caracteristiques)
	    - l'ordre des arguments n'a pas d'importance
 """

    try:
        (opts, params) = getopt(sys.argv[1:], "",
                                ["white=",
                                 "black=",
				 "arg_white=",
				 "arg_black=",
				 "nb_parties=",
				 "arbitre=",
                                ])
    except:
        usage(helpstring);

    for opt, value in opts:
        if opt == "--white":
            white = value ;
        elif opt == "--black":
            black = value
        elif opt == "--arg_white":
	    arg_white = value
	elif opt == "--arg_black":
	    arg_black = value
	elif opt == "--nb_parties":
	    nb_parties = int(value)
	elif opt == "--arbitre":
	    arbitre = value
        else : 
	    useage(helpstring); 
    if black == "" or white == "" or nb_parties == "" or arbitre == "": 
        usage(helpstring)

    W = 0
    B = 0
    D = 0
    for i in range(nb_parties):
    	print (str(i)+" parties jouees actuellement")
	game = Game(white, black, arg_white, arg_black, arbitre)
	resultat = ""
    	resultat = game.play()
	
	#Je ne comprends pas pourquoi il y a besoin d'un +1, mais ca marche...
	os.system("kill "+str(game.whiteplayer.connection.p.pid + 1))
	os.system("kill "+str(game.blackplayer.connection.p.pid + 1))
	os.system("kill "+str(game.arb.connection.p.pid + 1))
			
 	print resultat	
	if "black" in resultat : 
	    B = B+1	
	if "white" in resultat : 
	    W = W+1
	if "draw" in resultat : 
	    D = D+1
    if W+B+D != nb_parties : 
	print ("Erreur dans le nombre de parties")
    print("Sur "+ str(i+1)+ " parties jouees : ")
    print("Blanc a gagne "+ str(W)+" parties")
    print("Noir a gagne "+str(B)+" parties")
    print("Il y a eu "+str(D)+" egalites")
    return W-B


main()
